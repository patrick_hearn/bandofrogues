using com.patrickhearn.BandOfRogues.Actors;
using com.patrickhearn.BandOfRogues.Game;
using UnityEngine;
using Zenject;

public class DefaultInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<ActorManager>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<TurnManager>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<GameManager>().AsSingle().NonLazy();
    }
}