﻿using UnityEditor;
using UnityEngine;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace com.patrickhearn.NodeGraph
{
    public class NodeGraphWindow : EditorWindow
    {
        private NodeGraphView _graphView;

        [MenuItem("Graph/Node Graph")]
        public static void OpenNodeGraphWindow()
        {
            NodeGraphWindow window = GetWindow<NodeGraphWindow>();
            window.titleContent = new GUIContent("Node Graph");
        }

        private void OnEnable()
        {
            ConstructGraphView();
        }

        private void ConstructGraphView()
        {
            _graphView = new NodeGraphView
            {
                name = "Node Graph"
            };

            _graphView.StretchToParentSize();
            rootVisualElement.Add(_graphView);
        }
    }
    public class NodeGraphView : GraphView
    {
        public NodeGraphView()
        {
            SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);

            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());

            var grid = new GridBackground();
            Insert(0, grid);
            grid.StretchToParentSize();

            AddElement(new NodeView("Hello World Node"));
        }
    }
    public class NodeView : Node
    {
        public NodeView(string nodeName)
        {
            title = nodeName;
        }
    }
}