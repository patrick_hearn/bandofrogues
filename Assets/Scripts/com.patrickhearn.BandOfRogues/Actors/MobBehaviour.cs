using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Actors
{
    class MobBehaviour : ActorBehaviour
    {
        public override Actor Actor
        {
            get => Mob;
            set => Mob = (Mob)value;
        }
        
        [field:SerializeField]public Mob Mob { get; set; }
    }
}