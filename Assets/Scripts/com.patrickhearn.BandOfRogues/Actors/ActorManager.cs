﻿using System.Collections.Generic;
using System.Linq;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;

using UnityEditor;
using UnityEditorInternal;

namespace com.patrickhearn.BandOfRogues.Actors
{
    public class ActorManager : IActorManager, IPathfinder<Vector2Int>, ITickable
    {
        public delegate void ActorEvent(Actor actor);
        public event ActorEvent ActorAdded;
        public event ActorEvent ActorRemoved;
        
        public readonly List<Actor> Actors = new List<Actor>();
        
        public List<Mob> Mobs => Actors.FindAll(actor => actor is Mob).ConvertAll(mob => (Mob)mob);
        public List<Item> Items => Actors.FindAll(actor => actor is Item).ConvertAll(item => (Item)item);
        public List<Door> Doors => Actors.FindAll(actor => actor is Door).ConvertAll(door => (Door)door);
        
        public void RegisterActor(IActor actor)
        {
            Actors.Add(actor as Actor);
            Actors.Last().ActorRemoved += UnregisterActor;
            ActorAdded?.Invoke(actor as Actor);
        }

        public void UnregisterActor(IActor actor)
        {
            Actors.Remove(actor as Actor);
            ActorRemoved?.Invoke(actor as Actor);
        }

        public List<IActor> GetActors()
        {
            return Actors.ToList<IActor>();
        }

        public IActor GetActorAt(Vector2Int position)
        {
            return Actors.FirstOrDefault(actor => actor.Position == position);
        }

        public List<IActor> GetActorsOfType<T>()
        {
            return Actors.Where(actor => actor is T).ToList<IActor>();
        }

        #region IPathfinder
        private IPathfinder<Vector2Int> _pathfinder;
        
        public void SetPathfinder(IPathfinder<Vector2Int> pathfinder)
        {
            _pathfinder = pathfinder;
        }
        public List<Vector2Int> FindPath(Vector2Int start, Vector2Int goal) => _pathfinder.FindPath(start, goal);

        public void SetWalkableGrid(bool[,] grid) => _pathfinder.SetWalkableGrid(grid);

        public void SetCostGrid(float[,] grid) => _pathfinder.SetCostGrid(grid);

        public bool AllowDiagonal
        {
            get => _pathfinder.AllowDiagonal;
            set => _pathfinder.AllowDiagonal = value;
        }
        #endregion

        public void Tick()
        {
            foreach (var actor in Actors)
            {
                actor.Tick();
            }
        }
    }
    
    // Custom editor because Actor is abstract
    #if UNITY_EDITOR
    
    
    [CustomEditor(typeof(ActorManager))]
    public class ActorManagerEditor : Editor
    {
        private ReorderableList _actorList;
        private void OnEnable()
        {
            _actorList = new ReorderableList(serializedObject, serializedObject.FindProperty("Actors"), true, true, true, true);
            _actorList.drawElementCallback = (rect, index, isActive, isFocused) =>
            {
                var element = _actorList.serializedProperty.GetArrayElementAtIndex(index);
                rect.y += 2;
                EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight), element, GUIContent.none);
            };
            _actorList.drawHeaderCallback = rect => EditorGUI.LabelField(rect, "Actors");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            _actorList.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
        }
    }
    #endif
}