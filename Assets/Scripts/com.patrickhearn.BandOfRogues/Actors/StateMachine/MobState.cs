﻿using System;
using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.Actors.StateMachine
{
    public class MobStateFactory : IStateFactoryEnumerated<MobState, EMobState>
    {
        public MobState GetState(EMobState state)
        {
            return state switch
            {
                EMobState.Waiting => new WaitingState(),
                EMobState.Moving => new MovingState(),
                EMobState.Attacking => new AttackingState(),
                EMobState.Dead => new DeadState(),
                _ => null
            };
        }

        public MobState Create()
        {
            return new WaitingState();
        }
    }
    
    
    [Serializable]
    public abstract class MobState : IStateEnumerated<EMobState>
     {
         /// <summary>
         /// Abstract class for all Mob States
         /// </summary>
         ///
         protected MobState()
         {
         }

         protected MobState(Mob mob)
         {
             Mob = mob;
         }
         
         protected Mob Mob;
         
         public abstract EMobState StateEnum { get; }
         public abstract bool CanTransitionTo(EMobState state);
         public abstract void Enter();
        public abstract void Execute();
        public abstract void Exit();
        public bool CanTransitionTo(IState state)
        {
            return CanTransitionTo((state as IStateEnumerated<EMobState>)?.StateEnum ?? EMobState.Waiting);
        }
        public void SetMob(Mob mob)
        {
            Mob = mob;
        }
     }
    
    public class AttackingState : MobState
    {
        public override EMobState StateEnum => EMobState.Attacking;

        public override bool CanTransitionTo(EMobState state)
        {
            return state switch
            {
                EMobState.Attacking => false,
                EMobState.Dead => true,
                EMobState.Moving => true,
                EMobState.Waiting => true,
                _ => false
            };
        }

        public override void Enter()
        {
        }
        
        public override void Execute()
        {
        }
        
        public override void Exit()
        {
        }


    }
    
    public class DeadState : MobState
    {
        public override EMobState StateEnum => EMobState.Dead;

        public override bool CanTransitionTo(EMobState state)
        {
            return state switch
            {
                EMobState.Attacking => false,
                EMobState.Dead => false,
                EMobState.Moving => false,
                EMobState.Waiting => false,
                _ => false
            };

        }

        public override void Enter()
        {
        }
        
        
        public override void Execute()
        {
        }
        
        public override void Exit()
        {
        }

    }
    
    public class MovingState : MobState
    {
        public override EMobState StateEnum => EMobState.Moving;

        public override bool CanTransitionTo(EMobState state)
        {
            return state switch
            {
                EMobState.Attacking => true,
                EMobState.Dead => true,
                EMobState.Moving => false,
                EMobState.Waiting => true,
                _ => false
            };
        }

        public override void Enter()
        {
        }
        
        public override void Execute()
        {
        }
        
        public override void Exit()
        {
        }

    }
    
    public class WaitingState : MobState
    {
        public override EMobState StateEnum => EMobState.Waiting;

        public override bool CanTransitionTo(EMobState state)
        {
            return state switch
            {
                EMobState.Attacking => true,
                EMobState.Dead => true,
                EMobState.Moving => true,
                EMobState.Waiting => false,
                _ => false
            };
        }

        public override void Enter()
        {
        }
        
        
        public override void Execute()
        {
        }
        
        public override void Exit()
        {
        }
    }
}