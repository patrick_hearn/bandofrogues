﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.patrickhearn.BandOfRogues.Actors.StateMachine;
using com.patrickhearn.BandOfRogues.Combat;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Inventory;
using com.patrickhearn.BandOfRogues.Map;
using com.patrickhearn.BandOfRogues.Modifiers;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Actors
{

    [Serializable]
    public class Mob : Actor, ICombatant, IMob, IMetric, IModifierContainer, IStatefulEnumerated<EMobState>, IFactional
    {
        private ActorManager _actorManager;
        #region Properties
        [field:SerializeField] public int Strength { get; set; }
        [field:SerializeField] public int Dexterity { get; set; }
        [field:SerializeField] public int Constitution { get; set; }
        [field:SerializeField] public int Intelligence { get; set; } 
        [field:SerializeField]public int Level { get; set; }
        [field:SerializeField]public EMobState State { get; set; }
        [field:SerializeField]public float Health { get; set; }
        [field:SerializeField]public int AttackRange { get; set; }
        [field:SerializeField]public List<InventoryItem> Inventory { get; set; }
        
        public Dictionary<EEquipSlot, IEquipable> EquippedItems { get; set; }
        
        public Mob(ActorManager actorManager)
        {
            _actorManager = actorManager;
        }

        public float GetAccuracy()
        {
            return GetMetric(EMetric.Accuracy);
        }

        public float GetEvasion()
        {
            return GetMetric(EMetric.Evasion);
        }

        public float GetMinHit()
        {
            return GetMetric(EMetric.MinHit);
        }

        public float GetMaxHit()
        {
            return GetMetric(EMetric.MaxHit);
        }

        public float GetMinDefense()
        {
            return GetMetric(EMetric.MinDefense);
        }

        public float GetMaxDefense()
        {
            return GetMetric(EMetric.MaxDefense);
        }

        public bool IsDead()
        {
            return CurrentStateEnum == EMobState.Dead;
        }

        public void Attack(ICombatant target)
        {
            MobAttacked?.Invoke(this, target as Actor);
            CombatResolver.ResolveCombat(this, target);
        }

        [field:SerializeField]public Team Team { get; set; }
        [field:SerializeField]public List<IModifier> Modifiers { get; set; }

        private float MoveProgressPerTurn => Speed / 100f;
        [field:SerializeField]public bool IsMoving { get; set; }
        [field:SerializeField]public float MoveProgress { get; set; }
        public float Speed => GetMetric(EMetric.Speed);
        public Vector2Int Destination { get; set; }

        public bool AttemptMove(Vector2Int targetPosition)
        {
            if (MoveProgress < 1f)
                return false;
            return MoveManager.TryMoveActor(this, targetPosition);
        }

        [SerializeField] private MobState _currentState;

        [FormerlySerializedAs("actionTarget")] [SerializeField]
        private Actor _actionTarget;

        public Actor ActionTarget
        {
            get => _actionTarget;
            set => _actionTarget = value;
        }

        [field:SerializeField]public Vector2Int MoveTarget { get; set; }


        #endregion
        
        
        // Event Delegates

        #region Event Delegates
        public delegate void TurnStartedDelegate(Mob mob);
        public event TurnStartedDelegate TurnStarted;
        public delegate void TurnEndedDelegate(Mob mob);
        public event TurnEndedDelegate TurnEnded;
        
        public delegate void MobAttackedDelegate(Mob mob, Actor target);
        public event MobAttackedDelegate MobAttacked;
        public delegate void MobDiedDelegate(Mob mob);
        public event MobDiedDelegate MobDied;
        public delegate void MobHealedDelegate(Mob mob, int healAmount);
        public event MobHealedDelegate MobHealed;
        public delegate void MobConditionAddedDelegate(Mob mob, IModifier modifier);
        public event MobConditionAddedDelegate MobConditionAdded;
        public delegate void MobConditionRemovedDelegate(Mob mob, IModifier modifier);
        public event MobConditionRemovedDelegate MobModifierRemoved;
        public delegate void MobDamagedDelegate(Mob mob, float damage, ICombatant attacker);
        public event MobDamagedDelegate MobDamaged;
        #endregion

        protected float TickDelta { get; private set; }
        public override void DoTurn(float tickDelta)
        {
            TickDelta = tickDelta;
            TurnStarted?.Invoke(this);
            Move();
            TurnEnded?.Invoke(this);
        }

        public override void Interact(IInteractable source, Vector2Int position, IDictionary<string, object> parameters = null)
        {
            if (source is ICombatant combatant)
            {
                if( source is IFactional factionable)
                    if(FactionManager.IsFriendly(Faction, factionable.Faction))
                        return;
                Attack(combatant);
                return;
            }
        }

        public void Move()
        {
            if (!IsMoving) return;
            if (MoveTarget == Position) return;
            MoveProgress += MoveProgressPerTurn;
            if (MoveProgress < 1) return;
            MoveProgress -= 1;
            IsMoving = false;
            MoveManager.TryMoveActor(this, MoveTarget);
            // transform.position = Vector3.Lerp(new Vector3(X, Y, 0), new Vector3(MoveTarget.x, MoveTarget.y, 0), MoveProgress);
        }
    
        public void Attack(Mob target)
        {
            State = EMobState.Attacking;
            MobAttacked?.Invoke(this, target);
            CombatResolver.ResolveCombat(this, target);
        }
    
        public void Die()
        {
            State = EMobState.Dead;
            MobDied?.Invoke(this);
            
        }
        
        public void TakeDamage(int damage, ICombatant attacker)
        {
            Health -= damage;
            MobDamaged?.Invoke(this, damage, attacker);
            if (Health <= 0)
                Die();
        }
        
        public void Heal(int healAmount)
        {
            Health += healAmount;
            MobHealed?.Invoke(this, healAmount);
        }

        #region Modifiers
        public void AddModifier(IModifier modifier)
        {
            Modifiers.Add(modifier);
            MobConditionAdded?.Invoke(this, modifier);
        }
        
        public void RemoveModifier(IModifier modifier)
        {
            Modifiers.Remove(modifier);
            MobModifierRemoved?.Invoke(this, modifier);
        }
        
        public void RemoveAllModifier()
        {
            foreach (var modifier in Modifiers)
            {
                RemoveModifier(modifier);
                MobModifierRemoved?.Invoke(this, modifier);
            }
        }
        #endregion


        private Vector2Int[] GetPathTo(Vector2Int position)
        {
            return _actorManager.FindPath(Position, position).ToArray(); 
        }

        private Vector2Int[] GetPathTo([NotNull] IActor actor)
        {
            if (actor == null) throw new ArgumentNullException(nameof(actor));
            return GetPathTo(actor.Position);
        }

        public void StopMoving()
        {
            IsMoving = false;
            MoveProgress = 0;
            MoveTarget = Position;
        }
        
        public void StartMoving(Vector2Int target)
        {
            
            Debug.Log($"{Name}: StartMoving({target})");
            if(target == Position) return;
            var path = GetPathTo(target);
            if (path.Length > 0)
            {
                Debug.Log($"{Name}: StartMoving({target}) - Path");
                State = EMobState.Moving;
                IsMoving = true;
                // MoveProgress = 0;
                MoveTarget = path[0];
            }
        }

        #region IStateful implementation
        IState IStateful.CurrentState => CurrentState;

        public void ChangeState(MobState newState)
        {
            CurrentState?.Exit();
            _currentState = newState;
            _currentState.Enter();
        }

        public MobState CurrentState => _currentState;

        public void ChangeState(IState newState)
        {
            ChangeState(newState as MobState);
        }

        public void Update()
        {
            CurrentState?.Execute();
        }

        public EMobState CurrentStateEnum => CurrentState.StateEnum;
        public void ChangeState(EMobState newState)
        {
            ChangeState(new MobStateFactory().GetState(newState));
        }
        #endregion

        #region Equipment
        
        public Dictionary<EEquipSlot, IEquipable> Equipment { get; set; } = new();
        public IEquipable GetEquipped(EEquipSlot slot)
        {
            return !IsEquipped(slot) ? null : Equipment[slot];
        }
        
        public bool IsEquipped(EEquipSlot slot)
        {
            return Equipment.ContainsKey(slot);
        }
        
        public List<IEquipable> GetEquipped()
        {
            return Equipment.Values.ToList();
        }

        public List<IEquipable> GetEquippedSorted()
        {
            return Equipment.OrderBy(x => x.Key).Select(x => x.Value).ToList();
        }
        
        public bool Equip(IEquipable equipment, bool force = false)
        {
            if (!force && Equipment.ContainsKey(equipment.EquipSlot)) return false;
            Equipment[equipment.EquipSlot] = equipment;
            return true;
        }
        
        public bool Unequip(EEquipSlot slot)
        {
            if (!Equipment.ContainsKey(slot)) return false;
            Equipment.Remove(slot);
            return true;
        }
        
        public float ApplyMetricForEquipped(EMetric metric, float value)
        {
            return Equipment.Values.Aggregate(value, (current, equipment) => equipment.ApplyMetric(metric, current));
        }
        
        #endregion

        #region Metrics
        public float ApplyMetric(EMetric metric, float value)
        {
            Modifiers.Where(condition => condition.GetPhase()==ETriPhase.Early).ToList().ForEach(condition => value = condition.GetModifierEffect(metric, value));
            value += GetMetric(metric);
            value = ApplyMetricForEquipped(metric, value);
            Modifiers.Where(condition => condition.GetPhase()==ETriPhase.Default).ToList().ForEach(condition => value = condition.GetModifierEffect(metric, value));
            Modifiers.Where(condition => condition.GetPhase()==ETriPhase.Late).ToList().ForEach(condition => value = condition.GetModifierEffect(metric, value));
            return value;
        }

        public float GetMetric(EMetric metric)
        {
            return metric switch
            {
                EMetric.Accuracy => Level + Dexterity,
                EMetric.Evasion => Level + Dexterity,
                EMetric.MinHit => Level,
                EMetric.MaxHit => Level + Strength,
                EMetric.MinDefense => Level,
                EMetric.MaxDefense => Level + Constitution,
                EMetric.Speed => Level + Dexterity,
                _ => 0
            };
        }
        #endregion

        public Faction Faction { get; set; }
        
    }
}