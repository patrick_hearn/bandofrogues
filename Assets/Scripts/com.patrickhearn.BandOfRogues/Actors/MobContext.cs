﻿using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.Actors
{
    public class MobContext : Context
    {
        public Mob Mob { get; private set; }

        public MobContext(Mob mob)
        {
            Mob = mob;
        }
    }
}