﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Actors
{
    public abstract class ActorBehaviour : MonoBehaviour
    {
        public virtual Actor Actor { get; set; }
        public float zOffset = 0.0f;
        
        private void Awake()
        {
            Actor = GetComponent<Actor>();
            Actor.ActorMoved += OnActorMoved;
            Actor.ActorPlaced += OnActorPlaced;
            Actor.ActorRemoved += OnActorRemoved;
        }
        
        
        
        private void OnActorMoved(Actor actor, Vector2Int oldPosition, Vector2Int newPosition)
        {
            transform.position = new Vector3(newPosition.x, newPosition.y, zOffset);
        }
        
        private void OnActorPlaced(Actor actor, Vector2Int position)
        {
            transform.position = new Vector3(position.x, position.y, zOffset);
        }
        
        private void OnActorRemoved(Actor actor)
        {
            Destroy(gameObject);
        }
        
    }
}