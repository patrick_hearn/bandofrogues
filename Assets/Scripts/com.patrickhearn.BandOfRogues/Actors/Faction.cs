﻿using System;
using System.Collections.Generic;

namespace com.patrickhearn.BandOfRogues.Actors
{
    public interface IFactional
    {
        Faction Faction { get; set; }
    }

    [Flags]
    public enum Faction
    {
        None = 0,
        Player = 1 << 0, // 1
        Enemies = 1 << 1, // 2
        Neutral = 1 << 2, // 4
        Allies = 1 << 3, // 8
    }
    
    public static class FactionManager
    {
        private static readonly List<List<Faction>> Allies  = new()
        {
            new List<Faction> { Faction.Player, Faction.Allies, Faction.Neutral },
            new List<Faction> { Faction.Enemies, Faction.Neutral },
        };

        public static bool IsFriendly(Faction faction1, Faction faction2)
        {
            return Allies[(int) faction1].Contains(faction2);
        }
        
        public static bool IsHostile(Faction faction1, Faction faction2)
        {
            return !IsFriendly(faction1, faction2);
        }
        
        
    }
}