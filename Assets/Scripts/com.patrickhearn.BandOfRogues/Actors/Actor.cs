using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Combat;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Tagging;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Actors
{
    [System.Serializable]
    public abstract class Actor : IActor, ITaggable, ITickable
    {
        public string Name { get; set; }
        [field: SerializeField]public Vector2Int Position { get; set; }

        [field: SerializeField]public bool IsBlocking { get; set; }

        public abstract void DoTurn(float tickDelta);
        
        // Event Delegates
        public delegate void ActorMovedDelegate(Actor actor, Vector2Int oldPosition, Vector2Int newPosition);
        public event ActorMovedDelegate ActorMoved;
        public delegate void ActorPlacedDelegate(Actor actor, Vector2Int position);
        public event ActorPlacedDelegate ActorPlaced;
        public delegate void ActorRemovedDelegate(Actor actor);
        public event ActorRemovedDelegate ActorRemoved;
        public static IMoveManager MoveManager { get; set; }
        public void PlaceActor(Vector2Int position)
        {
            var oldPosition = Position;
            MoveManager.TryPlaceActor(this, position);
            ActorPlaced?.Invoke(this, position);
        }

        
        public void MoveActor(Vector2Int position)
        {
            var oldPosition = Position;
            MoveManager.TryMoveActor(this, position);
            ActorMoved?.Invoke(this, oldPosition, position);
        }
        
        public void MoveActor(int x, int y)
        {
            MoveActor(new Vector2Int(x, y));
            
        }

        [field:SerializeField]public List<string> Tags { get; set; }
        public void Tick()
        {
            DoTurn(Time.deltaTime);
        }

        public abstract void Interact(IInteractable source, Vector2Int position,
            IDictionary<string, object> parameters = null);
    }
}