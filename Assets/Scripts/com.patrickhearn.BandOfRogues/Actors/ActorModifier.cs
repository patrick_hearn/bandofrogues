﻿using System;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Modifiers;

namespace com.patrickhearn.BandOfRogues.Actors
{
    public interface IActorModifier : IModifier
    {
        public string Name { get; set; }
        public int Duration { get; set; }
        public int CreatedOnTurn { set; get; }
    }

    [Serializable]
    public abstract class ActorModifier : Modifier
    {
        public string Name { set; get; }
        public int Duration { get; set; }
        public int CreatedOnTurn { get; set; }
    
        public ActorModifier(string name, int duration)
        {
            Name = name;
            Duration = duration;
        }
    
        public ActorModifier(string name, int duration, int createdOnTurn)
        {
            Name = name;
            Duration = duration;
            CreatedOnTurn = createdOnTurn;
        }

    }
}