﻿using System;
using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Actors
{
    [Serializable]
    public class Door : Actor
    {
        private bool _isOpen;
        public bool IsOpen => _isOpen;
        
        [FormerlySerializedAs("BaseHealth")] [SerializeField]private float _baseHealth = 100f;
        
        // DoorEvents
        public delegate void DoorOpened(Door door);
        public event DoorOpened OnDoorOpened;
        
        public delegate void DoorClosed(Door door);
        public event DoorClosed OnDoorClosed;
        
        public override void DoTurn(float tickDelta)
        {
            
        }

        public override void Interact(IInteractable source, Vector2Int position, IDictionary<string, object> parameters = null)
        {
            _ = IsOpen ? Close() : Open();
        }


        protected virtual bool OpenInternal()
        {
            return true;
        }
        
        public bool Open()
        {
            if (!OpenInternal())
                return false;
            
            if (_isOpen) return true;
            _isOpen = true;
            OnDoorOpened?.Invoke(this);
            return true;
        }
        
        protected virtual bool CloseInternal()
        {
            return true;
        }
        
        public bool Close()
        {
            if (!CloseInternal())
                return false;
            if (!_isOpen) return true;
            _isOpen = false;
            OnDoorClosed?.Invoke(this);
            return true;
        }
        
    }
}