﻿using System;
using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Inventory;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Actors
{
    [Serializable]
    public class Item : Actor, IPickupable<InventoryItem>
    {
        public InventoryItem InventoryItem;
        public override void DoTurn(float tickDelta)
        {
        }

        public override void Interact(IInteractable source, Vector2Int position, IDictionary<string, object> parameters = null)
        {
            if (source is IInventoryHolder inventoryHolder)
            {
                inventoryHolder.Inventory.AddItem(InventoryItem);
                
            }
        }


        public InventoryItem Pickup()
        {
            return InventoryItem;
        }
    }

    public interface IInventoryHolder
    {
        IInventory Inventory { get; set; }
        
    }

    public interface IInventory
    {
        void AddItem(InventoryItem item);
        void RemoveItem(InventoryItem item);
        List<InventoryItem> Items { get; set; }
        
    }
}