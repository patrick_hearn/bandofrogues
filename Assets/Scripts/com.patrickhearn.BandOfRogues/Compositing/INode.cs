﻿using System.Collections.Generic;

namespace com.patrickhearn.BandOfRogues.Compositing
{
    public interface INode
    {
        int Id { get; }
        void Execute();
    }
    
    public interface INode<TInput, TOutput> : INode
    {
        List<TInput> Inputs { get; }
        List<TOutput> Outputs { get; }
    }
    
    public abstract class MergeNode<TInput, TOutput> : INode<TInput, TOutput>
    {
        private int _id;
        public int Id
        {
            get
            {
                if (_id == 0)
                {
                    _id = GetHashCode();
                }

                return _id;
            }
        }

        public abstract List<TInput> Inputs { get; set; }
        public abstract List<TOutput> Outputs { get; set; }
        public abstract void Execute();
    }
    
    public class AddNode : MergeNode<float[,], float[,]>
    {
        // start from the end of the graph and work backwards
        // each node asks its inputs for their outputs
        public override List<float[,]> Inputs { get; set; }
        public override List<float[,]> Outputs { get; set; }

        public override void Execute()
        {
            throw new System.NotImplementedException();
        }
    }
}