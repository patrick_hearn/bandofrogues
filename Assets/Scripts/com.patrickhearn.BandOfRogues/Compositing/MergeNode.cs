using System;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Compositing
{
    public class MergeNode : ImageNode
    {
        public override void Execute()
        {
            
        }
        
        public static float[,] AddPixelGrids(float[,] grid1, float[,] grid2)
        {
            if (grid1.GetLength(0) != grid2.GetLength(0) || grid1.GetLength(1) != grid2.GetLength(1))
            {
                throw new ArgumentException("Pixel grids must be the same size");
            }
            
            var width = grid1.GetLength(0);
            var height = grid1.GetLength(1);
            var result = new float[width, height];
            
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height ; y++)
                {
                    result[x, y] = grid1[x, y] + grid2[x, y];
                }
            }

            return result;
        }
        
        public static float[,] DividePixelGrid(float[,] grid, int divisor)
        {
            var width = grid.GetLength(0);
            var height = grid.GetLength(1);
            var result = new float[width, height];
            
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height ; y++)
                {
                    result[x, y] = grid[x, y] / divisor;
                }
            }

            return result;
        }
        
        public static float[,] MultiplyPixelGrids(float[,] grid1, float[,] grid2)
        {
            if (grid1.GetLength(0) != grid2.GetLength(0) || grid1.GetLength(1) != grid2.GetLength(1))
            {
                throw new ArgumentException("Pixel grids must be the same size");
            }
            
            var width = grid1.GetLength(0);
            var height = grid1.GetLength(1);
            var result = new float[width, height];
            
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height ; y++)
                {
                    result[x, y] = grid1[x, y] * grid2[x, y];
                }
            }

            return result;
        }
        
        public static float[,] MultiplyPixelGrids(float[,] grid1, float value)
        {
            var width = grid1.GetLength(0);
            var height = grid1.GetLength(1);
            var result = new float[width, height];
            
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height ; y++)
                {
                    result[x, y] = grid1[x, y] * value;
                }
            }

            return result;
        }
        
        public static float[,] SubtractPixelGrids(float[,] grid1, float[,] grid2)
        {
            if (grid1.GetLength(0) != grid2.GetLength(0) || grid1.GetLength(1) != grid2.GetLength(1))
            {
                throw new ArgumentException("Pixel grids must be the same size");
            }
            
            var width = grid1.GetLength(0);
            var height = grid1.GetLength(1);
            var result = new float[width, height];
            
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height ; y++)
                {
                    result[x, y] = grid1[x, y] - grid2[x, y];
                }
            }
            return result;
        }
    }
    
    public class AverageMergeNode : MergeNode
    {
        public new int NumInputs { get; set; } = 8;
        
        public override void Execute()
        {
            ClearPixelGrid();
            foreach (var input in Inputs)
            {
                PixelGrid = AddPixelGrids(PixelGrid, input.PixelGrid);
            }
            PixelGrid = DividePixelGrid(PixelGrid, Inputs.Count);
        }
    }
    
    public class AddMergeNode : MergeNode
    {
        public new int NumInputs { get; set; } = 2;
        
        public override void Execute()
        {
            ClearPixelGrid();
            foreach (var input in Inputs)
            {
                PixelGrid = AddPixelGrids(PixelGrid, input.PixelGrid);
            }
        }
    }
    
    public class MultiplyMergeNode : MergeNode
    {
        public new int NumInputs { get; set; } = 2;

        public override void Execute()
        {
            ClearPixelGrid();
            foreach (var input in Inputs)
            {
                PixelGrid = MultiplyPixelGrids(PixelGrid, input.PixelGrid);
            }
        }
    }
    
    public class SubtractMergeNode : MergeNode
    {
        public new int NumInputs { get; set; } = 2;
        
        public override void Execute()
        {
            ClearPixelGrid();
            foreach (var input in Inputs)
            {
                PixelGrid = SubtractPixelGrids(PixelGrid, input.PixelGrid);
            }
        }
    }
    
    public class MultiplyByValueMergeNode : MergeNode
    {
        public new int NumInputs { get; set; } = 1;
        public float Value { get; set; } = 1;
        
        public override void Execute()
        {
            ClearPixelGrid();
            foreach (var input in Inputs)
            {
                PixelGrid = MultiplyPixelGrids(PixelGrid, Value);
            }
        }
    }
    
    public class DivideByValueMergeNode : MergeNode
    {
        public new int NumInputs { get; set; } = 1;
        public float Value { get; set; } = 1;
        
        public override void Execute()
        {
            if (Value == 0)
            {
                throw new DivideByZeroException();
            }
            ClearPixelGrid();
            foreach (var input in Inputs)
            {
                PixelGrid = DividePixelGrid(PixelGrid, (int)Value);
            }
        }
    }
    
    public class MinMergeNode : MergeNode
    {
        public new int NumInputs { get; set; } = 2;
        
        public override void Execute()
        {
            ClearPixelGrid();
            foreach (var input in Inputs)
            {
                for (int x = 0; x < PixelGrid.GetLength(0); x++)
                {
                    for (int y = 0; y < PixelGrid.GetLength(1); y++)
                    {
                        PixelGrid[x, y] = Mathf.Min(PixelGrid[x, y], input.PixelGrid[x, y]);
                    }
                }
            }
        }
    }
    
    public class MaxMergeNode : MergeNode
    {
        public new int NumInputs { get; set; } = 2;
        
        public override void Execute()
        {
            ClearPixelGrid();
            foreach (var input in Inputs)
            {
                for (int x = 0; x < PixelGrid.GetLength(0); x++)
                {
                    for (int y = 0; y < PixelGrid.GetLength(1); y++)
                    {
                        PixelGrid[x, y] = Mathf.Max(PixelGrid[x, y], input.PixelGrid[x, y]);
                    }
                }
            }
        }
    }

    public class ScreenMergeNode : MergeNode
    {
        public new int NumInputs { get; set; } = 2;
        
        public override void Execute()
        {
            ClearPixelGrid();
            foreach (var input in Inputs)
            {
                for (int x = 0; x < PixelGrid.GetLength(0); x++)
                {
                    for (int y = 0; y < PixelGrid.GetLength(1); y++)
                    {
                        PixelGrid[x, y] = 1 - (1 - PixelGrid[x, y]) * (1 - input.PixelGrid[x, y]);
                    }
                }
            }
        }
    }
}