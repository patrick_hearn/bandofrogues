﻿namespace com.patrickhearn.BandOfRogues.Compositing
{
    public enum InterpolationType
    {
        NearestNeighbor,
        Bilinear,
        Bicubic
    }
}