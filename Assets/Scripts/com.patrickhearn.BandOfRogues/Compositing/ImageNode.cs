﻿using System.Collections.Generic;

namespace com.patrickhearn.BandOfRogues.Compositing
{
    public abstract class ImageNode : Node
    {
        public new List<ImageNode> Inputs { get; set; }
        public new List<ImageNode> Outputs { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public float[,] PixelGrid { get; set; }
        
        public override void Execute()
        {
            throw new System.NotImplementedException();
        }
        
        public void SetPixelGrid(float[,] pixelGrid)
        {
            PixelGrid = pixelGrid;
        }
        
        public void SetPixelGrid(int width, int height)
        {
            PixelGrid = new float[width, height];
            SetPixelGrid(PixelGrid);
        }
        
        public void SetPixelGrid(int width, int height, float[,] pixelGrid)
        {
            Width = width;
            Height = height;
            PixelGrid = pixelGrid;
        }
        
        public void SetPixelGrid(int width, int height, float value)
        {
            PixelGrid = new float[width, height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height ; y++)
                {
                    PixelGrid[x, y] = value;
                }
            }
            SetPixelGrid(width, height, PixelGrid);
        }

        public void SetPixelGrid(int width, int height, int value)
        {
            SetPixelGrid(width, height, (float)value);
        }

        public void SetPixelGrid(int[,] pixelGrid, int whiteLevel = 255)
        {
            var floatPixelGrid = new float[pixelGrid.GetLength(0), pixelGrid.GetLength(1)];
            for (int x = 0; x < pixelGrid.GetLength(0); x++)
            {
                for (int y = 0; y < pixelGrid.GetLength(1); y++)
                {
                    floatPixelGrid[x, y] = pixelGrid[x, y] / (float)whiteLevel;
                }
            }
            SetPixelGrid(floatPixelGrid);
        }
        
        public void SetPixel(int x, int y, float value)
        {
            PixelGrid[x, y] = value;
        }
        
        public void SetPixel(int x, int y, int value, int whiteLevel = 255)
        {
            SetPixel(x, y, value / (float)whiteLevel);
        }
        
        public void ClearPixelGrid()
        {
            SetPixelGrid(Width, Height, 0);
        }
        
        public float GetPixel(int x, int y)
        {
            return PixelGrid[x, y];
        }
        
        public float[,] GetPixelGrid()
        {
            return PixelGrid;
        }
        
        public int[,] GetPixelGrid(int whiteLevel = 255)
        {
            var intPixelGrid = new int[Width, Height];
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height ; y++)
                {
                    intPixelGrid[x, y] = (int)(PixelGrid[x, y] * whiteLevel);
                }
            }
            return intPixelGrid;
        }
        
        public float GetPixel(float x, float y)
        {
            return PixelGrid[(int)x, (int)y];
        }

        public static float InterpolateSubpixel(float x, float y, float[,] pixelGrid,
            InterpolationType interpolationType)
        {
            switch (interpolationType)
            {
                case InterpolationType.Bicubic:
                    return InterpolateBicubic(x, y, pixelGrid);
                case InterpolationType.Bilinear:
                    return InterpolateBilinear(x, y, pixelGrid);
                case InterpolationType.NearestNeighbor:
                    return InterpolateNearestNeighbor(x, y, pixelGrid);
                default:
                    return InterpolateNearestNeighbor(x, y, pixelGrid);
                
            }
        } 
        
        public static float InterpolateNearestNeighbor(float x, float y, float[,] pixelGrid)
        {
            return pixelGrid[(int)x, (int)y];
        }
        
        public static float InterpolateBicubic(float x, float y, float[,] pixelGrid)
        {
            throw new System.NotImplementedException();
        }
        
        public static float InterpolateBilinear(float x, float y, float[,] pixelGrid)
        {
            throw new System.NotImplementedException();
        }
        
        


    }
}