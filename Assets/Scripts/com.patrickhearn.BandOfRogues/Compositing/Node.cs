﻿using System.Collections.Generic;

namespace com.patrickhearn.BandOfRogues.Compositing
{
    public abstract class Node : INode
    {
        public List<INode> Inputs { get; set; }
        public List<INode> Outputs { get; set; }
        
        // ints for number of inputs and outputs which will be overridden by the child class
        public int NumInputs { get; set; }
        
        public int Id { get; set; } = GenerateId();

        public abstract void Execute();

        #region Add/Remove Inputs and Outputs

        

        
        public void AddInput(INode input)
        {
            if (Inputs.Count < NumInputs)
            {
                Inputs.Add(input);
            }
        }
        
        public void AddOutput(INode output)
        {
            Outputs.Add(output);
        }
        
        public void RemoveInput(INode input)
        {
            Inputs.Remove(input);
        }
        
        public void RemoveOutput(INode output)
        {
            Outputs.Remove(output);
        }
        
        public void RemoveAllInputs()
        {
            Inputs.Clear();
        }
        
        public void RemoveAllOutputs()
        {
            Outputs.Clear();
        }
        
        public void RemoveAllConnections()
        {
            RemoveAllInputs();
            RemoveAllOutputs();
        }
        
        public void RemoveInputAt(int index)
        {
            Inputs.RemoveAt(index);
        }
        
        public void RemoveOutputAt(int index)
        {
            Outputs.RemoveAt(index);
        }
        #endregion
        
        #region Constructors
        public Node()
        {
            Inputs = new List<INode>();
            Outputs = new List<INode>();
        }
        #endregion
        
        #region Id Generation
        private static int _idCounter;
        private static int GenerateId()
        {
            return _idCounter++;
        }
        #endregion
        
    }
}