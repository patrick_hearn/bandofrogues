﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Game
{
    [Serializable]
    public struct LogEvent
    {
        [FormerlySerializedAs("Turn")] public int turn;
        [FormerlySerializedAs("Message")] public string message;
        [FormerlySerializedAs("LogType")] public LogType logType;
    }
    
    [Serializable]
    public class Log : ScriptableObject
    {
        private TurnManager _turnManager;
        
        [FormerlySerializedAs("_logEvents")] [SerializeField]
        private List<LogEvent> logEvents = new List<LogEvent>();
        public List<LogEvent> LogEvents => logEvents;
        
        public Log(TurnManager turnManager)
        {
            _turnManager = turnManager;
        }
        
        public void AddLogEvent(LogEvent logEvent)
        {
            logEvents.Add(logEvent);
        }
        
        public void Clear()
        {
            logEvents.Clear();
        }
        
        public void Write(string message, LogType logType = LogType.Log)
        {
            logEvents.Add(new LogEvent
            {
                turn = _turnManager.TurnCount,
                message = message,
                logType = logType
            });
            Logging.Log(message);
        }
    }
    

    public static class Logging 
    {
        
        public static void Log(string message)
        {
            Debug.Log(message);
        }
        
        
    }
}