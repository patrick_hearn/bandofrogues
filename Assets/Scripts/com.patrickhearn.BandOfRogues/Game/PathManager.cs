﻿using System.Collections.Generic;
using System.Linq;
using com.patrickhearn.BandOfRogues.Actors;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Map;
using com.patrickhearn.BandOfRogues.Map.Pathfinding;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Game
{
    public class PathManager : SingletonBehaviour<PathManager>, IPathManager
    {
        private ActorManager _actorManager;
        private ITileMap TileMap => TileMapManager.Instance.TileMap;
        
        private IPathfinder<Vector2Int> _pathfinder;
        public bool AllowDiagonal { get; set; }

        public PathManager(ActorManager actorManager)
        {
            _actorManager = actorManager;
        }
        
        
        private void Start()
        {
            _pathfinder = new AStarPathfinder();
        }

        private bool[,] GenerateWalkableMap()
        {
            // Get walkable tiles from the tile map
            var walkableTiles = TileMap.GetWalkableGrid();
            bool[,] walkableMap = new bool[walkableTiles.GetLength(0), walkableTiles.GetLength(1)];

            // Combine walkable tiles and actor positions
            for (var i = 0; i < walkableTiles.GetLength(0); i++)
            {
                for (var j = 0; j < walkableTiles.GetLength(1); j++)
                {
                    var actor = _actorManager.GetActorAt(new Vector2Int(i, j));
                    if (actor != null && actor.IsBlocking)
                    {
                        walkableMap[i, j] = false;
                    }else
                    {
                        walkableMap[i, j] = walkableTiles[i, j] == 1;
                    }
                }
            }
            return walkableMap;
        }

        private float[,] GenerateCostMap()
        {
            return TileMap.GetCostGrid();
        }

        public List<Vector2Int> GetPath(Vector2Int start, Vector2Int end, bool allowDiagonal = false)
        {
            var walkableMap = GenerateWalkableMap();
            var costMap = GenerateCostMap();
            _pathfinder.SetWalkableGrid(walkableMap);
            _pathfinder.SetCostGrid(costMap);
            _pathfinder.AllowDiagonal = allowDiagonal;

            var path = _pathfinder.FindPath(start, end);
            return path.ToList();
        }

        public bool CanMoveTo(Vector2Int position)
        {
            var actor = _actorManager.GetActorAt(position);
            return TileMap.IsWalkable(position) && (actor == null || !actor.IsBlocking);
        }
    }
}