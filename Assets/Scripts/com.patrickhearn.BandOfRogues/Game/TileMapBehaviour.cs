﻿using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Map;
using com.patrickhearn.BandOfRogues.Map.Tiles;
using UnityEngine;
using UnityEngine.Serialization;


namespace com.patrickhearn.BandOfRogues.Map
{
    public class TileMapBehaviour : MonoBehaviour
    {
        public static TileMapBehaviour Instance;
        [FormerlySerializedAs("TileDefinitions")] public List<TileDefinition> tileDefinitions;
        private Dictionary<int, GameObject> _tileDictionary;
        public GameObject[,] GridObjectArray;
        public RoomGenerator RoomGenerator;
        public TileMap tileMap;
        public static TileMap TileMap => !Instance ? null : Instance.tileMap;
        public int[,] GridArray { get; private set; }
        public int Width => tileMap.Width;
        public int Height => tileMap.Height;
        public void Awake()
        {
            Instance = this;
        }
        public Tile GetTileAt(Vector2Int position)
        {
            return tileMap.GetItem(position) as Tile;
        }
        public void SetTileAt(Vector2Int position, Tile tile)
        {
            tileMap.SetItem(position, tile);
        }
        
        // public void Initialize()
        // {
        //     PopulateTileDictionary();
        //     tileMap = new TileMap(Width, Height);
        //     roomGenerator ??= ScriptableObject.CreateInstance<RoomGenerator>();
        //     var map = roomGenerator.GenerateMap(Width, Height);
        //     map = TileMapHelper.MapTilesTypes(map, new List<ETileType>() { ETileType.Wall ,ETileType.Floor});
        //     tileMap.SetTiles(map);
        //     InstantiateGrid();
        // }
        
        private void PopulateTileDictionary()
        {
            _tileDictionary = new Dictionary<int, GameObject>();
            foreach (var tileDefinition in tileDefinitions)
            {
                _tileDictionary.Add(tileDefinition.TileValue, tileDefinition.prefab);
            }
        }
        
        private void InstantiateGrid()
        {
            GridObjectArray = new GameObject[Width, Height];
            for (var x = 0; x < Width; x++)
            {
                for (var y = 0; y < Height; y++)
                {
                    var tile = tileMap.GetTileIndex(x, y);
                    var tilePrefab = _tileDictionary[tile];
                    var tileObject = Instantiate(tilePrefab, new Vector3(x, y, 0), Quaternion.identity);
                    tileObject.transform.parent = transform;
                    GridObjectArray[x, y] = tileObject;
                }
            }
        }
        
        public void ClearGrid()
        {
            if (GridObjectArray == null) return;
            foreach (var tile in GridObjectArray)
            {
                Destroy(tile);
            }
        }
       
        
    }
}