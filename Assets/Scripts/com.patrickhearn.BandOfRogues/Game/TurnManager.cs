﻿using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Actors;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Game
{
    public class TurnManager : ITurnManager
    {        
        
        public delegate void TurnEvent(int turnCount);

        public event TurnEvent TurnStarted;
        public event TurnEvent TurnEnded;
        
        private IActor _currentActor;
        
        private ActorManager _actorManager;
        
        public TurnManager(ActorManager actorManager)
        {
            _actorManager = actorManager;
        }

        [SerializeField] private float tickDelta;

        [SerializeField] private int turnCount;
        public int TurnCount => turnCount;
        public void Tick()
        {
            turnCount++;
            TurnStarted?.Invoke(turnCount);
            _actorManager.Tick();
            TurnEnded?.Invoke(turnCount);
        }

        public int GetTurnCount() => turnCount;

        public float GetTickDelta() => tickDelta;
        public IActor GetCurrentActor() => _currentActor;

    }
}