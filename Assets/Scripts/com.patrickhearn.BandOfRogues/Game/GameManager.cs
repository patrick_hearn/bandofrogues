using System.Collections;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Map;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Game
{
    public class GameManager :  ITickable, IStatefulEnumerated<GameStatesEnum>
    {
        [FormerlySerializedAs("AutoTick")] public bool autoTick = true;
        [FormerlySerializedAs("IsPaused")] public bool isPaused = true;
        [FormerlySerializedAs("TickRate")] public float tickRate = 10f;
        public TileMap TileMap => TileMapManager.Instance.TileMap;


        [FormerlySerializedAs("ActorPrefab")] public GameObject actorPrefab;
        [FormerlySerializedAs("MobPrefab")] public GameObject mobPrefab;
        [FormerlySerializedAs("ItemPrefab")] public GameObject itemPrefab;

        // Events
        public delegate void GameEvent();

        public event GameEvent GameSetup;
        public event GameEvent GameStarted;
        public event GameEvent GameEnded;
        
        private float _lastTickTime;
        private TurnManager _turnManager;
        
        public GameManager(TurnManager turnManager)
        {
            _turnManager = turnManager;
        }
        
        // private void Start()
        // {
        //     GameSetup?.Invoke();
        //     if (autoTick)
        //         StartCoroutine(StartGame());
        // }

        IState IStateful.CurrentState => CurrentState;

        public void ChangeState(GameState newState)
        {
            if(newState == CurrentState) return;
            if(newState == null) return;
            CurrentState?.Exit();
            CurrentState = newState;
            CurrentState.Enter();
        }

        public GameState CurrentState { get; private set; }

        public void ChangeState(IState newState)
        {
            ChangeState(newState as GameState);
        }


        public void Update()
        {
            
            if((autoTick && Time.time - _lastTickTime > 1 / tickRate) 
               || Input.GetKeyDown(KeyCode.Space)) 
            {
                Tick();
            }
            
        }
        
        private IEnumerator StartGame()
        {
            yield return new WaitForSeconds(1);
            GameStarted?.Invoke();
        }
        
        public void EndGame()
        {
            GameEnded?.Invoke();
        }

        public void Tick()
        {
            if (isPaused) return;
            _turnManager.Tick();
            _lastTickTime = Time.time;
        }

        public GameStatesEnum CurrentStateEnum => CurrentState.StateEnum;
        public void ChangeState(GameStatesEnum newState)
        {
            throw new System.NotImplementedException();
        }

        public GameStatesFactory StateFactory { get; }
    }

// #if UNITY_EDITOR
//     [UnityEditor.CustomEditor(typeof(GameManager))]
//     public class GameManagerEditor : UnityEditor.Editor
//     {
//         private TurnManager _turnManager;
//         
//         public GameManagerEditor(TurnManager turnManager)
//         {
//             _turnManager = turnManager;
//         }
//         public override void OnInspectorGUI()
//         {
//             DrawDefaultInspector();
//             var gameManager = (GameManager) target;
//             if (GUILayout.Button("Do Turn"))
//             {
//                 _turnManager.Tick();
//             }
//         }
//     }
//     #endif
}
