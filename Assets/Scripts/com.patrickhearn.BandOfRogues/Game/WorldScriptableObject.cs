﻿using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Game
{
    /// <summary>
    /// A scriptable object that contains a world.
    /// </summary>
    [CreateAssetMenu(menuName = "Band of Rogues/World")]
    public class WorldScriptableObject : ScriptableObject
    {
        [FormerlySerializedAs("World")] public DungeonWorld dungeonWorld;
    }
}