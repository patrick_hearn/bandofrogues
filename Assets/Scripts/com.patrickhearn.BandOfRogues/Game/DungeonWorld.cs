﻿using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Actors;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Map;

namespace com.patrickhearn.BandOfRogues.Game
{
    /// <summary>
    /// A world, which is a tile map and a list of actors.
    /// </summary>
    [System.Serializable]
    public class DungeonWorld : IDungeonWorld, IWorld
    {
        private TileMap _tileMap;
        public ITileMap TileMap => _tileMap;
        private List<Actor> _actors;
        public List<IActor> Actors => _actors.ConvertAll(actor => (IActor) actor);
        
        /// <summary>
        /// Constructor for a world.
        /// </summary>
        /// <param name="tileMap"></param>
        /// <param name="actors"></param>
        public DungeonWorld(TileMap tileMap, List<Actor> actors)
        {
            _tileMap = tileMap;
            _actors = actors;
        }
    }
}