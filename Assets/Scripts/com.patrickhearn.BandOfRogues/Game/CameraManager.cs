using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Game
{
    public class CameraManager 
    {
        [FormerlySerializedAs("MainCamera")] public Camera mainCamera;
        private GameManager _gameManager;
        
        public CameraManager(GameManager gameManager)
        {
            _gameManager = gameManager;
        }
        
        // public Camera UICamera;
        // public Camera MapCamera;

        // Start is called before the first frame update
        void Start()
        {
            if (!mainCamera)
                CreateMainCamera();
        }
        
        
        [ContextMenu("Create Main Camera")]
        public void CreateMainCamera()
        {
            mainCamera = CreateCameraForGrid(_gameManager.TileMap);
        }

        private static Camera CreateCameraForGrid(ITileMap tileMap)
        {
            var width = tileMap.Size.x;
            var height = tileMap.Size.y;
            var camera = new GameObject("MapCamera", typeof(Camera)).GetComponent<Camera>();
            camera.transform.position = new Vector3(width / 2f, height / 2f, -10);
            camera.orthographic = true;
            camera.orthographicSize = height / 2f;
            camera.clearFlags = CameraClearFlags.SolidColor;
            camera.backgroundColor = Color.black;
            camera.cullingMask = 1 << 6;
            camera.depth = 1;
            return camera;
        } 
    }
}
