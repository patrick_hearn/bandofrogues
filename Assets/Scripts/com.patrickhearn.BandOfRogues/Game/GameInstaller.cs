﻿using com.patrickhearn.BandOfRogues.Actors;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Map;
using com.patrickhearn.BandOfRogues.Map.Tiles;
using com.patrickhearn.BandOfRogues.MapGen;
using Zenject;

namespace com.patrickhearn.BandOfRogues.Game
{
    public class GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<ActorManager>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<TurnManager>().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<GameManager>().AsSingle().NonLazy();
            
            Container.Bind<IMapGenerator<MapGenerationParameters>>().To<MapGenerator>().AsSingle();
            Container.Bind<ITileMapFactory<TileMap, Tile>>().To<TileMapFactory>().AsSingle();
            Container.Bind<IMobGenerator>().To<MobGenerator>().AsSingle();
        }
    }

}