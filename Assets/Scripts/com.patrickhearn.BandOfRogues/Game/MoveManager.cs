﻿// using System.Collections.Generic;
// using System.Linq;
// using com.patrickhearn.BandOfRogues.Interfaces;
// using com.patrickhearn.BandOfRogues.Map;
// using UnityEngine;
//
// namespace com.patrickhearn.BandOfRogues.Game
// {
//     public class MoveManager : SingletonBehaviour<MoveManager>, IMoveManager
//     {
//         public bool TryMoveActor(IActor actor, Vector2Int position)
//         {
//             // Check if the new position is adjacent to the actor's current position
//             var distance = Vector2Int.Distance(actor.Position, position);
//             return !(distance > 1.0f);
//         }
//
//         public bool TryPlaceActor(IActor actor, Vector2Int position)
//         {
//             var tileMap = TileMapManager.TileMap;
//             
//             // Check if the tile is walkable and not occupied by another actor
//             if (tileMap.IsWalkable(position) && !GameManager.Instance.IsTileOccupied(position))
//                 return GameManager.Instance.PlaceActor(actor, position);
//             Debug.Log($"Tile {position} is not walkable or occupied");
//             return false;
//         }
//
//         public bool TryMoveActor(IActor actor, int x, int y)
//         {
//             return TryMoveActor(actor, new Vector2Int(x, y));
//         }
//
//         public static Vector2Int GetValidMoveTarget(Vector2Int mover, Vector2Int target, int minDistance,
//             int maxDistance)
//         {
//             // Finds a valid cell which is within the specified distance range from the target cell.
//             // The valid cell is the closest cell to the mover cell.
//             // Returns null if no valid cell is found.
//
//             var tileMap = TileMapManager.TileMap;
//             var validCells = tileMap.GetCellsInDistanceRange(target, minDistance, maxDistance)
//                 .Where(tileMap.IsWalkable).ToList();
//             if (!validCells.Any())
//             {
//                 Debug.Log($"No valid cells found within range {minDistance}-{maxDistance} from {target}");
//                 return mover;
//
//             }
//
//             Debug.Log($"Found {validCells.Count} valid cells within range {minDistance}-{maxDistance} from {target}");
//             Debug.Log(
//                 $"Which are: {string.Join(", ", validCells)}, with mover at {mover} the best cell is {validCells.OrderBy(cell => Vector2Int.Distance(mover, cell)).First()}");
//
//             return validCells.OrderBy(cell => Vector2Int.Distance(mover, cell)).First();
//
//         }
//         
//
//     }
// }