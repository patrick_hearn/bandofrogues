using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.Game
{
    public class InitializationState : GameState
    {
        public InitializationState(GameManager gameManager) : base(gameManager)
        {
        }

        public override void Enter()
        {
            // Implementation
        }

        public override void Execute()
        {
            // Implementation
        }

        public override void Exit()
        {
            // Implementation
        }

        public override GameStatesEnum StateEnum => GameStatesEnum.Initialization;

        public override bool CanTransitionTo(GameStatesEnum state)
        {
            return state switch
            {
                GameStatesEnum.Initialization => false,
                GameStatesEnum.Pregame => false,
                GameStatesEnum.Running => true,
                GameStatesEnum.Ended => false,
                GameStatesEnum.Paused => false,
                _ => false
            };
        }
    }
}