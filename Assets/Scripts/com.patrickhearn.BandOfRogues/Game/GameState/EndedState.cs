using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.Game
{
    public class EndedState : GameState
    {
        public EndedState(GameManager gameManager) : base(gameManager)
        {
        }

        public override void Enter()
        {
            // Implementation
        }

        public override void Execute()
        {
            // Implementation
        }

        public override void Exit()
        {
            // Implementation
        }

        public override GameStatesEnum StateEnum => GameStatesEnum.Ended;

        public override bool CanTransitionTo(GameStatesEnum state)
        {
            return false;
        }
    }
}