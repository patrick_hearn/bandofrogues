namespace com.patrickhearn.BandOfRogues.Game
{
    public enum GameStatesEnum
    {
        Pregame,
        Initialization,
        Running,
        Paused,
        Ended
    }
}