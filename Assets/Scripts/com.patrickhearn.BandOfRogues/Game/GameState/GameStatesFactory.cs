using System;
using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.Game
{
    /// <summary>
    /// This is a factory class that creates the various game states.
    /// </summary>
    public class GameStatesFactory : IStateFactory<GameStatesEnum>
    {
        /// <summary>
        /// The game manager
        /// </summary>
        
        private readonly GameManager _gameManager;
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="gameManager"></param>
        public GameStatesFactory(GameManager gameManager)
        {
            _gameManager = gameManager;
        }
        /// <summary>
        /// Get the state
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        
        public GameState GetState(GameStatesEnum state)
        {
            return state switch
            {
                GameStatesEnum.Pregame => new PregameState(_gameManager),
                GameStatesEnum.Initialization => new InitializationState(_gameManager),
                GameStatesEnum.Running => new RunningState(_gameManager),
                GameStatesEnum.Paused => new PausedState(_gameManager),
                GameStatesEnum.Ended => new EndedState(_gameManager),
                _ => throw new ArgumentOutOfRangeException(nameof(state), state, null)
            };
        }
        
        /// <summary>
        /// Create the state
        /// </summary>
        /// <returns></returns>
        public GameStatesEnum Create()
        {
            return GameStatesEnum.Pregame;
        }
    }
}