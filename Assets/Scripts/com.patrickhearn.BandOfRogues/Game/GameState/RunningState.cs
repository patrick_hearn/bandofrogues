using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.Game
{
    public class RunningState : GameState
    {
        public RunningState(GameManager gameManager) : base(gameManager)
        {
        }

        public override void Enter()
        {
            // Implementation
        }

        public override void Execute()
        {
            // Implementation
        }

        public override void Exit()
        {
            // Implementation
        }

        public override bool CanTransitionTo(GameStatesEnum state)
        {
            return state switch
            {
                GameStatesEnum.Initialization => false,
                GameStatesEnum.Pregame => false,
                GameStatesEnum.Running => false,
                GameStatesEnum.Ended => true,
                GameStatesEnum.Paused => true,
                _ => false
            };
        }
        
        public override GameStatesEnum StateEnum => GameStatesEnum.Running;
        
    }
}