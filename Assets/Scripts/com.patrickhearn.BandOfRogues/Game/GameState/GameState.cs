using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.Game
{
    public abstract class GameState : IStateEnumerated<GameStatesEnum>
    {
        protected GameManager GameManager;

        protected GameState(GameManager gameManager)
        {
            GameManager = gameManager;
        }

        public abstract void Enter();
        public abstract void Execute();
        public abstract void Exit();

        public bool CanTransitionTo(IState state)
        {
            return CanTransitionTo(((GameState) state).StateEnum);
        }
        public abstract GameStatesEnum StateEnum { get; }
        public abstract bool CanTransitionTo(GameStatesEnum state);
    }
}