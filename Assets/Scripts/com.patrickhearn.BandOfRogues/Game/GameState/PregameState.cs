using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.Game
{
    public class PregameState : GameState
    {
        public PregameState(GameManager gameManager) : base(gameManager)
        {
        }

        public override void Enter()
        {
            // Implementation
        }

        public override void Execute()
        {
            // Implementation
        }

        public override void Exit()
        {
            // Implementation
        }

        public override bool CanTransitionTo(GameStatesEnum state)
        {
            return state switch
            {
                GameStatesEnum.Initialization => true,
                GameStatesEnum.Pregame => false,
                GameStatesEnum.Running => false,
                GameStatesEnum.Ended => false,
                GameStatesEnum.Paused => false,
                _ => false
            };
        }
        
        public override GameStatesEnum StateEnum => GameStatesEnum.Pregame;
        
    }
}