using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Map;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Game
{
    public class DungeonWorldGenerationParameters : ScriptableObject, IGenerationParameters, IWorldGenerationParameters
    {
        public MapGenerationParameters MapGenerationParameters;
        public List<MobGenerationParameters> MobGenerationParameters;
        
        public Dictionary<string, object> Parameters => new Dictionary<string, object>()
        {
            {"MapGenerationParameters", MapGenerationParameters},
            {"MobGenerationParameters", MobGenerationParameters}
        };
    }

    public class MobGenerationParameters : ScriptableObject, IGenerationParameters
    {
    }
    
    
}