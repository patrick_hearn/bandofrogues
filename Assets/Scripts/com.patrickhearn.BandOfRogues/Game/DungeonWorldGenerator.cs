﻿using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Actors;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Map;
using com.patrickhearn.BandOfRogues.Map.Tiles;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Game
{
    

    
    
    /// <summary>
    /// Generates a world based on the parameters provided.
    /// </summary>
    public class DungeonWorldGenerator : MonoBehaviour, IWorldGenerator<DungeonWorld, DungeonWorldGenerationParameters>
    {
        [SerializeField] private DungeonWorldGenerationParameters dungeonWorldGenerationParameters;
        
        
        private readonly IMapGenerator<MapGenerationParameters> _mapGenerator;
        private readonly ITileMapFactory<TileMap, Tile> _tileMapFactory;
        private readonly IMobGenerator _mobGenerator;
        
        
        
        // public WorldGenerator(IMapGenerator mapGenerator, ITileMapFactory tileMapFactory, IMobGenerator mobGenerator)
        // {
        //     _mapGenerator = mapGenerator;
        //     _tileMapFactory = tileMapFactory;
        //     _mobGenerator = mobGenerator;
        // }


        /// <summary>
        /// Generates a world
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public DungeonWorld Generate()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Generates a world based on the parameters provided.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DungeonWorld Generate(DungeonWorldGenerationParameters parameters)
        {
            var map = _mapGenerator.Generate(parameters.MapGenerationParameters);
            var tileMap = _tileMapFactory.Create(map, parameters.MapGenerationParameters.TileMapper);
            var actors = new List<Actor>();
            foreach (var mobGenerationParameter in parameters.MobGenerationParameters)
            {
                var mob = _mobGenerator.Generate(mobGenerationParameter as IMobGenerationParameters);
                actors.Add(mob as Mob);
                
            }
            return new DungeonWorld(tileMap, actors);
        }
    }
}