﻿using com.patrickhearn.BandOfRogues.Actors;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Game
{
    public class Logger : MonoBehaviour
    {
        public static Logger Instance;
        [FormerlySerializedAs("Log")] public Log log;
        public void Awake()
        {
            Instance = this;
        }
        
        public void Start()
        {
            log = ScriptableObject.CreateInstance<Log>();
            // foreach (var mob in GameManager.Instance.Mobs)
            // {
            //     mob.MobAttacked += OnMobAttacked;
            //     mob.MobDamaged += OnMobDamaged;
            //     mob.MobDied += OnMobDied;
            //     
            // }
            //
            
        }

        private void OnMobDamaged(Mob mob, float damage, ICombatant attacker)
        {
            log.Write($"{mob.Name} took {damage} damage from {attacker.Name}");
        }

        private void OnMobAttacked(Mob mob, Actor target)
        {
            log.Write($"{mob.Name} was attacked {target.Name}");
        }
        
        private void OnMobDied(Mob mob)
        {
            log.Write($"{mob.Name} died");
        }
        
    }
}