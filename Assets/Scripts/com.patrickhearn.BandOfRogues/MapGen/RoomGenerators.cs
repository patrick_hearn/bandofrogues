﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;
using Rect = UnityEngine.Rect;

namespace com.patrickhearn.BandOfRogues.MapGen
{
    public class RectangularRoom : IRoom
    {
        private Rect _bounds;

        public RectangularRoom(Vector2Int position, Vector2Int size)
        {
            _bounds = new Rect(position.x, position.y, size.x, size.y);
        }

        public bool IsInside(Vector2Int position)
        {
            return _bounds.Contains(position);
        }

        Rect IRoom.GetBounds()
        {
            return _bounds; 
        }

        public List<Vector2Int> GetIncludedPositions()
        {
            var positions = new List<Vector2Int>();
            for (var x = (int)_bounds.xMin; x < _bounds.xMax; x++)
            {
                for (var y = (int)_bounds.yMin; y < _bounds.yMax; y++)
                {
                    positions.Add(new Vector2Int(x, y));
                }
            }
            
            return positions;
        }

        public List<Vector2Int> GetBoundaryPositions()
        {
            var positions = new List<Vector2Int>();
            for (var x = (int)_bounds.xMin; x < _bounds.xMax; x++)
            {
                positions.Add(new Vector2Int(x, (int)_bounds.yMin));
                positions.Add(new Vector2Int(x, (int)_bounds.yMax - 1));
            }
            for (var y = (int)_bounds.yMin; y < _bounds.yMax; y++)
            {
                positions.Add(new Vector2Int((int)_bounds.xMin, y));
                positions.Add(new Vector2Int((int)_bounds.xMax - 1, y));
            }
            
            positions.RemoveAll(position => _bounds.Contains(position));
            return positions;
        }

        public List<Vector2Int> GetInternalPositions()
        {
            var positions = new List<Vector2Int>();
            for (int x = (int)_bounds.xMin + 1; x < _bounds.xMax - 1; x++)
            {
                for (int y = (int)_bounds.yMin + 1; y < _bounds.yMax - 1; y++)
                {
                    positions.Add(new Vector2Int(x, y));
                }
            }
            
            return positions;
        }

        public Rect GetBounds()
        {
            return _bounds;
        }

        
    }

    public class CircularRoom : IRoom
    {
        private Vector2Int _center;
        private readonly int _radius;

        public CircularRoom(Vector2Int center, int radius)
        {
            _center = center;
            _radius = radius;
        }

        public bool IsInside(Vector2Int position)
        {
            return Vector2Int.Distance(position, _center) <= _radius;
        }

        public Rect GetBounds()
        {
            return new Rect(_center.x - _radius, _center.y - _radius, 2 * _radius, 2 * _radius);
        }

        public List<Vector2Int> GetIncludedPositions()
        {
            var positions = new List<Vector2Int>();
            for (var x = _center.x - _radius; x < _center.x + _radius; x++)
            {
                for (var y = _center.y - _radius; y < _center.y + _radius; y++)
                {
                    positions.Add(new Vector2Int(x, y));
                }
            }
            
            return positions;
        }

        public List<Vector2Int> GetBoundaryPositions()
        {
            return GetIncludedPositions().Where(position => Math.Abs(Vector2Int.Distance(position, _center) - _radius) < 1).ToList();
        }

        public List<Vector2Int> GetInternalPositions()
        {
            var positions = new List<Vector2Int>();
            for (int x = _center.x - _radius + 1; x < _center.x + _radius - 1; x++)
            {
                for (int y = _center.y - _radius + 1; y < _center.y + _radius - 1; y++)
                {
                    positions.Add(new Vector2Int(x, y));
                }
            }
            
            return positions;
        }
    }

    // public class NoiseRoom : IRoom
    // {
    //     private PerlinNoise noise;
    //     private Vector2Int center;
    //     private int radius;
    //     
    //     private List<Vector2Int> _includedPositions;
    //
    //     public NoiseRoom(Vector2Int center, int radius, float noiseScale)
    //     {
    //         this.center = center;
    //         this.radius = radius;
    //         noise = new PerlinNoise(/* seed */ 0, noiseScale);
    //         
    //         _includedPositions = new List<Vector2Int>();
    //         
    //     }
    //
    //     public bool IsInside(Vector2Int position)
    //     {
    //         float dist = Vector2Int.Distance(position, center) / radius;
    //         return noise.Sample(position.x, position.y) > dist;
    //     }
    //
    //     public Rect GetBounds()
    //     {
    //         return new Rect(center.x - radius, center.y - radius, 2 * radius, 2 * radius);
    //     }
    //
    //     public List<Vector2Int> GetIncludedPositions()
    //     {
    //         throw new NotImplementedException();
    //     }
    //
    //     public List<Vector2Int> GetBoundaryPositions()
    //     {
    //         throw new NotImplementedException();
    //     }
    //
    //     public List<Vector2Int> GetInternalPositions()
    //     {
    //         throw new NotImplementedException();
    //     }
    //
    //     // The rest of the methods would be implemented similarly...
    // }
    

}