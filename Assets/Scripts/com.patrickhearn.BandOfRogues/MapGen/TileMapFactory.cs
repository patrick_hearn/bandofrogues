﻿using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Map;
using com.patrickhearn.BandOfRogues.Map.Tiles;

namespace com.patrickhearn.BandOfRogues.MapGen
{
    public class TileMapFactory : ITileMapFactory<TileMap, Tile>
    {
        public TileMap Create(int[,] mapRepresentation, Dictionary<int, Tile> tileMapper)
        {
            var tileMap = new TileMap(mapRepresentation.GetLength(0), mapRepresentation.GetLength(1));
            for (var x = 0; x < mapRepresentation.GetLength(0); x++)
            {
                for (var y = 0; y < mapRepresentation.GetLength(1); y++)
                {
                    tileMap.SetTile(x, y, tileMapper[mapRepresentation[x, y]]);
                }
            }
            
            return tileMap;
        }
    }
}