﻿using System;
using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Map
{
    // public class DungeonGenerator : IGenerator<int[,] DungeonGenerationParameters>
    // {
    //     private IRoomPlacer _roomPlacer;
    //     private ICorridorPlacer _corridorPlacer;
    //     private IMapCarver _mapCarver;
    //     private IEntryExitPlacer _entryExitPlacer;
    //
    //     public DungeonGenerator(IRoomPlacer roomPlacer, ICorridorPlacer corridorPlacer, IMapCarver mapCarver, IEntryExitPlacer entryExitPlacer)
    //     {
    //         _roomPlacer = roomPlacer;
    //         _corridorPlacer = corridorPlacer;
    //         _mapCarver = mapCarver;
    //         _entryExitPlacer = entryExitPlacer;
    //     }
    //
    //     public int[,] GenerateMap(int[,] initialMap, MapGenParams parameters)
    //     {
    //         // Place rooms.
    //         var rooms = _roomPlacer.PlaceRooms(parameters.RoomCount, parameters.MinRoomSize, parameters.MaxRoomSize, initialMap.GetLength(0), initialMap.GetLength(1));
    //
    //         // Connect rooms.
    //         var corridors = _corridorPlacer.ConnectRooms(rooms);
    //
    //         // Carve rooms and corridors.
    //         var map = _mapCarver.CarveRoomsAndCorridors(initialMap, rooms, corridors);
    //
    //         // Place entry and exit points.
    //         var (entry, exit) = _entryExitPlacer.PlaceEntryAndExit(map);
    //
    //         // Mark entry and exit points on the map.
    //         map[entry.x, entry.y] = 2; // 2 represents entry.
    //         map[exit.x, exit.y] = 3; // 3 represents exit.
    //
    //         return map;
    //     }
    //
    //     public int[,] GenerateMap(int width, int height)
    //     {
    //         var initialMap = new int[width, height];
    //         return GenerateMap(initialMap, new MapGenParams());
    //     }
    // }
    public abstract class Corridor : ICorridor
    {
        protected readonly List<Vector2Int> Positions;
        
        
        public List<Vector2Int> GetPositions() => Positions;

        public Vector2Int GetStart() => Positions[0];

        public Vector2Int GetEnd() => Positions[^1];
        
        public Corridor()
        {
            Positions = new List<Vector2Int>();
            
        }

        public Corridor(Vector2Int start, Vector2Int end)
        {
            Positions = new List<Vector2Int>();
            Positions.Add(start);
            Positions.Add(end);
        }

        protected virtual void Create(Vector2Int start, Vector2Int end)
        {
            throw new NotImplementedException();
        }
    }

    public class SimpleCorridor : Corridor
    {
        protected override void Create(Vector2Int start, Vector2Int end)
        {
            
            var x = start.x;
            var y = start.y;
            while (x != end.x)
            {
                x += Math.Sign(end.x - x);
                Positions.Add(new Vector2Int(x, y));
            }

            while (y != end.y)
            {
                y += Math.Sign(end.y - y);
                Positions.Add(new Vector2Int(x, y));
            }
        }
    }

    public struct MapGenParams
    {
        public int RoomCount { get; set; }
        public int MinRoomSize { get; set; }
        public int MaxRoomSize { get; set; }
    }

}