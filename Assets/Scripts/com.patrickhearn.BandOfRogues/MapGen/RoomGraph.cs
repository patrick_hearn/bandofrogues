﻿using System.Collections.Generic;

namespace com.patrickhearn.BandOfRogues.Map
{
    public class RoomGraph
    {
        public List<Room> Nodes { get; set; }
        public Dictionary<Room, List<(Room room, int distance)>> Edges { get; set; }

        public RoomGraph(List<Room> nodes)
        {
            Nodes = nodes;
            Edges = new Dictionary<Room, List<(Room room, int distance)>>();
            foreach (var node in Nodes)
            {
                Edges[node] = new List<(Room room, int distance)>();
            }
        }

        public void AddEdge(Room node1, Room node2, int distance)
        {
            Edges[node1].Add((node2, distance));
            Edges[node2].Add((node1, distance));
        }
    }
}