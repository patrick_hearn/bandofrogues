﻿using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Map.Tiles;

namespace com.patrickhearn.BandOfRogues.Map
{
    
    public abstract class MapGenerator : IMapGenerator<MapGenerationParameters>
    {
        public int[,] Generate()
        {
            throw new System.NotImplementedException();
        }

        public int[,] Generate(MapGenerationParameters parameters)
        {
            throw new System.NotImplementedException();
        }

        public abstract int[,] GenerateMap(int width, int height);
    }

    public class MapGenerationParameters : IMapGenerationParameters
    {
        public MapGenerationParameters(int width, int height, int[,] baseMap)
        {
            Width = width;
            Height = height;
            BaseMap = baseMap;
        }

        public Dictionary<int, Tile> TileMapper { get; }

        public int Width { get; }
        public int Height { get; }
        public int[,] BaseMap { get; }
    }
}