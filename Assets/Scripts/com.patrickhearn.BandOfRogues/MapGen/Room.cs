﻿using System.Collections.Generic;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Map
{
    public class Room
    {
        public int X;
        public int Y;
        public int Width;
        public int Height;
        
        public int Left => X;
        public int Right => X + Width;
        public int Top => Y;
        public int Bottom => Y + Height;
        
        public int CenterX => X + Width / 2;
        public int CenterY => Y + Height / 2;

        public IEnumerable<Vector2Int> Perimeter
        {
            get
            {
                var perimeter = new List<Vector2Int>();
                for (var x = Left; x <= Right; x++)
                {
                    perimeter.Add(new Vector2Int(x, Top));
                    perimeter.Add(new Vector2Int(x, Bottom));
                }
                for (var y = Top; y <= Bottom; y++)
                {
                    perimeter.Add(new Vector2Int(Left, y));
                    perimeter.Add(new Vector2Int(Right, y));
                }
                return perimeter;
            }
        }

        public bool Overlaps(Room other)
        {
            return Left <= other.Right && Right >= other.Left && Top <= other.Bottom && Bottom >= other.Top;
        }
        
        public bool Overlaps(List<Room> others)
        {
            foreach (var other in others)
            {
                if (Overlaps(other))
                {
                    return true;
                }
            }
            return false;
        }
        
        public bool Overlaps(List<Room> others, int padding)
        {
            foreach (var other in others)
            {
                if (Overlaps(other, padding))
                {
                    return true;
                }
            }
            return false;
        }
        
        public bool Overlaps(Room other, int padding)
        {
            return Left - padding <= other.Right && Right + padding >= other.Left && Top - padding <= other.Bottom && Bottom + padding >= other.Top;
        }
        
        public bool Overlaps(List<Room> others, int paddingX, int paddingY)
        {
            foreach (var other in others)
            {
                if (Overlaps(other, paddingX, paddingY))
                {
                    return true;
                }
            }
            return false;
        }
        
        public bool Overlaps(Room other, int paddingX, int paddingY)
        {
            return Left - paddingX <= other.Right && Right + paddingX >= other.Left && Top - paddingY <= other.Bottom && Bottom + paddingY >= other.Top;
        }
        
        public Room(int x, int y, int width, int height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public int GetDistance(Room otherRoom)
        {
            // Manhattan distance
            return Mathf.Abs(CenterX - otherRoom.CenterX) + Mathf.Abs(CenterY - otherRoom.CenterY);
        }
        
        public int GetDistance(Vector2Int otherRoom)
        {
            // Manhattan distance
            return Mathf.Abs(CenterX - otherRoom.x) + Mathf.Abs(CenterY - otherRoom.y);
        }
    }
}