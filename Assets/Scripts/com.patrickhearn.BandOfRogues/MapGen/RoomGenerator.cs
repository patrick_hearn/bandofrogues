﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Map
{
    // [CreateAssetMenu(menuName = "Band of Rogues/Map/Room Generator")]
    // public class RoomGenerator : ScriptableObject, IMapGenerator
    // {
    //     public int MinRoomSize= 3;
    //     public int MaxRoomSize = 10;
    //     public int MaxRooms = 10;
    //     public int MinRooms = 5;
    //     public bool OverlappingRooms = true;
    //     public bool ConnectedRooms;
    //     public float CorridorEccentricity;
    //     public int MinCorridorLength;
    //     public int MaxCorridorLength;
    //     public int MinCorridorWidth;
    //     public int MaxCorridorWidth;
    //
    //
    //
    //     public int[,] GenerateMap(int width, int height)
    //     {
    //         var (rooms, map) = GenerateRooms(width, height);
    //         if (ConnectedRooms)
    //         {
    //             map = ConnectAllRooms(rooms, map);
    //         }
    //         return map;
    //     }
    //     public int[,] ConnectAllRooms(List<Room> rooms, int[,] map)
    //     {
    //         var graph = new RoomGraph(rooms);
    //         foreach (var room1 in rooms)
    //         {
    //             foreach (var room2 in rooms.Where(x => x != room1))
    //             {
    //                 var distance = Math.Abs(room1.CenterX - room2.CenterX) + Math.Abs(room1.CenterY - room2.CenterY); // Manhattan distance
    //                 graph.AddEdge(room1, room2, distance);
    //             }
    //         }
    //
    //         var newMap = (int[,])map.Clone();
    //
    //         var mst = PrimsAlgorithm(graph, rooms.First());
    //
    //         foreach (var (startRoom, endRoom) in mst)
    //         {
    //             int x = startRoom.CenterX;
    //             int y = startRoom.CenterY;
    //     
    //             while (x != endRoom.CenterX)
    //             {
    //                 newMap[y, x] = 1;
    //                 x += x < endRoom.CenterX ? 1 : -1;
    //             }
    //
    //             while (y != endRoom.CenterY)
    //             {
    //                 newMap[y, x] = 1;
    //                 y += y < endRoom.CenterY ? 1 : -1;
    //             }
    //         }
    //
    //         return newMap;
    //     }
    //
    //     public List<(Room, Room)> PrimsAlgorithm(RoomGraph graph, Room startNode)
    //     {
    //         var mst = new List<(Room, Room)>();
    //         var selected = new HashSet<Room>();
    //         selected.Add(startNode);
    //         
    //         var edgeList = new SortedSet<(Room room1, Room room2, int distance)>
    //             (Comparer<(Room room1, Room room2, int distance)>.Create((x, y) => x.distance.CompareTo(y.distance)));
    //
    //         while (selected.Count < graph.Nodes.Count)
    //         {
    //             edgeList.UnionWith(graph.Edges[startNode].Where(x => !selected.Contains(x.room)).Select(x => (startNode, x.room, x.distance)));
    //
    //             var minEdge = edgeList.Min;
    //             edgeList.Remove(minEdge);
    //
    //             mst.Add((minEdge.room1, minEdge.room2));
    //             startNode = minEdge.room2;
    //             selected.Add(startNode);
    //         }
    //
    //         return mst;
    //     }
    //     // private void ConnectRooms(List<Room> rooms, int[,] map)
    //     // {
    //     //     // return;
    //     //     // We need to connect all rooms to each other
    //     //     // We can do this by finding the closest room to each room and connecting them
    //     //     // We can then remove the rooms from the list and repeat until there are no rooms left
    //     //     var connectedRooms = new List<Room>();
    //     //     var unconnectedRooms = new List<Room>(rooms);
    //     //     var currentRoom = unconnectedRooms[0];
    //     //     unconnectedRooms.RemoveAt(0);
    //     //     connectedRooms.Add(currentRoom);
    //     //     int count = 0;
    //     //     
    //     //     while (unconnectedRooms.Count > 0 && count < 100)
    //     //     {
    //     //         var closestRoom = GetClosestRoom(currentRoom, unconnectedRooms);
    //     //         Debug.Log($"Connecting room {currentRoom} to room {closestRoom}");
    //     //         ConnectRoom(currentRoom, closestRoom, map);
    //     //         
    //     //         unconnectedRooms.Remove(closestRoom);
    //     //         connectedRooms.Add(closestRoom);
    //     //         currentRoom = closestRoom;
    //     //         Debug.Log($"ConnectedRooms: {connectedRooms.Count}, UnconnectedRooms: {unconnectedRooms.Count}");
    //     //         count++;
    //     //
    //     //     }
    //     //     
    //     //     if (count >= 100)
    //     //     {
    //     //         Debug.LogError("Couldn't connect all rooms");
    //     //     }
    //     //     
    //     //     
    //     //     
    //     //     
    //     //     
    //     // }
    //     
    //     private int[,] ConnectRooms(List<Room> rooms, int[,] map)
    //     {
    //         // Copy the original map
    //         int[,] newMap = (int[,]) map.Clone();
    //
    //         for (int i = 0; i < rooms.Count - 1; i++)
    //         {
    //             Room startRoom = rooms[i];
    //             Room endRoom = rooms[i + 1];
    //
    //             // Use Manhattan distance to create corridors
    //             // Start from the center of the first room
    //             int x = startRoom.CenterX;
    //             int y = startRoom.CenterY;
    //
    //             // Create corridor along x-axis until reaching the x-center of the endRoom
    //             while (x != endRoom.CenterX)
    //             {
    //                 newMap[y, x] = 1;  // 1 represents the floor (corridor)
    //                 x += x < endRoom.CenterX ? 1 : -1; // If x is less than target, increase, otherwise decrease
    //             }
    //
    //             // Create corridor along y-axis until reaching the y-center of the endRoom
    //             while (y != endRoom.CenterY)
    //             {
    //                 newMap[y, x] = 1;  // 1 represents the floor (corridor)
    //                 y += y < endRoom.CenterY ? 1 : -1; // If y is less than target, increase, otherwise decrease
    //             }
    //         }
    //
    //         return newMap;
    //     }
    //
    //
    //     private int[,] ConnectRoom(Room currentRoom, Room closestRoom, int[,] map)
    //     {
    //         // We need to connect the two rooms with a corridor
    //         // We can do this by finding the closest point on each room's perimeter
    //         // We can then draw a line between the two points
    //         
    //         var closestPoint = GetClosestPoint(currentRoom, closestRoom);
    //         var closestPoint2 = GetClosestPoint(closestRoom, currentRoom);
    //         var corridorWidth = UnityEngine.Random.Range(MinCorridorWidth, MaxCorridorWidth);
    //         var corridorLength = UnityEngine.Random.Range(MinCorridorLength, MaxCorridorLength);
    //         var corridor = new Room(closestPoint.x, closestPoint.y, corridorLength, corridorWidth);
    //         
    //         // We need to make sure the corridor doesn't overlap any rooms
    //         // If it does, we need to move it until it doesn't
    //         int count = 0;
    //         while ((corridor.Overlaps(currentRoom, 1, 1) || corridor.Overlaps(closestRoom, 1, 1)) && count < 10000)
    //         {
    //             // We need to move the corridor
    //             // We can do this by moving it in the direction of the closest point
    //             // We can then check if it overlaps any rooms
    //             // If it does, we can move it again
    //             // We can repeat this until it doesn't overlap any rooms
    //             var direction = GetDirection(corridor, closestPoint2);
    //             corridor.X += direction.x;
    //             corridor.Y += direction.y;
    //             count++;
    //         }
    //         if (count >= 10000)
    //         {
    //             Debug.LogError("Couldn't find a place to put the corridor");
    //             // return map;
    //         }
    //         
    //         // We need to draw the corridor
    //         // We can do this by drawing a line between the two points
    //         
    //         var x = corridor.X;
    //         var y = corridor.Y;
    //         var x2 = closestPoint2.x;
    //         var y2 = closestPoint2.y;
    //         var dx = Mathf.Abs(x2 - x);
    //         var dy = Mathf.Abs(y2 - y);
    //         var sx = x < x2 ? 1 : -1;
    //         var sy = y < y2 ? 1 : -1;
    //         var err = dx - dy;
    //         
    //         count = 0;
    //         while (true && count < 1000)
    //         {
    //             map[x, y] = 1;
    //             if (x == x2 && y == y2)
    //             {
    //                 break;
    //             }
    //             var e2 = 2 * err;
    //             if (e2 > -dy)
    //             {
    //                 err -= dy;
    //                 x += sx;
    //             }
    //             if (e2 < dx)
    //             {
    //                 err += dx;
    //                 y += sy;
    //             }
    //             count++;
    //         }
    //         
    //         if (count >= 1000)
    //         {
    //             Debug.LogError("Couldn't draw the corridor");
    //         }
    //         
    //         return map;
    //     }
    //
    //     private Vector2Int GetDirection(Room corridor, Vector2Int closestPoint2)
    //     {
    //         return new Vector2Int(closestPoint2.x - corridor.X, closestPoint2.y - corridor.Y);
    //     }
    //
    //     private Vector2Int GetClosestPoint(Room currentRoom, Room closestRoom)
    //     {
    //         var closestPoint = new Vector2Int();
    //         var closestDistance = int.MaxValue;
    //         foreach (var point in currentRoom.Perimeter)
    //         {
    //             var distance = closestRoom.GetDistance(point);
    //             if (distance < closestDistance)
    //             {
    //                 closestDistance = distance;
    //                 closestPoint = point;
    //             }
    //         }
    //         return closestPoint;
    //     }
    //
    //
    //     private (List<Room>, int[,]) GenerateRooms(int width, int height)
    //     {
    //         var map = new int[width, height];
    //         var roomCount = 0;
    //         var roomSize = 0;
    //         var roomX = 0;
    //         var roomY = 0;
    //         var roomWidth = 0;
    //         var roomHeight = 0;
    //         var rooms = new List<Room>();
    //         Debug.Log($"Map: {width}x{height}, 0:0: {map[0, 0]}, {width/2}:{height/2}: {map[width/2, height/2]}");
    //         while (roomCount < MaxRooms)
    //         {
    //
    //             roomSize = UnityEngine.Random.Range(MinRoomSize, MaxRoomSize);
    //             roomX = UnityEngine.Random.Range(0, width - roomSize);
    //             roomY = UnityEngine.Random.Range(0, height - roomSize);
    //             roomWidth = UnityEngine.Random.Range(MinRoomSize, MaxRoomSize);
    //             roomHeight = UnityEngine.Random.Range(MinRoomSize, MaxRoomSize);
    //             var room = new Room(roomX, roomY, roomWidth, roomHeight);
    //             if (!OverlappingRooms && room.Overlaps(rooms))
    //             {
    //                 continue;
    //             }
    //             Debug.Log($"Room {roomCount} at {roomX}, {roomY} with size {roomWidth}x{roomHeight}");
    //             rooms.Add(room);
    //             roomCount++;
    //
    //             for (var x = roomX; x < roomX + roomWidth; x++)
    //             {
    //                 for (var y = roomY; y < roomY + roomHeight; y++)
    //                 {
    //                     if (x > 0 && x < width && y > 0 && y < height)
    //                     {
    //                         map[x, y] = 1;
    //                     }
    //                 }
    //             }
    //         }
    //
    //
    //         Debug.Log($"Map: {width}x{height}, 0:0: {map[0, 0]}, {width/2}:{height/2}: {map[width/2, height/2]}");
    //
    //         return (rooms, map);
    //     }
    //
    //     public int[,] GenerateCorridors(List<Room> rooms, int width, int height)
    //     {
    //         // Join rooms with corridors
    //         // We pick a room and then pick another room and join them with a corridor
    //         // We do this until all rooms are connected
    //
    //         var map = new int[width, height];
    //         var connectedRooms = new List<Room>();
    //         var unconnectedRooms = new List<Room>();
    //         var room = rooms[0];
    //         connectedRooms.Add(room);
    //         rooms.Remove(room);
    //         unconnectedRooms.AddRange(rooms);
    //
    //         while (unconnectedRooms.Count > 0)
    //         {
    //             var closestRoom = GetClosestRoom(room, unconnectedRooms);
    //             var corridor = GenerateCorridor(room, closestRoom);
    //             if (corridor != null)
    //             {
    //                 foreach (var point in corridor)
    //                 {
    //                     map[point.x, point.y] = 1;
    //                 }
    //             }
    //
    //             connectedRooms.Add(closestRoom);
    //             unconnectedRooms.Remove(closestRoom);
    //             room = closestRoom;
    //         }
    //
    //         return map;
    //
    //
    //
    //     }
    //
    //     private List<Vector2Int> GenerateCorridor(Room room1, Room room2)
    //     {
    //         var corridor = new List<Vector2Int>();
    //
    //         var room1Center = new Vector2Int(room1.CenterX, room1.CenterY);
    //         var room2Center = new Vector2Int(room2.CenterX, room2.CenterY);
    //
    //         // Get the direction of the corridor
    //         var direction = room2Center - room1Center;
    //
    //         // We can only got up, down, left or right
    //         /// We're going to draw a reasonable jagge path
    //
    //         // Find the edge of the first room
    //         var room1Edge = new Vector2Int();
    //
    //         room1Edge.x = direction.x switch
    //         {
    //             > 0 => room1.Right,
    //             < 0 => room1.Left,
    //             _ => room1Center.x
    //         };
    //
    //         room1Edge.y = direction.y switch
    //         {
    //             > 0 => room1.Bottom,
    //             < 0 => room1.Top,
    //             _ => room1Center.y
    //         };
    //
    //         // Find the edge of the second room
    //         var room2Edge = new Vector2Int();
    //
    //         room2Edge.x = direction.x switch
    //         {
    //             > 0 => room2.Left,
    //             < 0 => room2.Right,
    //             _ => room2Center.x
    //         };
    //
    //         room2Edge.y = direction.y switch
    //         {
    //             > 0 => room2.Top,
    //             < 0 => room2.Bottom,
    //             _ => room2Center.y
    //         };
    //
    //         // Find the middle of the corridor
    //         var corridorMiddle = new Vector2Int();
    //         corridorMiddle.x = (room1Edge.x + room2Edge.x) / 2;
    //
    //         corridorMiddle.y = direction.y switch
    //         {
    //             > 0 => room1Edge.y,
    //             < 0 => room2Edge.y,
    //             _ => room1Edge.y
    //         };
    //
    //         // Find the width and height of the corridor
    //
    //         var corridorWidth = Mathf.Abs(room1Edge.x - room2Edge.x);
    //         var corridorHeight = Mathf.Abs(room1Edge.y - room2Edge.y);
    //
    //         // Draw the corridor
    //         var corridorPoints = new List<Vector2Int>();
    //
    //         if (direction.x != 0)
    //         {
    //             // Draw a horizontal corridor
    //             for (var x = corridorMiddle.x - corridorWidth / 2; x < corridorMiddle.x + corridorWidth / 2; x++)
    //             {
    //                 corridorPoints.Add(new Vector2Int(x, corridorMiddle.y));
    //             }
    //         }
    //         else if (direction.y != 0)
    //         {
    //             // Draw a vertical corridor
    //             for (var y = corridorMiddle.y - corridorHeight / 2; y < corridorMiddle.y + corridorHeight / 2; y++)
    //             {
    //                 corridorPoints.Add(new Vector2Int(corridorMiddle.x, y));
    //             }
    //         }
    //
    //         return corridorPoints;
    //
    //
    //     }
    //
    //     private Room GetClosestRoom(Room room, List<Room> rooms)
    //     {
    //         var closestRoom = rooms[0];
    //         var closestDistance = int.MaxValue;
    //
    //         foreach (var otherRoom in rooms)
    //         {
    //             if (room == otherRoom) continue;
    //             if (room.Overlaps(otherRoom)) continue;
    //             
    //             var distance = room.GetDistance(otherRoom);
    //             if (distance >= closestDistance) continue;
    //             closestDistance = distance;
    //             closestRoom = otherRoom;
    //         }
    //
    //         return closestRoom;
    //     }
    // }
    
    public class RoomGenerator : IGenerator<int[,], RoomGeneratorParameters>
{
    public int[,] Generate()
    {
        throw new NotImplementedException();
    }

    public int[,] Generate(RoomGeneratorParameters parameters)
    {
        throw new NotImplementedException();
        // var map = new int[parameters.Width, parameters.Height];
        // var rooms = new List<Room>();
        // var unconnectedRooms = new List<Room>();
        // var connectedRooms = new List<Room>();
        //
        // // Generate rooms
        // for (var i = 0; i < parameters.RoomCount; i++)
        // {
        //     var room = GenerateRoom(parameters);
        //     rooms.Add(room);
        //     unconnectedRooms.Add(room);
        // }
        //
        // // Connect rooms
        // var room = rooms[0];
        // connectedRooms.Add(room);
        // unconnectedRooms.Remove(room);
        //
        // while (unconnectedRooms.Count > 0)
        // {
        //     var closestRoom = GetClosestRoom(room, unconnectedRooms);
        //     var corridor = GenerateCorridor(room, closestRoom);
        //     if (corridor != null)
        //     {
        //         foreach (var point in corridor)
        //         {
        //             map[point.x, point.y] = 1;
        //         }
        //     }
        //
        //     connectedRooms.Add(closestRoom);
        //     unconnectedRooms.Remove(closestRoom);
        //     room = closestRoom;
        // }
        //
        // return map;
    }
}

    [Serializable]
    public class RoomGeneratorParameters : IGenerationParameters
    {
        [SerializeField]private int minRoomSize;
        [SerializeField]private int maxRoomSize;
        [SerializeField]private int roomCount;
        [SerializeField]private int width;
        [SerializeField]private int height;
        
        public int MinRoomSize
        {
            get => minRoomSize;
            set => minRoomSize = Mathf.Clamp(value, 1, maxRoomSize);
        }
        
        public int MaxRoomSize
        {
            get => maxRoomSize;
            set => maxRoomSize = Mathf.Clamp(value, minRoomSize, int.MaxValue);
        }
        
        public int RoomCount
        {
            get => roomCount;
            set => roomCount = Mathf.Clamp(value, 1, int.MaxValue);
        }
        
        public RoomGeneratorParameters(int minRoomSize, int maxRoomSize, int roomCount, int width, int height)
        {
            MinRoomSize = minRoomSize;
            MaxRoomSize = maxRoomSize;
            RoomCount = roomCount;
            Width = width;
            Height = height;
        }
        
        
        public int Width
        {
            get => width;
            set => width = Mathf.Clamp(value, 1, int.MaxValue);
        }
        
        public int Height
        {
            get => height;
            set => height = Mathf.Clamp(value, 1, int.MaxValue);
        }

    }

    public class CorridorGenerator : IGenerator<int[,], CorridorGeneratorParameters>
    {
        public int[,] Generate()
        {
            throw new NotImplementedException();
        }

        public int[,] Generate(CorridorGeneratorParameters parameters)
        {
            throw new NotImplementedException();
        }
    }

    public class CorridorGeneratorParameters : IGenerationParameters
    {
        
        
    }

    public class RoomAndCorridorGenerationStrategy : IMapGenerationStrategy
    {
        private readonly RoomGenerator _roomGenerator;
        private readonly CorridorGenerator _corridorGenerator;

        public RoomAndCorridorGenerationStrategy(RoomGenerator roomGenerator, CorridorGenerator corridorGenerator)
        {
            this._roomGenerator = roomGenerator;
            this._corridorGenerator = corridorGenerator;
        }

        public int[,] Execute(int[,] initialMap)
        {
            // var mapWithRooms = _roomGenerator.Generate(initialMap);
            // var finalMap = _corridorGenerator.Generate(mapWithRooms);

            // return finalMap;
            throw new NotImplementedException();
        }

        public T[,] Execute<T>(T[,] initialMap)
        {
            throw new NotImplementedException();
        }
    }

    // public class SimpleTileMapFactory : ITileMapFactory
    // {
    //     public ITileMap Create(int[,] mapRepresentation)
    //     {
    //         // Implement the conversion from mapRepresentation to ITileMap here.
    //         // For simplicity, let's assume that we have a SimpleTileMap class that implements ITileMap and 
    //         // takes a 2D array of ints in its constructor.
    //         
    //         return new SimpleTileMap(mapRepresentation);
    //     }
    // }
}