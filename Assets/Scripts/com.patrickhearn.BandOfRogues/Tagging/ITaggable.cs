﻿using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;

namespace com.patrickhearn.BandOfRogues.Tagging
{
    public interface ITaggable
    {
        List<string> Tags { get; set; }

        public List<string> GetTags()
        {
            return Tags;
        }

        void AddTag(string tag)
        {
            if (Tags == null)
            {
                Tags = new List<string>();
            }
            
            if (Tags.Contains(tag))
            {
                return;
            }
            Tags.Add(tag);
        }

        void RemoveTag(string tag)
        {
            if (Tags == null)
            {
                return;
            }
            Tags.Remove(tag);
        }

        bool HasTag(string tag)
        {
            return Tags.Contains(tag);
        }
        
        
    }

}