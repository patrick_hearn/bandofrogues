﻿namespace com.patrickhearn.BandOfRogues.Tagging
{
    public struct Tag
    {
        public string Name;
        
        public Tag(string name)
        {
            Name = name;
        }
        
        public static implicit operator Tag(string name)
        {
            return new Tag(name);
        }
        
        public static implicit operator string(Tag tag)
        {
            return tag.Name;
        }
        
        public override string ToString()
        {
            return Name;
        }
        
        public override bool Equals(object obj)
        {
            if (obj is Tag tag)
            {
                return tag.Name == Name;
            }
            return false;
        }
        
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
        
        public static bool operator ==(Tag a, Tag b)
        {
            return a.Name == b.Name;
        }
        
        public static bool operator !=(Tag a, Tag b)
        {
            return a.Name != b.Name;
        }
        
        public static bool operator ==(Tag a, string b)
        {
            return a.Name == b;
        }
        
        public static bool operator !=(Tag a, string b)
        {
            return a.Name != b;
        }
        
        public static bool operator ==(string a, Tag b)
        {
            return a == b.Name;
        }
        
        public static bool operator !=(string a, Tag b)
        {
            return a != b.Name;
        }
        

    }
}