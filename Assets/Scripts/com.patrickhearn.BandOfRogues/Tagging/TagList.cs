﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Tagging
{

    [CreateAssetMenu(fileName = "TagList", menuName = "Band of Rogues/Tag List", order = 0)]
    public class TagList : ScriptableObject, IEnumerable<string>
    {
        [SerializeField]
        private List<string> tags = new();
        public List<string> GetTags => tags;

        public void AddTag(string text)
        {
            text = text.Trim().ToLower();
            if(text == "")
            {
                return;
            }
            if (tags.Contains(text))
            {
                return;
            }
            tags.Add(text);
        }

        public void RemoveTag(string tag)
        {
            tag = tag.Trim().ToLower();
            tags.Remove(tag);
        }

        public bool Contains(string tag)
        {
            tag = tag.Trim().ToLower();
            return tags.Contains(tag);
        }

        public IEnumerator<string> GetEnumerator()
        {
            return tags.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

}