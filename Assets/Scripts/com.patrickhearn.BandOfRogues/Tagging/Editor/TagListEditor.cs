﻿using UnityEditor;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Tagging.Editor
{
    [CustomEditor(typeof(TagList))]
    public class TagListEditor : UnityEditor.Editor
    {
        private string _newTag;
        public override void OnInspectorGUI()
        {
            var tagList = (TagList) target;
            EditorGUILayout.LabelField("Tags", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            foreach (var tag in tagList.GetTags)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(tag);
                if (GUILayout.Button("Remove Tag"))
                {
                    tagList.RemoveTag(tag);
                }
                EditorGUILayout.EndHorizontal();
            }
            
            EditorGUI.indentLevel--;
            
            EditorGUILayout.LabelField("Add Tag", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;

            EditorGUILayout.BeginHorizontal();
            _newTag = _newTag ?? "";
            GUI.SetNextControlName("AddTagField");
            _newTag = EditorGUILayout.TextField("Tag", _newTag);

            // Check if the Enter key was pressed while the text field was active
            if ((Event.current.type == EventType.KeyUp && Event.current.keyCode == KeyCode.Return) || GUILayout.Button("Add Tag"))
            {
                tagList.AddTag(_newTag);
                _newTag = "";
                GUI.FocusControl("AddTagField"); // Set the focus back to the text field
            }
            EditorGUILayout.EndHorizontal();
        } 
    }
}