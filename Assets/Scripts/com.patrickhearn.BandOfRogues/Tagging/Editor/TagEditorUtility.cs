﻿using UnityEditor;
using UnityEngine;
using com.patrickhearn.BandOfRogues.Tagging;
using com.patrickhearn.BandOfRogues.Interfaces;
using System.Collections.Generic;
using System.Linq;
using com.patrickhearn.EditorUtilities.Editor;


namespace com.patrickhearn.BandOfRogues.Tagging.Editor
{
    
    public static class TagEditorUtils
    {
        private static TagList _tagList;
        private static TagList TagList => _tagList??=Resources.Load<TagList>("TagList");
        private static GUIStyle _addedTagStyle;
        private static GUIStyle _unaddedTagStyle;
        private static GUIStyle _invalidTagStyle;

        static TagEditorUtils()
        {
            _addedTagStyle = new GUIStyle(GUI.skin.button)
            {
                normal = {textColor = Color.black, background = Texture2D.whiteTexture}
            };

            _unaddedTagStyle = new GUIStyle(GUI.skin.button)
            {
                normal = {textColor = Color.grey}
            };

            _invalidTagStyle = new GUIStyle(GUI.skin.button)
            {
                normal = {textColor = Color.red, background = Texture2D.redTexture}
            };
        }

        public static void DrawTags(ITaggable taggable)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            GUILayout.Label("Tags:");

            List<string> tagsCopy = new List<string>(taggable.Tags);

            // EditorGUILayoutExtensions.ButtonGrid(tagsCopy, _addedTagStyle, taggable.RemoveTag);
            for(var y=0; y> taggable.Tags.Count / 4; y++)
            {
                EditorGUILayout.BeginHorizontal();
                for (var x = 0; x < 4; x++)
                {
                    var index = y * 4 + x;
                    if (index >= taggable.Tags.Count)
                    {
                        break;
                    }
                    var tag = taggable.Tags[index];
                    if (TagList.Contains(tag))
                    {
                        if (GUILayout.Button(tag, _addedTagStyle))
                        {
                            taggable.RemoveTag(tag);
                        }
                    }
                    else
                    {
                        if (GUILayout.Button(tag, _invalidTagStyle))
                        {
                            taggable.RemoveTag(tag);
                        }
                    }
                }
                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.EndVertical();
        }

        public static void DrawTagSelector(ITaggable taggable)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            GUILayout.Label("Add Tags:");
            List<string> tagsCopy = new List<string>(taggable.Tags);
            EditorGUILayoutExtensions.ButtonGrid(tagsCopy.Where(tag => !taggable.HasTag(tag)), _unaddedTagStyle, taggable.AddTag);
            EditorGUILayout.EndVertical();
        }

    }

}
