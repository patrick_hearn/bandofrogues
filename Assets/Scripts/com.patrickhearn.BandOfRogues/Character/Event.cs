﻿namespace com.patrickhearn.BandOfRogues.Character
{
    public class EventOutcome
    {
        public float[] OutcomeVector { get; set; }
        public string OutcomeDescription { get; set; }
    }
    
    public class EventPersonalityArchetype
    {
        public PersonalityArchetype PersonalityArchetype { get; set; }
        public float Weight { get; set; }
    }
    
    public class Event
    {
        // Events are defined by a number of Personality Archetypes and their weights on different outcomes
        // Event particpants have their personality vectors trnasformed, compared, etc to determine the outcome of the event
        // The outcome of the event is an nd vector of floats, where n is the number of outcomes
        // The outcome of the event is determined by the personality vectors of the participants
        
        public EventPersonalityArchetype[] PersonalityArchetypes { get; set; }
        public EventOutcome[] Outcomes { get; set; }
        
        public Event(EventPersonalityArchetype[] personalityArchetypes, EventOutcome[] outcomes)
        {
            PersonalityArchetypes = personalityArchetypes;
            Outcomes = outcomes;
        }
        
        public EventOutcome GetOutcome(PersonalityArchetype[] personalityArchetypes)
        {
            // Use the synergy matrix to determine the synergy between two personalities
            var outcomeVector = new float[Outcomes.Length];
            for (var i = 0; i < Outcomes.Length; i++)
            {
                for (var j = 0; j < PersonalityArchetypes.Length; j++)
                {
                    outcomeVector[i] += PersonalityArchetypes[j].Weight * PersonalityArchetypes[j].PersonalityArchetype.GetSynergy(personalityArchetypes[j]);
                }
            }
            
            // Find the outcome with the highest value
            var max = 0f;
            var maxIndex = 0;
            for (var i = 0; i < outcomeVector.Length; i++)
            {
                if (outcomeVector[i] > max)
                {
                    max = outcomeVector[i];
                    maxIndex = i;
                }
            }
            
            return Outcomes[maxIndex];
        }
        
        public EventOutcome GetOutcome(PersonalityArchetype personalityArchetype)
        {
            return GetOutcome(new[] { personalityArchetype });
        }
        
        public EventOutcome GetOutcome(PersonalityArchetype personalityArchetypeA, PersonalityArchetype personalityArchetypeB)
        {
            return GetOutcome(new[] { personalityArchetypeA, personalityArchetypeB });
        }
        
        
    }
}