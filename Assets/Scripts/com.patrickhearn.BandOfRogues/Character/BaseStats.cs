﻿using System;

namespace com.patrickhearn.BandOfRogues.Character
{
    [Serializable]
    public struct BaseStats
    {
        public int strength;
        public int dexterity;
        public int constitution;
        public int intelligence;
        public int wisdom;
        public int charisma;
        public int maxHealth;
        public int maxMana;
    }
}