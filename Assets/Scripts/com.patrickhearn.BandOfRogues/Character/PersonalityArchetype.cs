using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Character
{
    [Serializable]
    public class PersonalityArchetype 
    {
        [FormerlySerializedAs("Name")] public string name;
        public float Agreeableness { get; set; }
        public float Conscientiousness { get; set; }
        public float Extraversion { get; set; }
        public float Neuroticism { get; set; }
        public float Openness { get; set; }
        public float[] PersonalityVector => new[] { Agreeableness, Conscientiousness, Extraversion, Neuroticism, Openness };
     
        public float this[int i]
        {
            get => PersonalityVector[i];
            set => PersonalityVector[i] = value;
        }

        public float this[PersonalityAspect personalityAspect]
        {
            get => PersonalityVector[(int)personalityAspect];
            set => PersonalityVector[(int)personalityAspect] = value;
        }
        
        public PersonalityArchetype(float agreeableness, float conscientiousness, float extraversion, float neuroticism, float openness)
        {
            Agreeableness = agreeableness;
            Conscientiousness = conscientiousness;
            Extraversion = extraversion;
            Neuroticism = neuroticism;
            Openness = openness;
        }


        public float GetSynergy(PersonalityArchetype personalityArchetype)
        {
            var synergy = 0f;
            synergy += Agreeableness * personalityArchetype.Agreeableness;
            synergy += Conscientiousness * personalityArchetype.Conscientiousness;
            synergy += Extraversion * personalityArchetype.Extraversion;
            synergy += Neuroticism * personalityArchetype.Neuroticism;
            synergy += Openness * personalityArchetype.Openness;
            return synergy;
        }
    }
    
    #if UNITY_EDITOR
    [UnityEditor.CustomPropertyDrawer(typeof(PersonalityArchetype))]
    public class PersonalityArchetypePropertyDrawer : UnityEditor.PropertyDrawer
    {
        public override void OnGUI(Rect position, UnityEditor.SerializedProperty property, GUIContent label)
        {
            var agreeableness = property.FindPropertyRelative("Agreeableness");
            var conscientiousness = property.FindPropertyRelative("Conscientiousness");
            var extraversion = property.FindPropertyRelative("Extraversion");
            var neuroticism = property.FindPropertyRelative("Neuroticism");
            var openness = property.FindPropertyRelative("Openness");
            var name = property.FindPropertyRelative("Name");
            UnityEditor.EditorGUI.BeginProperty(position, label, property);
            UnityEditor.EditorGUI.PropertyField(position, name);
            // The synergy matrix is a 5x5 matrix of floats, we want to display it as a 5x5 grid of floats
            var rect = new Rect(position.x, position.y + UnityEditor.EditorGUIUtility.singleLineHeight, position.width, UnityEditor.EditorGUIUtility.singleLineHeight);
            UnityEditor.EditorGUI.PropertyField(rect, agreeableness);
            rect.y += UnityEditor.EditorGUIUtility.singleLineHeight;
            UnityEditor.EditorGUI.PropertyField(rect, conscientiousness);
            rect.y += UnityEditor.EditorGUIUtility.singleLineHeight;
            UnityEditor.EditorGUI.PropertyField(rect, extraversion);
            rect.y += UnityEditor.EditorGUIUtility.singleLineHeight;
            UnityEditor.EditorGUI.PropertyField(rect, neuroticism);
            rect.y += UnityEditor.EditorGUIUtility.singleLineHeight;
            UnityEditor.EditorGUI.PropertyField(rect, openness);
            UnityEditor.EditorGUI.EndProperty();
        }
    }
    #endif
}