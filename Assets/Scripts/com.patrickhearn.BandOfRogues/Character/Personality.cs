﻿using System;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Character
{
    [Serializable]
    public class Personality
    {
        #region PersonalityAttributes
        
        [Range(0, 10)]
        [SerializeField]
        private int agreeableness;
        public int Agreeableness { get => agreeableness; set => agreeableness = value; }

        [Range(0, 10)]
        [SerializeField]
        private int conscientiousness;
        public int Conscientiousness { get => conscientiousness; set => conscientiousness = value; }
        
        [Range(0, 10)]
        [SerializeField]
        private int extraversion;
        public int Extraversion { get => extraversion; set => extraversion = value; }
        
        [Range(0, 10)]
        [SerializeField]
        private int neuroticism;
        public int Neuroticism { get => neuroticism; set => neuroticism = value; }
        
        [Range(0, 10)]
        [SerializeField]
        private int openness;
        public int Openness { get => openness; set => openness = value; }

        #endregion

        private float[] PersonalityMatrix => new float[]
        {
            (Agreeableness - 5) / 5f,
            (Conscientiousness - 5) / 5f,
            (Extraversion - 5) / 5f,
            (Neuroticism - 5) / 5f,
            (Openness - 5) / 5f
        };
        
        public float[] GetPersonalityMatrix()
        {
            return PersonalityMatrix;
        }
        
        public Personality()
        {
            Agreeableness = 5;
            Conscientiousness = 5;
            Extraversion = 5;
            Neuroticism = 5;
            Openness = 5;
        }
        
        public Personality(int agreeableness, int conscientiousness, int extraversion, int neuroticism, int openness)
        {
            Agreeableness = agreeableness;
            Conscientiousness = conscientiousness;
            Extraversion = extraversion;
            Neuroticism = neuroticism;
            Openness = openness;
        }
        
        public Personality(float[] personalityMatrix)
        {
            Agreeableness = (int) (personalityMatrix[0] * 5 + 5);
            Conscientiousness = (int) (personalityMatrix[1] * 5 + 5);
            Extraversion = (int) (personalityMatrix[2] * 5 + 5);
            Neuroticism = (int) (personalityMatrix[3] * 5 + 5);
            Openness = (int) (personalityMatrix[4] * 5 + 5);
        }
        
    }
}