﻿namespace com.patrickhearn.BandOfRogues.Character
{
    public class Character
    {
        public string Name;
        public string NickName;
        public int Level;
        public int Experience;
        public int Mood;
        public BaseStats BaseStats;
        public Personality Personality { get; set; }
        
        
    }
}