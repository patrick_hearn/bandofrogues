using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Character
{
    public class PartnershipArchetypes : ScriptableObject
    {
        public static readonly float[,] Default = new float[,]
        {
            { 0.5f, 0.5f, 0.5f, 0.5f, 0.5f },
            { 0.5f, 0.5f, 0.5f, 0.5f, 0.5f },
            { 0.5f, 0.5f, 0.5f, 0.5f, 0.5f },
            { 0.5f, 0.5f, 0.5f, 0.5f, 0.5f },
            { 0.5f, 0.5f, 0.5f, 0.5f, 0.5f }
        };
        
        

        public static float[] GetSynergy(float[] personalityA, float[] personalityB, float[,] synergyMatrix)
        {
            // Use the synergy matrix to determine the synergy between two personalities
            var synergy = new float[5];
            for (var i = 0; i < 5; i++)
            {
                for (var j = 0; j < 5; j++)
                {
                    synergy[i] += personalityA[j] * personalityB[i] * synergyMatrix[i, j];
                }
            }
            return synergy;
        }
        
        [UnityEditor.MenuItem("Band of Rogues/Character/Create Partnership Archetypes")]
        public static void CreatePartnershipArchetype()
        {
            var asset = CreateInstance<PartnershipArchetypes>();
            UnityEditor.AssetDatabase.CreateAsset(asset, "Assets/PartnershipArchetypes.asset");
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.EditorUtility.FocusProjectWindow();
            UnityEditor.Selection.activeObject = asset;
        }
        
    }
}