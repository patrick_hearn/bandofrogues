﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Character
{
    public  class PersonalityArchtypes : ScriptableObject
    {
        // Agreeableness, Conscientiousness, Extraversion, Neuroticism, Openness 
        // -1 = negative, 0 = neutral, 1 = positive
        //  1 = Agreeable, Conscientious, Extraverted, Neurotic, Open
        //  0 = Moderate
        // -1 = Disagreeable, Impulsive, Introverted, Stable, Closed 

        [FormerlySerializedAs("Archetypes")] public List<PersonalityArchetype> archetypes = new List<PersonalityArchetype>();

        [UnityEditor.MenuItem("Band of Rogues/Character/Create Personality Archetypes")]
        public static void CreatePersonalityArchetype()
        {
            var asset = CreateInstance<PersonalityArchtypes>();
            UnityEditor.AssetDatabase.CreateAsset(asset, "Assets/PersonalityArchetypes.asset");
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.EditorUtility.FocusProjectWindow();
            UnityEditor.Selection.activeObject = asset;
        }

    }
}