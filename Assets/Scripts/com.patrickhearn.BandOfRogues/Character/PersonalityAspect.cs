namespace com.patrickhearn.BandOfRogues.Character
{
    public enum PersonalityAspect
    {
        Agreeableness=0,
        Conscientiousness=1,
        Extraversion=2,
        Neuroticism=3,
        Openness=4
    }
}