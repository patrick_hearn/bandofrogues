using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Character
{
    [Serializable]
    public class PartnershipArchetype
    {
        [FormerlySerializedAs("Name")] public string name;
        public float[,] SynergyMatrix { get; set; }
        
        public PartnershipArchetype(float[,] synergyMatrix)
        {
            SynergyMatrix = synergyMatrix;
        }
        
        public float[] GetSynergy(float[] personalityA, float[] personalityB)
        {
            // Use the synergy matrix to determine the synergy between two personalities
            var synergy = new float[5];
            for (var i = 0; i < 5; i++)
            {
                for (var j = 0; j < 5; j++)
                {
                    synergy[i] += SynergyMatrix[i, j] * personalityB[j];
                }
            }
            return synergy;
        }
        
        public float[] GetSynergy(PersonalityArchetype personalityA, PersonalityArchetype personalityB)
        {
            return GetSynergy(personalityA.PersonalityVector, personalityB.PersonalityVector);
        }
        
        
    }
    
    #if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(PartnershipArchetype))]
    public class PartnershipArchetypePropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var synergyMatrix = property.FindPropertyRelative("SynergyMatrix");
            var name = property.FindPropertyRelative("Name");
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.PropertyField(position, name);
            // The synergy matrix is a 5x5 matrix of floats, we want to display it as a 5x5 grid of floats
            var rect = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight, position.width, EditorGUIUtility.singleLineHeight);
            for (var i = 0; i < 5; i++)
            {
                var row = synergyMatrix.GetArrayElementAtIndex(i);
                for (var j = 0; j < 5; j++)
                {
                    var element = row.GetArrayElementAtIndex(j);
                    EditorGUI.PropertyField(rect, element, GUIContent.none);
                    rect.x += rect.width;
                }
                rect.x = position.x;
                rect.y += rect.height;
            }
            EditorGUI.EndProperty();
            
            
        }
        
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight * 6;
        }
        
    }
    #endif
    
}