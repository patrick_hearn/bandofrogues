﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace com.patrickhearn
{
    public class ReflectionHelper
    {
        public static List<Type> GetDerivedTypes(Type baseType)
        {
            return (
                from assembly in AppDomain.CurrentDomain.GetAssemblies() 
                from type in assembly.GetTypes() 
                where type.IsSubclassOf(baseType) 
                select type
                ).ToList();
        }
        
        
    }
}