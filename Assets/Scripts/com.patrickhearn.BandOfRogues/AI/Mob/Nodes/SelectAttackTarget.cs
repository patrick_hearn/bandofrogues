﻿using System.ComponentModel;
using System.Linq;
using com.patrickhearn.BandOfRogues.Actors;
using com.patrickhearn.BandOfRogues.AI.AI.Mob.Nodes.Action;
using com.patrickhearn.BandOfRogues.AI.BehaviourTree;
using com.patrickhearn.BandOfRogues.Game;
using com.patrickhearn.BandOfRogues.Map;

namespace com.patrickhearn.BandOfRogues.AI.AI.Mob.Nodes
{
    public class SelectAttackTarget : MobActionNode
    {
        [DisplayName]public static string DisplayName => "Select Attack Target";

        public override string Name => "Select Attack Target";
        protected override NodeState EvaluateInternal()
        {
            // var potentialTargets = Mob.VisibleEnemyMobList(
            //         ActorManager.Instance.Actors.Cast<global::com.patrickhearn.BandOfRogues.Actors.Mob>().ToList()).ToList();
            var potentialTargets = ActorManager.Mobs.Where(m => m != Mob).ToList();
            // Select the closest target
            var closestTarget = potentialTargets
                .OrderBy(t => TileMap.Distance(Mob.Position, t.Position))
                .FirstOrDefault();

            if (closestTarget == null)
            {
                // Debug.Log("No targets");
                return NodeState.Failure;
            }
            Mob.ActionTarget = closestTarget;
            // Debug.Log($"Selected {closestTarget.name} as attack target for {Mob.name}");
            return NodeState.Success;
        }

        public SelectAttackTarget(global::com.patrickhearn.BandOfRogues.Actors.Mob mob) : base(mob)
        {
        }
    }
}