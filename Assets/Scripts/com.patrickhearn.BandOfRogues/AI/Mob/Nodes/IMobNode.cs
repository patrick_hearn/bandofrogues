﻿using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.AI.AI.Mob.Nodes
{
    // public interface IMobNode
    // {
    //     public Actors.Mob Mob { get; }
    //
    //     public void SetMob(Actors.Mob mob);
    // }
    public interface IActionNode<T> where T : IActionable
    {
        public ICommandContext<T> CommandContext { get; }
    }
    
    // public interface IMobActionNode : IMobNode, IActionNode<Actors.Mob>
    // {
    // }
}