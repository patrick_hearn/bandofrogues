﻿using System.ComponentModel;
using System.Linq;
using com.patrickhearn.BandOfRogues.Actors;
using com.patrickhearn.BandOfRogues.AI.AI.Mob.Nodes.Action;
using com.patrickhearn.BandOfRogues.AI.BehaviourTree;
using com.patrickhearn.BandOfRogues.Game;
using com.patrickhearn.BandOfRogues.Map;

namespace com.patrickhearn.BandOfRogues.AI.AI.Mob.Nodes
{
    public class SelectMoveTarget : MobActionNode
    {
        [DisplayName]public static string DisplayName => "Select Move Target";

        public override string Name => "Select Move Target";
        protected override NodeState EvaluateInternal()
        {
            if (Mob.ActionTarget == null)
            {
                // Debug.Log($"No action target for {Mob.name}"); 
                return NodeState.Failure;
            }

            // If already in range, return failure
            if (TileMap.Distance(Mob.Position, Mob.ActionTarget.Position) <= Mob.AttackRange)
            {
                // Debug.Log($"{Mob.name} is already in range of {Mob.ActionTarget.name}");
                return NodeState.Failure;
            }
            
            var path = ActorManager.FindPath(Mob.Position, Mob.ActionTarget.Position);
            
            // If no valid path, return failure
            if (path == null)
            {
                // Debug.Log($"No valid path for {Mob.name} to {Mob.ActionTarget.name}");
                return NodeState.Failure;
            }
            
            var moveTarget = path.FirstOrDefault();

            // If not in range, find a valid move target within attack range of enemy
            //var moveTarget = MoveManager.GetValidMoveTarget(Mob.Position, Mob.ActionTarget.Position, 1, Mob.AttackRange);
            

            // Set the Mob's MoveTarget to the valid move target
            Mob.MoveTarget = moveTarget;

            // Debug.Log($"Selected {moveTarget} as move target for {Mob.name}");
            return NodeState.Success;
        }


        public SelectMoveTarget(global::com.patrickhearn.BandOfRogues.Actors.Mob mob) : base(mob)
        {
        }
    }
}