using System;
using com.patrickhearn.BandOfRogues.AI.BehaviourTree;
using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.AI.AI.Mob.Nodes.Condition
{
    public abstract class MobConditionNode : ConditionNode, IActionable
    {
        public new ActionableBlackboard Blackboard { get; set; }
    }

    public class IsLowHealth : MobConditionNode
    {
        public override string Name => "Is Low Health?";

        protected override bool CheckCondition()
        {
            throw new NotImplementedException();
        }
        
    }

    public class IsEnemyNearby : MobConditionNode
    {
        public override string Name => "Is Enemy Nearby?";
        protected override bool CheckCondition()
        {
            throw new NotImplementedException();
        }

    }
    
    public class IsEnemyInAttackRange : MobConditionNode
    {
        public override  string Name => "Is Enemy In Attack Range?";
        protected override bool CheckCondition()
        {
            throw new NotImplementedException();
        }
    }
    
    public class IsItemNearby : MobConditionNode
    {
        public override string Name => "Is Item Nearby?";
        protected override bool CheckCondition()
        {
            throw new NotImplementedException();
        }
    }
    
    public class IsItemUseful : MobConditionNode
    {
        public override string Name => "Is Item Useful?";
        protected override bool CheckCondition()
        {
            throw new NotImplementedException();
        }
    }
    
    public class IsInventoryFull : MobConditionNode
    {
        public override string Name => "Is Inventory Full?";
        protected override bool CheckCondition()
        {
            throw new NotImplementedException();
        }
    }
    
    public class IsTrapNearby : MobConditionNode
    {
        public override string Name => "Is Trap Nearby?";
        protected override bool CheckCondition()
        {
            throw new NotImplementedException();
        }
    }
    
    public class IsSafeToRest : MobConditionNode
    {
        public override string Name => "Is Safe To Rest?";
        protected override bool CheckCondition()
        {
            throw new NotImplementedException();
        }
        
    }

}