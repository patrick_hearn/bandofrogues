﻿using System.Collections.Generic;
using System.ComponentModel;
using com.patrickhearn.BandOfRogues.Actors;
using com.patrickhearn.BandOfRogues.AI.BehaviourTree;
using com.patrickhearn.BandOfRogues.Game;
using com.patrickhearn.BandOfRogues.Map;

namespace com.patrickhearn.BandOfRogues.AI.AI.Mob.Nodes.Action
{
    public class MoveTo : MobActionNode
    {
        [DisplayName]public static string DisplayName => "Move To";

        public override string Name => "Move To";
        protected override NodeState EvaluateInternal()
        {
            // Check if our ActionTarget is within range
            if (TileMap.Distance(Mob.Position, Mob.ActionTarget.Position) <= Mob.AttackRange)
            {
                // Debug.Log($"{Mob.name} is already in range of {Mob.ActionTarget.name}");    
                return NodeState.Failure; // already in range
            }

            // var path = TileMapManager.TileMap.Pathfinding.GetPathMasked(
            //     Mob.Position, Mob.MoveTarget, GameManager.Instance.GetActorOccupiedTiles(new List<Actor> {Mob.ActionTarget}));
            var path = ActorManager.FindPath(Mob.Position, Mob.MoveTarget);
            if (path == null)
            {
                // Debug.Log("No valid path");
                return NodeState.Failure; // no valid path to target
            }
            // Debug.Log("Moving");
            Mob.StartMoving(Mob.MoveTarget);
            return NodeState.Running; // started moving towards target
        }


        public MoveTo(global::com.patrickhearn.BandOfRogues.Actors.Mob mob) : base(mob)
        {
        }
    }
}