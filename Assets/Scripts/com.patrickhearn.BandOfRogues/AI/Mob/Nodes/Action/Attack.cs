﻿using System.ComponentModel;
using com.patrickhearn.BandOfRogues.AI.BehaviourTree;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Map;

namespace com.patrickhearn.BandOfRogues.AI.AI.Mob.Nodes.Action
{
    public class Attack : MobActionNode
    {

        public Attack() : base() {
        }

        public Attack (global::com.patrickhearn.BandOfRogues.Actors.Mob mob) : base(mob)
        {
            
        }
        
        [DisplayName]public static string DisplayName => "Attack"; 
        public override string Name => "Attack";
        
        protected override NodeState EvaluateInternal()
        {
            // Check if target derives from Mob
            if (Mob?.ActionTarget is not Actors.Mob)
            {
                // Debug.Log("Action target is not a mob");
                return NodeState.Failure;
            }

            if (TileMap.Distance(Mob.Position, Mob.ActionTarget.Position) > Mob.AttackRange)
            {
                // Debug.Log($"{Mob.name} Action target is out of range: {Mob.Position} to {Mob.ActionTarget.Position} is {TileMap.Distance(Mob.Position, Mob.ActionTarget.Position)} which is greater than {Mob.AttackRange}");
                return NodeState.Failure;
            }

            // if (!TileMapManager.TileMap.IsVisibleFrom(Mob.Position, Mob.ActionTarget.Position))
            // {
            //     // Debug.Log($"Action target is not visible to {Mob.name}");
            //     return NodeState.Failure;
            // }

            // Debug.Log($"{Mob.name} is attacking {Mob.ActionTarget.name}");
            Mob.Attack(Mob.ActionTarget as global::com.patrickhearn.BandOfRogues.Actors.Mob);           
            return NodeState.Success;
        }
    }
}