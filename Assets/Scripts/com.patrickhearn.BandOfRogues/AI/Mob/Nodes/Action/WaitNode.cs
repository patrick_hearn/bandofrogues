﻿using System.ComponentModel;
using com.patrickhearn.BandOfRogues.AI.BehaviourTree;

namespace com.patrickhearn.BandOfRogues.AI.AI.Mob.Nodes.Action
{
    public class WaitNode : MobActionNode
    {
        [DisplayName]public static string DisplayName => "Wait";

        public override string Name => "Wait";
        protected override NodeState EvaluateInternal()
        {
            return NodeState.Success;
        }

        public WaitNode(global::com.patrickhearn.BandOfRogues.Actors.Mob mob) : base(mob)
        {
        }
    }
}