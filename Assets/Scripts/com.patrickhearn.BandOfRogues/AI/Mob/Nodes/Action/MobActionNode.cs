﻿using System;
using System.ComponentModel;
using com.patrickhearn.BandOfRogues.Actors;
using com.patrickhearn.BandOfRogues.AI.BehaviourTree;
using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.AI.AI.Mob.Nodes.Action
{
    [Serializable]
    public abstract class MobActionNode : ActionNode, IActionableNode
    {
        protected ActorManager ActorManager;
        public Actors.Mob Mob { get; private set; }
        public void SetMob(Actors.Mob mob)
        {
            Mob = mob;
        }

        [DisplayName]public static string DisplayName => "Mob Leaf Node";
        public override string Name => "Mob Leaf Node";
        public MobActionNode() : base() {}
        public MobActionNode(ActorManager actorManager) : base()
        {
            ActorManager = actorManager;
        }
        
        public MobActionNode(Actors.Mob mob) : base()
        {
            Mob = mob;
        }
        
        public MobActionNode(ActorManager actorManager, Actors.Mob mob) : base()
        {
            ActorManager = actorManager;
            Mob = mob;
        }
        
        protected abstract override NodeState EvaluateInternal();
        
        protected ICommandContext<Actors.Mob> CommandContextInternal { get; set; }
        public ICommandContext<Actors.Mob> CommandContext => CommandContextInternal;

        public ActionableBlackboard Blackboard { get; set; }
    }
}