﻿using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.AI.BehaviourTree;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.AI.AI.Mob
{
    public abstract class BehaviourTreeObject : ScriptableObject, IBehaviourTreeNode
    {
        
        public BehaviourTree BehaviourTree;

        public NodeState NodeState { get; }

        public NodeState Evaluate()
        {
            return BehaviourTree.Evaluate();
        }

        public IEnumerable<IBehaviourTreeNode> GetDescendants()
        {
            if (BehaviourTree == null)
            {
                return new List<IBehaviourTreeNode>();
            }
            return BehaviourTree.GetDescendants();
        }

        public IEnumerable<IBehaviourTreeNode> GetChildren()
        {
            if (BehaviourTree == null)
            {
                return new List<IBehaviourTreeNode>();
            }
            return BehaviourTree.GetChildren();
        }

        public IBlackboard Blackboard { get; set; }

        public string Name => BehaviourTree?.Name;
    }
}