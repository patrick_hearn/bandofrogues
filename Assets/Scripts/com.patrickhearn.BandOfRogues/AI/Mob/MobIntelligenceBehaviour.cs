﻿using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.AI.AI.Mob
{
    [RequireComponent(typeof(global::com.patrickhearn.BandOfRogues.Actors.Mob))]
    public class MobIntelligenceBehaviour : MonoBehaviour
    {
        [FormerlySerializedAs("BehaviourTreeTemplate")] public MobBehaviourTreeObject behaviourTreeTemplate;

        [FormerlySerializedAs("BehaviourTree")] public MobBehaviourTreeObject behaviourTree;
        private global::com.patrickhearn.BandOfRogues.Actors.Mob _mob;
        
        private void Awake()
        {
            behaviourTree = Instantiate(behaviourTreeTemplate);
            _mob = GetComponent<Actors.Mob>();
            behaviourTree.MobBehaviourTree.SetMob(_mob);
            // _mob.TurnStarted += EvaluateBehaviour;   
        }
    }
}