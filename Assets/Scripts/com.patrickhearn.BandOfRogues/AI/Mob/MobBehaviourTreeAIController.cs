﻿using com.patrickhearn.BandOfRogues.Actors;
using com.patrickhearn.BandOfRogues.AI.AI.Mob;
using com.patrickhearn.BandOfRogues.AI.BehaviourTree;
using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.AI
{
    public class MobBehaviourTreeAIController : AIController
    {
        private readonly MobBehaviourTreeObject mobBehaviourTree;

        public MobBehaviourTreeAIController(MobBehaviourTreeObject behaviourTree)
        {
            mobBehaviourTree = behaviourTree;
        }

        public override ICommand<IActor> GetCommand(IActor actor)
        {
            if (actor is not Mob mob) return null;

            mobBehaviourTree.SetMob(mob);
    
            // Execute the behaviour tree and decide the command based on its result
            var nodeState = mobBehaviourTree.Evaluate();
            return nodeState == NodeState.Success ? (mobBehaviourTree.BehaviourTree.Blackboard as ICommandBlackboard<IActor>)?.CommandContext.GetCommand() : null; // TODO: return the command from the context
        }
    }
}