﻿#if UNITY_EDITOR
using com.patrickhearn.BandOfRogues.AI.AI.Mob;
using com.patrickhearn.BandOfRogues.AI.BehaviourTree;
using UnityEditor;

namespace com.patrickhearn.BandOfRogues.AI.Mob.AI.Mob.Editor
{
    [CustomEditor(typeof(BehaviourTreeObject))]
    public class BehaviourTreeObjectEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var behaviourTree = (BehaviourTreeObject)target;
        
            DrawNode(behaviourTree.BehaviourTree.RootNode, 0);
        
            // Draw the rest of the default inspector
            DrawDefaultInspector();
        }
    
        private void DrawNode(IBehaviourTreeNode behaviourTreeNode, int indentLevel)
        {
            // Indent for readability
            EditorGUI.indentLevel = indentLevel;
        
            // Display the type of the node
            EditorGUILayout.LabelField(behaviourTreeNode.GetType().Name);
        
            // If it's a CompositeNode, draw all children
            if(behaviourTreeNode is CompositeNode compositeNode)
            {
                foreach(var child in compositeNode.Children)
                {
                    DrawNode(child, indentLevel + 1);
                }
            }
        }
    }
}
#endif