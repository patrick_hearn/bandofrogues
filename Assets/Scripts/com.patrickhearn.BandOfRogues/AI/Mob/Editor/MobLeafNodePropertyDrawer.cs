﻿#if UNITY_EDITOR
using com.patrickhearn.BandOfRogues.Actors;
using com.patrickhearn.BandOfRogues.AI.AI.Mob.Nodes.Action;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.AI.Mob.AI.Mob.Editor
{
    [UnityEditor.CustomPropertyDrawer(typeof(MobActionNode), true)]
    public class MobLeafNodePropertyDrawer : UnityEditor.PropertyDrawer
    {
        private MobActionNode _mobActionNode;
        
        public override void OnGUI(Rect position, UnityEditor.SerializedProperty property, GUIContent label)
        {
            _mobActionNode ??= (MobActionNode)fieldInfo.GetValue(property.serializedObject.targetObject);
            
            var mob = _mobActionNode.Mob;
            var mobColor = mob switch
            {
                null => Color.white,
                _ => mob.Team switch
                {
                    Team.Ally => Color.blue,
                    Team.Enemy => Color.red,
                    _ => Color.white
                }
            };
            UnityEditor.EditorGUI.DrawRect(position, mobColor);
            UnityEditor.EditorGUI.PropertyField(position, property, label, true);
        }
    }
}
#endif