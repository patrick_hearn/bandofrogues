﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using com.patrickhearn.BandOfRogues.AI.AI.Mob;
using com.patrickhearn.BandOfRogues.AI.AI.Mob.Nodes;
using com.patrickhearn.BandOfRogues.AI.BehaviourTree;
using UnityEditor;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.AI.Mob.AI.Mob.Editor
{
    [CustomEditor(typeof(MobBehaviourTreeObject))]
    public class MobBehaviourTreeObjectEditor : UnityEditor.Editor
    {
        private MobBehaviourTreeObject _mobBehaviourTree;
        private MobBehaviourTreeObject MobBehaviourTree => _mobBehaviourTree ??= (MobBehaviourTreeObject) target;

        private List<string> _nodeTypeNames;
        private List<Type> _nodeTypes;
        private static GUIStyle _upDownButtonStyle;
        
        private static Dictionary<string, Texture2D> _nodeBackgrounds;
        
        private List<string> GetNodeDisplayNames()
        {
             var nodeDisplayNames = new List<string>();
            foreach (var nodeType in _nodeTypes)
            {
                if (nodeType.GetCustomAttributes(typeof(DisplayNameAttribute), false).FirstOrDefault() is
                    DisplayNameAttribute attribute)
                {
                    nodeDisplayNames.Add(attribute.DisplayName);
                }
                else
                {
                    nodeDisplayNames.Add(nodeType.Name);
                }
            }
            
            return nodeDisplayNames;
        }
        
        private List<Type> GetNodeTypes()
        {
            return (from assembly in AppDomain.CurrentDomain.GetAssemblies() from type in assembly.GetTypes() where !type.IsAbstract && typeof(IBehaviourTreeNode).IsAssignableFrom(type) select type).ToList();
        }

        public void OnEnable()
        {
            _nodeTypes = GetNodeTypes();
            _nodeTypeNames = GetNodeDisplayNames();
        }

        public override void OnInspectorGUI()
        {
            // Draw Default
            DrawDefaultInspector();
            
            // Ensure the MobBehaviourTree has a root node
            MobBehaviourTree.BehaviourTree.RootNode ??= new RootNode();
            
            // Draw the Node tree
            DrawNode(MobBehaviourTree.BehaviourTree.RootNode, 0);
            
            
        }

        private void DrawNode(IBehaviourTreeNode behaviourTreeNode, int indent)
        {
            if (behaviourTreeNode == null)
            {
                return;
            }
            EditorGUILayout.BeginVertical();  // MAIN BOX
            
            if (behaviourTreeNode is IMultipleChildrenNode parentNode)
            {
                EditorGUILayout.BeginHorizontal();  // NODE BOX
                EditorGUI.indentLevel = indent;
                DrawAddNodeDropdown(parentNode);
                EditorGUILayout.LabelField(behaviourTreeNode.Name);

                // Spacer to fill the rest of the row
                GUILayout.FlexibleSpace();
                EditorGUILayout.EndHorizontal();
                DrawChildren(parentNode, indent);
            }
            else
            {
                EditorGUI.indentLevel = indent;
                EditorGUILayout.LabelField(behaviourTreeNode.Name);
            }
            
            EditorGUILayout.EndVertical();  // MAIN BOX
        }

        private void DrawChildren(IMultipleChildrenNode multipleChildrenNode, int indent)
        {
            var previousIndent = EditorGUI.indentLevel;
             EditorGUILayout.BeginVertical();  // CHILDREN BOX
            
            
            // foreach (var child in parentNode.GetChildren())
            for(var i=0; i<multipleChildrenNode.NumberOfChildren; i++)
            {
                var child = multipleChildrenNode.GetChild(i);
                if (child == null)
                {
                    continue;
                }
                var boxStyle = new GUIStyle(GUI.skin.box) { padding = new RectOffset(10, 10, 10, 10) };
                // Set the color of the box based on the child type
                // We can hash the name of the type to get a consistent color
                var hash = child.GetType().Name.GetHashCode();
                var texture = GetNodeBackground(hash);
                
                boxStyle.normal.background = texture;
                
                EditorGUILayout.BeginHorizontal(boxStyle); // NODE BOX
                // Box around the node
                DrawNode(child, indent + 1);
                
                // Buttons for moving the node up and down
                EditorGUILayout.BeginVertical(); // BUTTONS BOX
                _upDownButtonStyle ??= new GUIStyle(GUI.skin.button) { fontSize = 8 };
                if (i > 0)
                {
                    if (GUILayout.Button("▲",  _upDownButtonStyle, GUILayout.Width(20), GUILayout.Height(10)))
                    {
                        multipleChildrenNode.RemoveChild(child);
                        multipleChildrenNode.AddChild(child, i - 1);
                        EditorUtility.SetDirty(MobBehaviourTree);
                        // AssetDatabase.SaveAssets();
                        return;
                    }
                }
                else
                {
                    GUILayout.Space(10);
                }
                if (i < multipleChildrenNode.NumberOfChildren-1)
                {
                    if (GUILayout.Button("▼", _upDownButtonStyle, GUILayout.Width(20), GUILayout.Height(10)))
                    {
                        multipleChildrenNode.RemoveChild(child);
                        multipleChildrenNode.AddChild(child, i + 1);
                        EditorUtility.SetDirty(MobBehaviourTree);
                        // AssetDatabase.SaveAssets();
                        return;
                    }
                }
                else
                {
                    GUILayout.Space(10);
                }
                EditorGUILayout.EndVertical();  // BUTTONS BOX
                
                
                if (GUILayout.Button("X", GUILayout.Width(20)))
                {
                    multipleChildrenNode.RemoveChild(child);
                    EditorUtility.SetDirty(MobBehaviourTree);
                    // AssetDatabase.SaveAssets();
                    return;
                }
                EditorGUILayout.EndHorizontal();  // NODE BOX
            }
            EditorGUILayout.EndVertical();
            EditorGUI.indentLevel = previousIndent;

        }

        private Texture2D GetNodeBackground(int hash)
        {
            if (_nodeBackgrounds == null)
            {
                _nodeBackgrounds = new Dictionary<string, Texture2D>();
            }
            if (_nodeBackgrounds.ContainsKey(hash.ToString()))
            {
                return _nodeBackgrounds[hash.ToString()];
            }
            var texture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            // This doesn't produce a very varied color
            // var hue = Math.Abs(hash/100000 % 360);
            var hue = Mathf.Abs(Mathf.Sin(hash));
            
            // sat from 0.25 to 0.5
            var sat = Mathf.Abs(hash) % 10000 / 20000f + 0.25f;
            
            // value from 0.2 to 0.4
            var value = Mathf.Abs(hash) % 20000 / 100000f + 0.2f;
            
            var color = Color.HSVToRGB(hue, sat, value);
            color.a = 0.5f;
            texture.SetPixel(0, 0, color);
            texture.Apply();
            _nodeBackgrounds.Add(hash.ToString(), texture);
            return texture;
        }

        private void DrawAddNodeDropdown(IMultipleChildrenNode node)
        {
            var style = new GUIStyle(EditorStyles.popup) {fixedHeight = 20};
            var rect = EditorGUILayout.GetControlRect(GUILayout.Width(20));
            if (EditorGUI.DropdownButton(rect, new GUIContent("▼"), FocusType.Keyboard, style))
            {
                var menu = new GenericMenu();
                for (var i = 0; i < _nodeTypeNames.Count; i++)
                {
                    var index = i;
                    menu.AddItem(new GUIContent(_nodeTypeNames[i]), false, () =>
                    {
                        CreateNode(node, _nodeTypes[index]);
                    });
                }
                menu.ShowAsContext();
            }
        }

        private void CreateNode(IMultipleChildrenNode node, Type nodeType, Actors.Mob mob=null)
        {
            // Check if it's a child of NodeBase
            if (nodeType.IsSubclassOf(typeof(NodeBase)))
            {
                // Debug.Log($"Creating Node: {nodeType}");
                var newNode = (IBehaviourTreeNode)Activator.CreateInstance(nodeType);
                node.AddChild(newNode);
            }
            // Check if it's a MobBehaviourTree, in which case we show a dialog to select the asset
            else if (nodeType == typeof(MobBehaviourTree))
            {
                var newNode = (MobBehaviourTree)Activator.CreateInstance(nodeType);
                var path = EditorUtility.OpenFilePanel("Select Mob Behaviour Tree", "Assets", "asset");
                if (path.Length != 0)
                {
                    var relativePath = path.Substring(Application.dataPath.Length - "Assets".Length);
                    var asset = AssetDatabase.LoadAssetAtPath<MobBehaviourTreeObject>(relativePath);
                    if (asset is not null)
                    {
                            
                        newNode = asset.MobBehaviourTree;
                    }
                }
                node.AddChild(newNode);
            }
            else
            {
                // Debug.Log($"Creating Node: {nodeType}");
                var newNode = (IBehaviourTreeNode)Activator.CreateInstance(nodeType);
                node.AddChild(newNode);
            }
            
            EditorUtility.SetDirty(MobBehaviourTree);
            // AssetDatabase.SaveAssets();
        }
    }
}
#endif