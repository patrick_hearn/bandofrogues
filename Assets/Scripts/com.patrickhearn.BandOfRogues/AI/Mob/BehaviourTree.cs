﻿using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.AI.BehaviourTree;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.AI.AI.Mob
{
    public abstract class BehaviourTree : IBehaviourTreeNode
    {
        public virtual string Name => "Behaviour Tree";
        [SerializeReference] public RootNode RootNode;
        [SerializeField]
        protected bool IsRunning;

        public NodeState NodeState { get; }
        public NodeState Evaluate()
        {
            if (!IsRunning)
            {
                RootNode.Blackboard = Blackboard;
                IsRunning = true;
                var state = RootNode.Evaluate();
                while (state == NodeState.Running)
                {
                    state = RootNode.Evaluate();
                }
                IsRunning = false;
            }
            return NodeState.Running;
        }

        public IEnumerable<IBehaviourTreeNode> GetDescendants()
        {
            if (RootNode == null)
            {
                return new List<IBehaviourTreeNode>();
            }
            return RootNode.GetDescendants();
        }

        public IEnumerable<IBehaviourTreeNode> GetChildren()
        {
            if (RootNode == null)
            {
                return new List<IBehaviourTreeNode>();
            }
            return RootNode.GetChildren();
        }

        public IBlackboard Blackboard { get; set; }
    }
    
    public interface IActionableNode
    {
        public new ActionableBlackboard Blackboard { get; }
    }
}