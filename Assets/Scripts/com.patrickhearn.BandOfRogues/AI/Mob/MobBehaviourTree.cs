using System;
using com.patrickhearn.BandOfRogues.AI.BehaviourTree;
using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.AI.AI.Mob
{
    public abstract class ActionableBehaviourTree : BehaviourTree, IActionableNode
    {
        public new ActionableBlackboard Blackboard { get; set; }

        public ActionableBehaviourTree(IActionable actionable) 
        {
            Blackboard = new ActionableBlackboard(actionable);
        }
    }

    [Serializable]
    public class MobBehaviourTree : ActionableBehaviourTree
    {
        public override string Name => "Mob Behaviour Tree";
    
        public MobBehaviourTree() : base(null) {}
        public MobBehaviourTree(Actors.Mob mob) : base(mob) {}
        
        public void SetMob(Actors.Mob mob)
        {
            Blackboard = new ActionableBlackboard(mob);
        }
        
        public void Execute()
        {
            var commandBlackboard = Blackboard;
            commandBlackboard.CommandContext.GetCommand().Execute(commandBlackboard.CommandContext);
        }
        
        
    }
}
