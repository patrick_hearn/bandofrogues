﻿using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.AI.AI.Mob
{
    [CreateAssetMenu(fileName = "MobBehaviourTree", menuName = "Band of Rogues/AI/Mob Behaviour Tree")]
    public class MobBehaviourTreeObject : BehaviourTreeObject
    {
        [FormerlySerializedAs("_mobBehaviourTree")] [SerializeReference]
        private MobBehaviourTree mobBehaviourTree;
        public new BehaviourTree BehaviourTree => MobBehaviourTree;
        public MobBehaviourTree MobBehaviourTree => mobBehaviourTree ??= new MobBehaviourTree();
        
        public void SetMob(Actors.Mob mob)
        {
            MobBehaviourTree.SetMob(mob);
        }



    }
}