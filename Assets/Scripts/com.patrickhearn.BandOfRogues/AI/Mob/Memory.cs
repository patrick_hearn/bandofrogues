﻿using com.patrickhearn.BandOfRogues.Actors;

namespace com.patrickhearn.BandOfRogues.AI.AI.Mob
{
    public class Memory
    {
        
    }
    
    public class ActorMemory : Memory
    {
        public Actor Actor;
        public bool Known;
        public int LastSeenTurn;
        public int LastKnownX;
        public int LastKnownY;
    }
    
    public class MapMemory : Memory
    {
        public int Width;
        public int Height;
        public int[,] Tiles;
        public int[,] Actors;
    }
    
    
}