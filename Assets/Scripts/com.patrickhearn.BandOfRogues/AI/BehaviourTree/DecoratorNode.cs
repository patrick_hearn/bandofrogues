namespace com.patrickhearn.BandOfRogues.AI.BehaviourTree
{
    public abstract class DecoratorNode : NodeBase
    {
        // Decorator nodes have only one child
        public override string Name { get; }
        public NodeBase Child { get; }
        protected override NodeState EvaluateInternal()
        {
            NodeState = Child.Evaluate();
            return NodeState;
        }
    }
}