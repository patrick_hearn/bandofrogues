﻿using System.Collections.Generic;
using System.ComponentModel;

namespace com.patrickhearn.BandOfRogues.AI.BehaviourTree
{
    public class SequenceNode : CompositeNode 
    {
        [DisplayName]public static string DisplayName => "Sequence Node";
        public override string Name => "Sequence Node";
        public SequenceNode() : base(new List<IBehaviourTreeNode>()) {}
        public SequenceNode(List<IBehaviourTreeNode> nodes) : base(nodes) {}

        protected override NodeState EvaluateInternal()
        {
            var failed = false;
            foreach (var node in Children)
            {
                // Debug.Log($"Evaluating SequenceNode: {node.GetType().Name}");
                var nodeState = node.Evaluate();
                if (nodeState == NodeState.Failure)
                {
                    // Debug.Log($"SequenceNode failed: {node.GetType().Name}");
                    failed = true;
                    break;
                }
                // Debug.Log($"SequenceNode succeeded: {node.GetType().Name}");
            }

            return failed ? NodeState.Failure : NodeState.Success;
        }
        
        
    }
}