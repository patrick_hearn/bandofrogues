﻿using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.AI.BehaviourTree
{
    
    public interface IBehaviourTreeNode
    {   
        public string Name { get; }
        public NodeState NodeState { get; }
        public NodeState Evaluate();
        IEnumerable<IBehaviourTreeNode> GetDescendants();
        IEnumerable<IBehaviourTreeNode> GetChildren();
        
        IBlackboard Blackboard { get; set; }
    }
    
    public interface IBlackboardNode : IBehaviourTreeNode
    {
        NodeState EvaluateWithBlackboard(IBlackboard blackboard);
    }

}