using System;
using System.Collections.Generic;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.AI.BehaviourTree
{
    [Serializable]
    public class RootNode : NodeBase, ISingleChildNode
    {
        public override string Name => "Root Node";
        [field: SerializeReference]
        public IBehaviourTreeNode Child { get; set; }

        public void SetChild(IBehaviourTreeNode child)
        {
            Child = child;
        }
        public void RemoveChild()
        {
            Child = null;
        }

        protected override NodeState EvaluateInternal()
        {
            NodeState = Child.Evaluate();
            return NodeState;
        }
        
        protected override IEnumerable<IBehaviourTreeNode> GetChildrenInternal()
        {
            return new List<IBehaviourTreeNode> {Child};
        }
        public IBehaviourTreeNode this[int i] => Child;
    }
}