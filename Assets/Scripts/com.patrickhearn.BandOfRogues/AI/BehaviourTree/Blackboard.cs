using System.Collections;
using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Commands;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.AI.BehaviourTree
{
    public class Blackboard : IBlackboard
    {
        public Blackboard()
        {
            
        }
    }
    public abstract class CommandBlackboard<T> : ICommandBlackboard<T> where T : IActionable
    {
        public ICommandContext<T> CommandContext { get; set; }
    }

    public class ActionableBlackboard : CommandBlackboard<IActionable>
    {
        public IActionable Actionable { get; set; }
        
        public ActionableBlackboard(IActionable actor)
        {
            CommandContext = CommandFactory.CreateCommandContext(actor);
        }
    }
}
