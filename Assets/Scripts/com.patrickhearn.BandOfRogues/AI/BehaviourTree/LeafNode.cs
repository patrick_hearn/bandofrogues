using System;

namespace com.patrickhearn.BandOfRogues.AI.BehaviourTree
{
    public abstract class LeafNode : NodeBase
    {
        public override string Name { get; }
        protected abstract override NodeState EvaluateInternal();
    }
}