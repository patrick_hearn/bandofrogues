﻿using System.Collections.Generic;

namespace com.patrickhearn.BandOfRogues.AI.BehaviourTree
{
    public interface ISingleChildNode : IBehaviourTreeNode
    {
        IBehaviourTreeNode Child { get; }
        void SetChild(IBehaviourTreeNode child);
        void RemoveChild();
    }

    public interface IMultipleChildrenNode : IBehaviourTreeNode
    {
        public List<IBehaviourTreeNode> Children { get; }
        public int NumberOfChildren { get; }
        public void AddChild(IBehaviourTreeNode child);
        public void AddChild(IBehaviourTreeNode child, int index);
        public void RemoveChild(IBehaviourTreeNode child);
        public void RemoveChild(int index);
        public void RemoveAllChildren();
        IBehaviourTreeNode GetChild(int i);
        
        IBehaviourTreeNode this[int i] { get; }
    }
}