using System;
using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEditor.Experimental.GraphView;

namespace com.patrickhearn.BandOfRogues.AI.BehaviourTree
{
    public enum NodeState
    {
        Running,
        Success,
        Failure
    }
    
    [Serializable]
    public abstract class NodeBase : IBehaviourTreeNode
    {
        // All nodes have a name and a state
        public abstract string Name { get; }
        public NodeState NodeState { get; protected set; }
        public  NodeState Evaluate()
        {
            SetBlackboardToChildren();
            NodeState = EvaluateInternal();
            return NodeState;
        }

        private void SetBlackboardToChildren()
        {
            foreach (var child in GetChildren())
                child.Blackboard = Blackboard;
        }
        protected abstract NodeState EvaluateInternal();

        protected NodeBase() => NodeState = NodeState.Running;
        
        
        public IEnumerable<IBehaviourTreeNode> GetDescendants()
        {
            return GetDescendantsIntenal();
        }
        protected IEnumerable<IBehaviourTreeNode> GetDescendantsIntenal()
        {
            var nodes = new List<IBehaviourTreeNode>();
            foreach (var child in GetChildren())
            {
                nodes.Add(child);
                nodes.AddRange(child.GetDescendants());
            }
            return nodes;
        }
        
        public IEnumerable<IBehaviourTreeNode> GetChildren()
        {
            return GetChildrenInternal();
        }

        public IBlackboard Blackboard { get; set; }

        protected virtual IEnumerable<IBehaviourTreeNode> GetChildrenInternal()
        {
            return new IBehaviourTreeNode[] { };
        }
    }
    
    public abstract class BlackboardNodeBase : NodeBase
    {
        public abstract Blackboard Blackboard { get; set; }
    }
}