﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace com.patrickhearn.BandOfRogues.AI.BehaviourTree
{
    public class SelectorNode : CompositeNode 
    {
        [DisplayName]public static string DisplayName => "Selector Node";
        public override string Name => "Selector Node";
        public SelectorNode() : base(new List<IBehaviourTreeNode>()) {}
        public SelectorNode(List<IBehaviourTreeNode> nodes) : base(nodes) {}

        protected override NodeState EvaluateInternal()
        {
            return Children.Select(node => node.Evaluate()).Any(nodeState => nodeState == NodeState.Success) ? NodeState.Success : NodeState.Failure;
        }
    }
    
    // #if UNITY_EDITOR
    // [UnityEditor.CustomPropertyDrawer(typeof(SelectorNode), true)]
    // public class SelectorPropertyDrawer : CompositeNodePropertyDrawer
    // {
    //     protected override int NumLines => 1;
    //     
    // }
    // #endif
    
    
    
}