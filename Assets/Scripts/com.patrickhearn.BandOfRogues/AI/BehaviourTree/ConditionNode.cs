﻿namespace com.patrickhearn.BandOfRogues.AI.BehaviourTree
{
    public abstract class ConditionNode : NodeBase
    {
        public override string Name { get; }

        protected override NodeState EvaluateInternal()
        {
            NodeState = CheckCondition() ? NodeState.Success : NodeState.Failure;

            return NodeState;
        }

        protected abstract bool CheckCondition();
    }
}