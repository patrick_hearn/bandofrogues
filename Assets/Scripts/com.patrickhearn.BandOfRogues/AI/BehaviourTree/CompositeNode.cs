﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.AI.BehaviourTree
{
    [Serializable]
    public abstract class CompositeNode : NodeBase, IMultipleChildrenNode
    {
        public override string Name => "Composite Node";
        [field: SerializeReference] public List<IBehaviourTreeNode> Children { get; private set; }
        public int NumberOfChildren => Children.Count;

        public void AddChild(IBehaviourTreeNode child)
        {
            Children.Add(child);
        }

        public void AddChild(IBehaviourTreeNode child, int index)
        {
            if (index < 0 || index > Children.Count)
            {
                throw new IndexOutOfRangeException();
            }
            Children.Insert(index, child);
        }

        public void RemoveChild(IBehaviourTreeNode child)
        {
            Children.Remove(child);
        }

        public void RemoveChild(int index)
        {
            if (index < 0 || index > Children.Count)
            {
                throw new IndexOutOfRangeException();
            }
            Children.RemoveAt(index);
        }

        public void RemoveAllChildren()
        {
            Children.Clear();
        }

        public IBehaviourTreeNode GetChild(int i)
        {
            if (i < 0 || i > Children.Count)
            {
                throw new IndexOutOfRangeException();
            }
            return Children[i];
        }

        public IBehaviourTreeNode this[int i]
        {
            get => Children[i];
            set => Children[i] = value;
        }

        public CompositeNode()
        {
            Children = new List<IBehaviourTreeNode>();
        }

        protected CompositeNode(List<IBehaviourTreeNode> nodes)
        {
            Children = nodes;
        }
        
        protected override IEnumerable<IBehaviourTreeNode> GetChildrenInternal()
        {
            return Children;
        }


    }
    
}