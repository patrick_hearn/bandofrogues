﻿namespace com.patrickhearn.BandOfRogues.AI.BehaviourTree
{
    public abstract class ActionNode : NodeBase
    {
        public override string Name => "Leaf Node";
        public ActionNode() : base() {}
    }
}