﻿using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.AI
{
    public abstract class AIController : IController<IActor>
    {
        public abstract ICommand<IActor> GetCommand(IActor actor);
    }
}