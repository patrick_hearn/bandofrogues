using UnityEngine;

namespace com.patrickhearn.BandOfRogues.AI.StateMachine
{
    public class StateMachine
    {
        public State CurrentState { get; private set; }
        public State PreviousState { get; private set; }
        public State GlobalState { get; private set; }
        public GameObject Owner { get; private set; }

        public StateMachine(GameObject owner)
        {
            Owner = owner;
            CurrentState = null;
            PreviousState = null;
            GlobalState = null;
        }

        public void ChangeState(State newState)
        {
            PreviousState = CurrentState;
            CurrentState?.Exit(Owner);
            CurrentState = newState;
            CurrentState?.Enter(Owner);
        }

        public void Update()
        {
            GlobalState?.Execute(Owner);
            CurrentState?.Execute(Owner);
        }

        public void RevertToPreviousState()
        {
            ChangeState(PreviousState);
        }

        public bool IsInState(State state)
        {
            return CurrentState == state;
        }

        public bool HandleMessage(Telegram telegram)
        {
            if (CurrentState != null && CurrentState.OnMessage(Owner, telegram))
            {
                return true;
            }

            if (GlobalState != null && GlobalState.OnMessage(Owner, telegram))
            {
                return true;
            }

            return false;
        }

        public void SetGlobalState(State state)
        {
            GlobalState = state;
        }

        public void SetCurrentState(State state)
        {
            CurrentState = state;
        }

    }

    public abstract class State
    {
        public abstract void Enter(GameObject owner);
        public abstract void Execute(GameObject owner);
        public abstract void Exit(GameObject owner);
        public abstract bool OnMessage(GameObject owner, Telegram telegram);
    }

    public class Telegram
    {
        public GameObject Sender { get; private set; }
        public GameObject Receiver { get; private set; }
        public int Message { get; private set; }
        public object ExtraInfo { get; private set; }

        public Telegram(GameObject sender, GameObject receiver, int message, object extraInfo)
        {
            Sender = sender;
            Receiver = receiver;
            Message = message;
            ExtraInfo = extraInfo;
        }
    }
}

