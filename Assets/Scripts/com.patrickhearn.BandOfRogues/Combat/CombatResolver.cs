﻿using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.Combat
{
    public static class CombatResolver
    {
        private static System.Random _rng = new System.Random();

        public static bool SuccessfulAttack(ICombatant attacker, ICombatant defender)
        {
            var acuRoll = (float)_rng.NextDouble() * attacker.GetAccuracy();
            var defRoll = (float)_rng.NextDouble() * defender.GetEvasion();

            return acuRoll >= defRoll;
        }

        public static int CalculateDamage(ICombatant attacker, ICombatant defender)
        {
            if (!SuccessfulAttack(attacker, defender)) 
            {
                return 0;
            }

            var damageRoll = _rng.Next((int)attacker.GetMinHit(), (int)(attacker.GetMaxHit()));
            var absorption = _rng.Next((int)defender.GetMinDefense(), (int)(defender.GetMaxDefense()));

            return System.Math.Max(damageRoll - absorption, 0);
        }

        public static bool ResolveCombat(ICombatant attacker, ICombatant defender)
        {
            if (!SuccessfulAttack(attacker, defender)) return false;
            var damage = CalculateDamage(attacker, defender);
            defender.TakeDamage(damage, attacker);
            return true;
        }
    }
}