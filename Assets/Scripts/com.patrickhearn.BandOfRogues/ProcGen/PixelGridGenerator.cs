using UnityEngine;

namespace com.patrickhearn.BandOfRogues.ProcGen
{
    public abstract class PixelGridGenerator : MonoBehaviour, IPixelGridGenerator
    {
        public virtual float[,] GeneratePixelGrid(int width, int height)
        {
            throw new System.NotImplementedException();
        }
    }
}
