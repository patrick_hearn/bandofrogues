﻿using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.ProcGen
{
    public class ImageCreator : MonoBehaviour
    {
        public int width, height;
        
        
        [FormerlySerializedAs("PixelGridGenerator")] public PixelGridGenerator pixelGridGenerator;
        [FormerlySerializedAs("Texture")] public Texture2D texture;

        [ContextMenu("Generate Texture")]
        public Texture2D GenerateTexture()
        {
            texture = new Texture2D(width, height);
            var pixelGrid = pixelGridGenerator.GeneratePixelGrid(width, height);
            texture.SetPixels(GenerateColorArray(pixelGrid));
            texture.Apply();
            return texture;
        }
        
        private Color[] GenerateColorArray(float[,] pixelGrid)
        {
            var colorArray = new Color[pixelGrid.GetLength(0) * pixelGrid.GetLength(1)];
            for (int x = 0; x < pixelGrid.GetLength(0); x++)
            {
                for (int y = 0; y < pixelGrid.GetLength(1); y++)
                {
                    colorArray[x * y] = Color.Lerp(Color.black, Color.white, pixelGrid[x, y]);
                }
            }

            return colorArray;
        }
        
        public void AddSimplexNoiseGenerator()
        {
            pixelGridGenerator = gameObject.AddComponent<SimplexNoise>();
        }
        
        private void Start()
        {
            GenerateTexture();
        }
    }
}