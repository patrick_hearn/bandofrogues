﻿namespace com.patrickhearn.BandOfRogues.ProcGen
{
    public interface IPixelGridGenerator
    {
        public float[,] GeneratePixelGrid(int width, int height);
    }
}