﻿using ProceduralToolkit.Runtime.FastNoiseLib;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.ProcGen
{
    public class SimplexNoise : PixelGridGenerator
    {
        #region Noise Settings
        
        public FastNoise Noise = new();
        public const FastNoise.NoiseType NoiseType = FastNoise.NoiseType.Simplex;
        [FormerlySerializedAs("Interp")] public FastNoise.Interp interp = FastNoise.Interp.Quintic;
        
        [FormerlySerializedAs("Frequency")] public float frequency;
        [FormerlySerializedAs("Seed")] public int seed;
        [FormerlySerializedAs("Offset")] public Vector2 offset;
        [FormerlySerializedAs("Scale")] public Vector2 scale = Vector2.one;
        
        #endregion

        private float[,] _pixelGrid;
        
        public float[,] GeneratePixelGrid(int width, int height, int seed)
        {
            Noise.SetSeed(seed != -1 ? seed : Random.Range(0, 100000));

            if(_pixelGrid == null)
                _pixelGrid = new float[width, height];
            else
            {
                if (_pixelGrid.GetLength(0) != width || _pixelGrid.GetLength(1) != height)
                    _pixelGrid = new float[width, height];
                
            }
            Noise.SetNoiseType(NoiseType);
            Noise.SetFrequency(frequency);
            
            Noise.SetInterp(interp);
            
            
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height ; y++)
                {
                    var xCoord = (float) x / width * scale.x + offset.x;
                    var yCoord = (float) y / height * scale.y + offset.y;
                    _pixelGrid[x, y] = Noise.GetNoise(xCoord, yCoord);
                }
            }
            
            return _pixelGrid;
        }

        public override float[,] GeneratePixelGrid(int width, int height)
        {
            return GeneratePixelGrid(width, height, seed);
        }
    }
    
}