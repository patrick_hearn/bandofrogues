﻿using System.Collections.Generic;

namespace com.patrickhearn.Text
{
    public enum EAdjectiveOrder
    {
        First=0,
        Quantity=1,
        Opinion=2,
        Size=3,
        Age=4,
        Shape=5,
        Color=6,
        Origin=7,
        Material=8,
        Purpose=9,
        Last=10

    }
    
    public interface IDescribable
    {
        string Description { get; }
        List<IDescriptor> Descriptors { get; }
    }
    
    public interface IDescriptor
    {
        string Descriptor { get; }
        EAdjectiveOrder Order { get; }
    }
}