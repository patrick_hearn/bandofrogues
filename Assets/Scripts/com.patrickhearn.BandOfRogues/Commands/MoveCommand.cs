﻿using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Commands
{
    
    public static class CommandFactory
    {
        public static ICommandContext<T> CreateCommandContext<T>(T entity) where T : IActionable
        {
            if (entity is IMover)
            {
                return new MoveCommandContext() as ICommandContext<T>;
            }
            if (entity is IInteractable)
            {
                return new InteractCommandContext() as ICommandContext<T>;
            }
            return null;
        }
        
        public static ICommand<T> CreateCommand<T>(ICommandContext<T> context) where T : IActionable
        {
            if (context is MoveCommandContext)
            {
                return new MoveCommand() as ICommand<T>;
            }
            if (context is InteractCommandContext)
            {
                return new InteractCommand() as ICommand<T>;
            }
            return null;
        }
    }
    
    public abstract class CommandContext<T> : ICommandContext<T> where T : IActionable
    {
        public T Entity { get; set; }

        public ICommand<T> GetCommand()
        {
            return CommandFactory.CreateCommand(this);
        }
    }
    
    public class MoveCommandContext : CommandContext<IMover>           
    {                                              
        public Vector2Int Destination { get; set; }                     
    } 

    public class InteractCommandContext : CommandContext<IInteractable>
    {

        public IInteractable Target { get; set; }
        public Vector2Int Position { get; set; }
        public IDictionary<string, object> Parameters { get; set; }
    }

    public abstract class Command<T> : ICommand<T> where T : IActionable
    {
        public abstract void Execute(ICommandContext<T> context);
    } 
    public class MoveCommand : Command<IMover>
    {                                                                   
        public override void Execute(ICommandContext<IMover> context)                 
        {   
            var moveContext = context as MoveCommandContext;
            moveContext?.Entity.AttemptMove(moveContext.Destination);             
        }                                                               
    } 

    public class InteractCommand : Command<IInteractable>
    {
        public override void Execute(ICommandContext<IInteractable> context)
        {
            var interactContext = context as InteractCommandContext;
            interactContext?.Target?.Interact(interactContext.Entity, interactContext.Position, interactContext.Parameters);
        }
    }
    

}
