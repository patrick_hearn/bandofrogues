﻿using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Inventory
{
    public abstract class InventoryItemScriptableObject : ScriptableObject
    {
        public abstract InventoryItem InventoryItem { get; }
    }
}