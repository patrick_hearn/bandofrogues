using System;
using System.Collections.Generic;
using System.Linq;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Modifiers;
using com.patrickhearn.BandOfRogues.Tagging;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Inventory
{
    public interface IInventoryItem : IEntity, ITaggable, IMetric, IModifierContainer
    {
        float BaseWeight { get; set; }
        float BaseValue { get; set; }
        Sprite Icon { get; set; }
        string Description { get; set; }
    }

    [Serializable]
    public abstract class InventoryItem : IInventoryItem
    {
        [SerializeField] private List<ItemModifier> modifiers;
        [field: SerializeField] public string Name { get; set; }
        [field: SerializeField] public float BaseWeight { get; set; }
        [field: SerializeField] public float BaseValue { get; set; }
        [field: SerializeField] public Sprite Icon { get; set; }
        [field: SerializeField] public string Description { get; set; }

        public List<string> Tags { get; set; }

        public List<IModifier> Modifiers
        {
            get => modifiers.Cast<IModifier>().ToList();
            set => modifiers = value.Cast<ItemModifier>().ToList();
        }

        public float ApplyEarlyModifiers(EMetric metric, float value)
        {
            return Modifiers.Where(modifier => modifier.GetPhase() == ETriPhase.Early)
                .Aggregate(value, (current, modifier) => modifier.GetModifierEffect(metric, current));
        }

        public float ApplyDefaultModifiers(EMetric metric, float value)
        {
            return Modifiers.Where(modifier => modifier.GetPhase() == ETriPhase.Default)
                .Aggregate(value, (current, modifier) => modifier.GetModifierEffect(metric, current));
        }

        public float ApplyLateModifiers(EMetric metric, float value)
        {
            return Modifiers.Where(modifier => modifier.GetPhase() == ETriPhase.Late)
                .Aggregate(value, (current, modifier) => modifier.GetModifierEffect(metric, current));
        }

        public float ApplyMetric(EMetric metric, float value)
        {
            value = ApplyEarlyModifiers(metric, value);
            value = ApplyInternal(metric, value);

            value = ApplyDefaultModifiers(metric, value);
            value = ApplyLateModifiers(metric, value);
            return value;
        }

        protected float ApplyInternal(EMetric metric, float value)
        {
            return value + GetMetric(metric);
        }
        
        public float GetMetric(EMetric metric)
        {
            return metric switch
            {
                EMetric.Accuracy => GetBaseAccuracy(),
                EMetric.Evasion => GetBaseEvasion(),
                EMetric.Speed => GetBaseSpeed(),
                EMetric.AttackRange => GetBaseAttackRange(),
                EMetric.MinHit => GetBaseMinHit(),
                EMetric.MaxHit => GetBaseMaxHit(),
                EMetric.MinDefense => GetBaseMinDefense(),
                EMetric.MaxDefense => GetBaseMaxDefense(),
                EMetric.AttackSpeed => GetBaseAttackSpeed(),
                EMetric.Weight => GetBaseWeight(),
                EMetric.Value => GetBaseValue(),
                EMetric.MaxHealth => GetBaseMaxHealth(),
                EMetric.MaxMana => GetBaseMaxMana(),
                EMetric.ManaRegenRate => GetBaseManaRegenRate(),
                EMetric.HealthRegenRate => GetBaseHealthRegenRate(),
                _ => throw new ArgumentOutOfRangeException(nameof(metric), metric, null)
            };
        }

        protected virtual float GetBaseAccuracy()
        {
            return 0;
        }

        protected virtual float GetBaseEvasion()
        {
            return 0;
        }

        protected virtual float GetBaseSpeed()
        {
            return 0;
        }

        protected virtual float GetBaseAttackRange()
        {
            return 0;
        }

        protected virtual float GetBaseMinHit()
        {
            return 0;
        }

        protected virtual float GetBaseMaxHit()
        {
            return 0;
        }

        protected virtual float GetBaseMinDefense()
        {
            return 0;
        }

        protected virtual float GetBaseMaxDefense()
        {
            return 0;
        }

        protected virtual float GetBaseAttackSpeed()
        {
            return 0;
        }

        protected virtual float GetBaseWeight()
        {
            return BaseWeight;
        }

        protected virtual float GetBaseValue()
        {
            return BaseValue;
        }
        
        protected virtual float GetBaseMaxHealth()
        {
            return 0;
        }
        
        protected virtual float GetBaseMaxMana()
        {
            return 0;
        }
        
        protected virtual float GetBaseManaRegenRate()
        {
            return 0;
        }
        
        protected virtual float GetBaseHealthRegenRate()
        {
            return 0;
        }

    }
}