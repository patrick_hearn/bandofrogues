﻿using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Inventory
{
    [CreateAssetMenu(menuName = "Band of Rogues/Inventory/Armour")]
    public class ArmourScriptableObject : InventoryItemScriptableObject
    {
        public override InventoryItem InventoryItem => armour;
        [FormerlySerializedAs("Armour")] public Armour armour = new ();
    }
}