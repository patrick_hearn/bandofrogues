﻿using System;
using com.patrickhearn.BandOfRogues.Modifiers;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Inventory
{
    [Serializable]
    [CreateAssetMenu(fileName = "New Item Modifier", menuName = "Band of Rogues/Inventory/Item Modifier")]
    public class ItemModifier : Modifier
    {
    }
}