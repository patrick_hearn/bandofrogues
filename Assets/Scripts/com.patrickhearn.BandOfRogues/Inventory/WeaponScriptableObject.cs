﻿using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Inventory
{
    [CreateAssetMenu(menuName = "Band of Rogues/Inventory/Weapon")]
    public class WeaponScriptableObject : InventoryItemScriptableObject
    {
        public override InventoryItem InventoryItem => weapon;
        [FormerlySerializedAs("Weapon")] public Weapon weapon;
    }
}