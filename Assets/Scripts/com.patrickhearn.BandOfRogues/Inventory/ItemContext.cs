using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.Inventory
{
    public class ItemContext : Context
    {
        public InventoryItem Item { get; private set; }

        public ItemContext(InventoryItem item)
        {
            Item = item;
        }
    }
}