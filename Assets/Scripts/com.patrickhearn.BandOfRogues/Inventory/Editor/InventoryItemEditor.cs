﻿using System;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Tagging;
using com.patrickhearn.BandOfRogues.Tagging.Editor;
using UnityEditor;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Inventory.Editor
{
    [CustomEditor(typeof(InventoryItemScriptableObject), true)]
    public class InventoryItemScriptableObjectEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var item = (InventoryItemScriptableObject) target;
            // Display all Metrics
            EditorGUILayout.LabelField("Metrics", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            // Iterate through all items in the EMetric enum
            foreach (EMetric metric in Enum.GetValues(typeof(EMetric)))
            {
                var metricValue = item.InventoryItem.ApplyMetric(metric, 0);
                if (metricValue != 0)
                {
                    EditorGUILayout.LabelField($"{metric}: {metricValue}");
                }
            }

            var taggable = (ITaggable)item.InventoryItem;
            if (taggable == null)
            {
                Debug.Log("Taggable is null");
                return;
            }
                
            TagEditorUtils.DrawTags(taggable);
            TagEditorUtils.DrawTagSelector(taggable);
        }
    }
}