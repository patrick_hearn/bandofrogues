﻿// using System.Collections.Generic;
// using BandOfRogues.Interfaces;
// using BandOfRogues.Modifiers;
// using com.patrickhearn.BandOfRogues.Modifiers.Editor;
// using UnityEditor;
// using UnityEngine;
//
// namespace BandOfRogues.Inventory
// {
//     [CustomEditor(typeof(ItemModifier))]
//     public class ItemModifierEditor : Editor
//     {
//         private ItemModifier _modifier;
//         private SerializedObject _getTarget;
//         private SerializedProperty _effects;
//         private SerializedProperty _phase;
//         private SerializedProperty _validators;
//         private SerializedProperty _tags;
//
//         private void OnEnable()
//         {
//             _modifier = (ItemModifier)target;
//             _getTarget = new SerializedObject(_modifier);
//             _effects = _getTarget.FindProperty("effects");
//             _phase = _getTarget.FindProperty("triPhase");
//             _validators = _getTarget.FindProperty("validators");
//             _tags = _getTarget.FindProperty("tags");
//         }
//
//         public override void OnInspectorGUI()
//         {
//             _getTarget.Update();
//
//             // EditorGUILayout.PropertyField(_modifierEffects, true);
//             // EditorGUILayout.PropertyField(_modifierPriority);
//             // EditorGUILayout.PropertyField(_modifierValidators, true);
//             // EditorGUILayout.PropertyField(_tags, true);
//             // These properites are interfaces, so we need to do some extra work to get them to show up in the inspector
//             // we'll implmement them all here, then refactor later
//
//             // Effects
//             EditorGUILayout.LabelField("Effects", EditorStyles.boldLabel);
//             EditorGUI.indentLevel++;
//             if (_effects is null || _effects.arraySize == 0)
//             {
//                 EditorGUILayout.HelpBox("No effects", MessageType.Info);
//             }
//             else
//             {
//                 for (var i = 0; i < _effects.arraySize; i++)
//                 {
//                     var effect = _effects.GetArrayElementAtIndex(i);
//                     // EditorGUILayout.PropertyField(effect, true);
//                     // IModifierEffect so we can't just use PropertyField
//                     EditorGUILayout.BeginHorizontal();
//                     EditorGUILayout.PropertyField(effect.FindPropertyRelative("Metric"));
//                     EditorGUILayout.PropertyField(effect.FindPropertyRelative("Value"));
//                     EditorGUILayout.EndHorizontal();
//
//
//                 }
//             }
//
//             EditorGUI.indentLevel--;
//             
//             // Phase
//             EditorGUILayout.PropertyField(_phase);
//             ModifierValidatorEditorUtility.DrawAddValidatorDropdown(_modifier);
//             // Validators
//             EditorGUILayout.LabelField("Validators", EditorStyles.boldLabel);
//             EditorGUI.indentLevel++;
//             
//             if (_validators is null || _validators.arraySize == 0)
//             {
//                 EditorGUILayout.HelpBox("No validators", MessageType.Info);
//             }
//             else
//             {
//                 for (var i = 0; i < _validators.arraySize; i++)
//                 {
//                     var validator = _validators.GetArrayElementAtIndex(i);
//                     EditorGUILayout.PropertyField(validator, true);
//                     // IModifierValidator so we can't just use PropertyField
//                     // EditorGUILayout.BeginHorizontal();
//                     // EditorGUILayout.LabelField(validator.GetType().Name);
//                     // EditorGUILayout.EndHorizontal();
//                 }
//             }
//
//             EditorGUI.indentLevel--;
//             
//             // Tags
//             EditorGUILayout.PropertyField(_tags, true);
//             
//             
//             
//             
//
//             _getTarget.ApplyModifiedProperties();
//         }
//     }
// }