using System;
using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Modifiers;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Inventory
{
    [Serializable]
    public class Armour : InventoryItem, IEquipable, IModifierContainer
    {
        [SerializeField] private EEquipSlot equipSlot;
        [FormerlySerializedAs("_minLevel")] [SerializeField] private int minLevel;
        public int MinLevel => minLevel;
        [FormerlySerializedAs("_baseEvasion")] [SerializeField] private float baseEvasion;
        [FormerlySerializedAs("_baseSpeed")] [SerializeField] private float baseSpeed;
        [FormerlySerializedAs("_baseMaxHealth")] [SerializeField] private float baseMaxHealth;
        [FormerlySerializedAs("_baseMaxMana")] [SerializeField] private float baseMaxMana;
        [FormerlySerializedAs("_baseMinDefense")] [SerializeField] private float baseMinDefense;
        [FormerlySerializedAs("_baseMaxDefense")] [SerializeField] private float baseMaxDefense;

        public EEquipSlot EquipSlot => equipSlot;

        [field: Range(0,100)][field: SerializeField]public int Defense { get; set; }


        public List<IModifier> Modifiers { get; }
        
        protected override float GetBaseEvasion()
        {
            return baseEvasion;
        }
        
        protected override float GetBaseSpeed()
        {
            return baseSpeed;
        }
        
        protected override float GetBaseMaxHealth()
        {
            return baseMaxHealth;
        }
        
        protected override float GetBaseMaxMana()
        {
            return baseMaxMana;
        }
        
        protected override float GetBaseMinDefense()
        {
            return baseMinDefense;
        }
        
        protected override float GetBaseMaxDefense()
        {
            return baseMaxDefense;
        }
        
        
        
    }
}