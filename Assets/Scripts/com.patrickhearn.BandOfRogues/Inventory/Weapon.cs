using System;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Inventory
{
    [Serializable]
    public class Weapon : InventoryItem, IEquipable
    {
        public int damage = 10;
        public int Damage => damage;
        public int attackRange = 1;
        public int AttackRange => attackRange;
        [SerializeField] private int minLevel;
        public int MinLevel => minLevel;
        [SerializeField] private EEquipSlot equipSlot;
        public EEquipSlot EquipSlot => equipSlot;
        public float accuracy = 1f;
        public float Accuracy => accuracy;
        
        [FormerlySerializedAs("_baseAccuracy")] [SerializeField] private float baseAccuracy;
        [FormerlySerializedAs("_baseAttackRange")] [SerializeField] private float baseAttackRange;
        [FormerlySerializedAs("_baseMinHit")] [SerializeField] private float baseMinHit;
        [FormerlySerializedAs("_baseMaxHit")] [SerializeField] private float baseMaxHit;
        [FormerlySerializedAs("_baseAttackSpeed")] [SerializeField] private float baseAttackSpeed;
        
        protected override float GetBaseAccuracy()
        {
            return baseAccuracy;
        }
        
        protected override float GetBaseAttackRange()
        {
            return baseAttackRange;
        }
        
        protected override float GetBaseMinHit()
        {
            return baseMinHit;
        }
        
        protected override float GetBaseMaxHit()
        {
            return baseMaxHit;
        }
        
        protected override float GetBaseAttackSpeed()
        {
            return baseAttackSpeed;
        }
    }
}