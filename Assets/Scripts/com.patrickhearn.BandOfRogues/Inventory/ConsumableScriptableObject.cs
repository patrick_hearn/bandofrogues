﻿using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Inventory
{
    [CreateAssetMenu(menuName = "Band of Rogues/Inventory/Consumable")]
    public class ConsumableScriptableObject : InventoryItemScriptableObject
    {
        public override InventoryItem InventoryItem => Consumable;
        public Consumable Consumable;
    }
}