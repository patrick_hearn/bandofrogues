﻿using System;
using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Modifiers
{
    [Serializable]
    public class ModifierEffect : IModifierEffect
    {
        [SerializeField] private float multiplier = 1f;
        [SerializeField] private int additiveModifier = 0;
        [SerializeField] private List<EMetric> metrics = new();

        public List<EMetric> Metrics
        {
            get => metrics;
            set => metrics = value;
        }


        public float Multiplier
        {
            get => multiplier;
            set => multiplier = value;
        }

        
        public int AdditiveModifier
        {
            get => additiveModifier;
            set => additiveModifier = value;
        }

        public float GetModifierEffect(EMetric metric, float value)
        {
            return Metrics.Contains(metric) ? value * Multiplier + AdditiveModifier : value;
        }
    }
}