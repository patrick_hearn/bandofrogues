﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Modifiers;
using UnityEditor;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Modifier.Modifiers.Editor
{
    public static class ModifierValidatorEditorUtility
    {
        private static List<string> ValidatorDisplayNames => GetValidatorDisplayNames();
        private static List<Type> ValidatorTypes => GetValidatorTypes();

        private static List<string> GetValidatorDisplayNames()
        {
            var validatorDisplayNames = new List<string>();
            foreach (var validatorType in ValidatorTypes)
            {
                if (validatorType.GetCustomAttributes(typeof(DisplayNameAttribute), false).FirstOrDefault() is
                    DisplayNameAttribute attribute)
                {
                    validatorDisplayNames.Add(attribute.DisplayName);
                }
                else
                {
                    validatorDisplayNames.Add(validatorType.Name);
                }
            }

            return validatorDisplayNames;
        }

        private static List<Type> GetValidatorTypes()
        {
            return (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                    from type in assembly.GetTypes()
                    where !type.IsAbstract && typeof(IModifierValidator).IsAssignableFrom(type)
                    select type).ToList();
        }

        public static void DrawAddValidatorDropdown(IModifier modifier)
        {
            var style = new GUIStyle(EditorStyles.popup) {fixedHeight = 20};
            var rect = EditorGUILayout.GetControlRect(GUILayout.Width(20));
            if (!EditorGUI.DropdownButton(rect, new GUIContent("▼"), FocusType.Keyboard, style)) return;
            var menu = new GenericMenu();
            for (var i = 0; i < ValidatorDisplayNames.Count; i++)
            {
                var index = i;
                menu.AddItem(new GUIContent(ValidatorDisplayNames[i]), false, () =>
                {
                    AddValidatorToModifier(modifier, ValidatorTypes[index]);
                });
            }
            menu.ShowAsContext();
        }

        private static void AddValidatorToModifier(IModifier modifier, Type validatorType)
        {
            if (validatorType.GetConstructor(Type.EmptyTypes) is not { } constructor) return;
            var validator = (IModifierValidator) constructor.Invoke(null);
            modifier.AddValidator(validator);
        }
    }

}