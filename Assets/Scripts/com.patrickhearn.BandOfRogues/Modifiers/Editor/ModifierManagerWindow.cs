﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Modifiers.Editor
{
    public class ModifierManagerWindow : EditorWindow
    {
        
        private List<Modifier> _modifiers;
        
        [MenuItem("Band of Rogues/Modifier Manager")]
        public static void ShowWindow()
        {
            GetWindow<ModifierManagerWindow>("Modifier Manager");
        }
        
        private List<Type> _modifierTypes;
        private int _selectedModifierTypeIndex;
        private void OnEnable()
        {
            _modifierTypes = new List<Type>();
            var types = typeof(Modifier).Assembly.GetTypes().Where(x => x.IsAbstract == false);
            foreach (var type in types)
            {
                if (type.IsSubclassOf(typeof(Modifier)))
                {
                    _modifierTypes.Add(type);
                }
            }
        }
        
        private void OnGUI()
        {
            _modifiers ??= new List<Modifier>();
            if (GUILayout.Button("Load Modifiers"))
            {
                _modifiers = ModifierManager.LoadModifiers();
            }
            if (GUILayout.Button("Save Modifiers"))
            {
                ModifierManager.SaveModifiers(_modifiers);
            }
            if (GUILayout.Button("Clear Modifiers"))
            {
                _modifiers = new List<Modifier>();
            }
            _selectedModifierTypeIndex = EditorGUILayout.Popup(_selectedModifierTypeIndex, _modifierTypes.Select(x => x.Name).ToArray());
            
            if (GUILayout.Button("Add Modifier"))
            {
                var modifier = (Modifier)Activator.CreateInstance(_modifierTypes[_selectedModifierTypeIndex]);
                _modifiers.Add(modifier);
            }
            
            foreach (var modifier in _modifiers)
            {
                EditorGUILayout.LabelField(modifier.name);
                
            }
            
        }
        
    }

    internal static class ModifierManager
    {
        public static List<Modifier> LoadModifiers()
        {
            var modifiers = new List<Modifier>();
            var guids = AssetDatabase.FindAssets("t:Modifier");
            foreach (var guid in guids)
            {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                var modifier = AssetDatabase.LoadAssetAtPath<Modifier>(path);
                modifiers.Add(modifier);
            }
            return modifiers;
        }

        public static void SaveModifiers(List<Modifier> modifiers)
        {
            foreach (var modifier in modifiers)
            {
                AssetDatabase.CreateAsset(modifier, "Assets/Modifiers/" + modifier.name + ".asset");
            }
            AssetDatabase.SaveAssets();
        }
    }
}