﻿namespace com.patrickhearn.BandOfRogues.Modifiers.Editor
{
    [UnityEditor.CustomEditor(typeof(Modifier))]
    public class ModifierEditor : UnityEditor.Editor
    {
        private UnityEditor.SerializedProperty _modifierEffect;
        private UnityEditor.SerializedProperty _modifierValidator;
        private UnityEditor.SerializedProperty _triPhase;
        private UnityEditor.SerializedProperty _tags;
        
        private void OnEnable()
        {
            _modifierEffect = serializedObject.FindProperty("effects");
            _modifierValidator = serializedObject.FindProperty("validators");
            _triPhase = serializedObject.FindProperty("triPhase");
            _tags = serializedObject.FindProperty("tags");
        }
        
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            UnityEditor.EditorGUILayout.PropertyField(_modifierEffect, true);
            UnityEditor.EditorGUILayout.PropertyField(_modifierValidator, true);
            UnityEditor.EditorGUILayout.PropertyField(_triPhase);
            UnityEditor.EditorGUILayout.PropertyField(_tags, true);
            serializedObject.ApplyModifiedProperties();
        }
        
        
        
    }
}