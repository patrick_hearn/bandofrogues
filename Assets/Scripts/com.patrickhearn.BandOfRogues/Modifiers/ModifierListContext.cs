using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.Modifiers
{
    public class ModifierListContext : Context
    {
        public List<IModifier> Modifiers { get; private set; }

        public ModifierListContext(List<IModifier> modifiers)
        {
            Modifiers = modifiers;
        }
    }
}