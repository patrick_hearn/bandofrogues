﻿using System;
using System.Linq;
using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.Modifiers
{
    [Serializable]
    public class ModifierIsUniqueValidator : ModifierValidator
    {
        public override bool IsValid(IModifier modifier, Context context)
        {
            if (context is ModifierListContext modifierListContext)
            {
                return modifierListContext.Modifiers.Count(m => m.GetType() == modifier.GetType()) <= 1;
            }
        
            throw new ArgumentException("Invalid context type");
        }
    }
}