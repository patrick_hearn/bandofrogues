﻿using System;
using System.Linq;
using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.Modifiers
{
    [Serializable]
    public class ModifierTagIsUniqueValidator : ModifierValidator
    {
        public override bool IsValid(IModifier modifier, Context context)
        {
            if (context is not ModifierListContext modifierListContext)
                throw new ArgumentException("Invalid context type");
            var modifierTags = modifier.GetTags();
            return modifierTags.All(tag => modifierListContext.Modifiers.Count(c => c.GetTags().Contains(tag)) <= 1);
        }
    }
}