﻿using com.patrickhearn.BandOfRogues.Modifiers;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IModifierValidator
    {
        bool IsValid(IModifier modifier, Context context);
    }
}