﻿using System.Collections.Generic;
using System.Linq;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Tagging;

namespace com.patrickhearn.BandOfRogues.Modifiers
{
    /// <summary>
    /// A modifier is a collection of effects that can be applied to a metric.
    /// </summary>
    public interface IModifier : ITaggable
    {
        /// <summary>
        /// Get the value after the modifier is applied on the metric.
        /// </summary>
        /// <param name="metric"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        float GetModifierEffect(EMetric metric, float value);
        /// <summary>
        /// Get the phase of the modifier.
        /// </summary>
        /// <returns></returns>
        ETriPhase GetPhase();

        
        /// <summary>
        /// Returns true if the modifier is valid for the given context.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        bool IsValid(Context context)
        {
            return Validators.All(validator => validator.IsValid(this, context));
        }
        
        /// <summary>
        /// Add a validator to the modifier.
        /// </summary>
        /// <param name="validator"></param>
        void AddValidator(IModifierValidator validator);
        
        /// <summary>
        /// Returns the list of validators for the modifier.
        /// </summary>
        public IEnumerable<IModifierValidator> Validators { get; }
        
        /// <summary>
        /// Returns the list of effects for the modifier.
        /// </summary>
        public List<IModifierEffect> Effects { get; }
    }

    /// <summary>
    /// A modifier effect is a single effect that can be applied to a metric.
    /// </summary>
    public interface IModifierEffect
    {
        /// <summary>
        /// Get the value after the effect is applied on the metric.
        /// </summary>
        /// <param name="metric"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public float GetModifierEffect(EMetric metric, float value);
    }

    /// <summary>
    /// The phase of the modifier.
    /// Early: Applied before the base value is calculated.
    /// Default: Applied after the base value is calculated.
    /// Late: Applied after all other modifiers are applied.
    /// </summary>
    public enum ETriPhase
    {
        Early,
        Default,
        Late
    }
}
