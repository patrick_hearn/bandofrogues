﻿using System;
using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.Modifiers
{
    [Serializable]
    public class ModifierValidator : IModifierValidator
    {
        public virtual bool IsValid(IModifier modifier, Context context)
        {
            return true;
        }
    }
}