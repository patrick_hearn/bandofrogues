using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Modifiers;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IModifierContainer
    {
        List<IModifier> Modifiers { get; }
    }
}