﻿using System.Collections.Generic;
using System.Linq;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Modifiers
{
    public abstract class Modifier : ScriptableObject, IModifier
    {
        
        [SerializeField] private List<ModifierValidator> validators = new();
        [SerializeField] private List<ModifierEffect> effects = new();
        [SerializeField] private ETriPhase triPhase = ETriPhase.Default;
        [SerializeField] private List<string> tags = new ();

        public List<IModifierEffect> Effects
        {
            get => effects.Cast<IModifierEffect>().ToList();
            set => effects = value.Cast<ModifierEffect>().ToList();
        }

        
        public ETriPhase TriPhase
        {
            get => triPhase;
            set => triPhase = value;
        }

        public void AddValidator(IModifierValidator validator)
        {
            if (validator is ModifierValidator modifierValidator)
            {
                validators.Add(modifierValidator);
            }
        }
        
        public void AddValidator(ModifierValidator validator)
        {
            validators.Add(validator);
        }

        
        public IEnumerable<IModifierValidator> Validators
        {
            get => validators;
            private set
            {
                validators = new List<ModifierValidator>();
                foreach (var validator in value)
                {
                    if (validator is ModifierValidator modifierValidator)
                    {
                        validators.Add(modifierValidator);
                    }
                }
            }
        }

        public float GetModifierEffect(EMetric metric, float value)
        {
            return Effects.Aggregate(value, (current, conditionEffect) => conditionEffect.GetModifierEffect(metric, current));
        }

        public ETriPhase GetPhase()
        {
            return TriPhase;
        }

        public bool IsValid(Context context)
        {
            return Validators.All(validator => validator.IsValid(this, context));
        }

        
        public List<string> Tags
        {
            get => tags;
            set => tags = value;
        }
    }
}