﻿using System;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Map.Tiles
{
    public static class TileHelper
    {
        
        public static ETileType RandomTileType()
        {
            var tileTypes = Enum.GetValues(typeof(ETileType));
            var randomIndex = UnityEngine.Random.Range(0, tileTypes.Length);
            return (ETileType)tileTypes.GetValue(randomIndex);
        }
        
        public static ETileType GetTileBaseType(ETileType tileType)
        {
            return (ETileType)((int)tileType / 100);
        }
        
        public static Color GetTileIndexColor(ETileType tileType)
        {
            var baseColor = GetTileBaseColor(tileType);
            var tileIndex = (int)tileType % 100;
            // We use the index to get a variation of the base color, with 100 distinct colors per base color
            // We do this by varying the brightness, hue, and saturation of the base color, within a 60º cone
            // around the base color on the HSV color wheel.
            Color.RGBToHSV(baseColor, out var h, out var s, out var v);
            var hue = h + (tileIndex / 100f) * 0.6f;
            var saturation = s + (tileIndex / 100f) * 0.6f;
            var value = v + (tileIndex / 100f) * 0.6f;
            return Color.HSVToRGB(hue, saturation, value);
        }
        public static Color GetTileBaseColor(ETileType tileType)
        {
            return GetTileBaseType(tileType) switch
            {
                ETileType.None => Color.black,
                ETileType.Floor => Color.white,
                ETileType.Wall => Color.gray,
                ETileType.Pit => Color.black,
                ETileType.Screen => Color.green,
                _ => Color.magenta
            };
        }
        
        public static float GetTileMovementCost(ETileType tileType)
        {
            return TileHelper.GetTileBaseType(tileType) switch
            {
                ETileType.None => Mathf.Infinity,
                ETileType.Floor => 1,
                ETileType.Wall => Mathf.Infinity,
                ETileType.Pit => Mathf.Infinity,
                ETileType.Screen => 1,
                _ => Mathf.Infinity
            };
        }
        
        public static float GetTileMovementCost(int tileValue)
        {
            return GetTileMovementCost((ETileType)tileValue);
        }
        
        public static bool IsTileWalkable(ETileType tileType)
        {
            return TileHelper.GetTileBaseType(tileType) == 0;
        }
        
        public static bool IsTileWalkable(int tileValue)
        {
            return IsTileWalkable((ETileType)tileValue);
        }
    }
}