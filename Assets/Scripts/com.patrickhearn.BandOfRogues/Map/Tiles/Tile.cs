﻿using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Map.Tiles
{
    public class Tile : IEntity, ITile
    {
        [field: SerializeField]public string Name { get; private set; }
        public ETileType TileType
        {
            get
            {
                return (IsWalkable, IsOccluding) switch
                {
                    (true, false) => ETileType.Floor,
                    (false, true) => ETileType.Wall,
                    (false, false) => ETileType.Pit,
                    (true, true) => ETileType.Screen
                };
            }
        }
        
        [field: SerializeField]public float MovementCost { get; private set; }
        [field: SerializeField]public bool IsWalkable { get; private set; }
        [field: SerializeField]public bool IsOccluding { get; private set; }


        public Tile()
        {
            MovementCost = 1.0f;
            IsWalkable = false;
            IsOccluding = false;
        }
    }
}