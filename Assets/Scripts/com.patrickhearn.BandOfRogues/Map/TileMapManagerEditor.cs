﻿#if UNITY_EDITOR

using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Map.Tiles;
using UnityEditor;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Map
{
    [CustomEditor(typeof(TileMapManager))]
    public class TileMapManagerEditor : Editor
    {
        private static Dictionary<Color, Texture2D> _textureCache;

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            var gridManager = (TileMapManager) target;
            if (GUILayout.Button("Generate Grid"))
            {
                // gridManager.Initialize();
            }
        }
        
        
        private static void DrawGrid(int[,] gridArray)
        {
            // We show a coloured grid in the inspector, with the tile type as the colour
            var width = gridArray.GetLength(0);
            var height = gridArray.GetLength(1);
            for (var y = height - 1; y >= 0; y--)
            {
                EditorGUILayout.BeginHorizontal();
                for (var x = 0; x < width; x++)
                {
                    var tileType = (ETileType)gridArray[x, y];
                    var tileColor = TileHelper.GetTileIndexColor(tileType);
                    var style = new GUIStyle(GUI.skin.box) {normal = {background = MakeTex(2, 2, tileColor)}};
                    EditorGUILayout.LabelField("", style, GUILayout.Width(20), GUILayout.Height(20));
                }
                EditorGUILayout.EndHorizontal();
            }
        }
        
        private static Texture2D MakeTex(int width, int height, Color col)
        {
            if (_textureCache == null)
            {
                _textureCache = new Dictionary<Color, Texture2D>();
            }
            if (_textureCache.ContainsKey(col)) return _textureCache[col];
            var pix = new Color[width * height];
 
            for (var i = 0; i < pix.Length; i++)
                pix[i] = col;
 
            var result = new Texture2D(width, height);
            result.SetPixels(pix);
            result.Apply();
            _textureCache.Add(col, result);
 
            return result;
        }
    }
}
#endif