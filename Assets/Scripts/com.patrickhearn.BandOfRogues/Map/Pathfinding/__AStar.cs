﻿// using System;
// using System.Collections.Generic;
// using UnityEngine;
//
// namespace BandOfRogues.Map.Pathfinding
// {
//     public static class AStar
//     {
//         public static Vector2Int[] FindPath(Vector2Int start, Vector2Int end, int[,] walkable)
//         {
//             var openSet = new Heap<Node>(walkable.GetLength(0) * walkable.GetLength(1));
//             var closedSet = new HashSet<Node>();
//             var grid = new Node[walkable.GetLength(0), walkable.GetLength(1)];
//
//             for (var x = 0; x < walkable.GetLength(0); x++)
//             {
//                 for (var y = 0; y < walkable.GetLength(1); y++)
//                 {
//                     grid[x, y] = new Node(walkable[x, y]==0, new Vector2Int(x, y));
//                 }
//             }
//
//             var startNode = grid[start.x, start.y];
//             var endNode = grid[end.x, end.y];
//
//             openSet.Add(startNode);
//
//             while (openSet.Count > 0)
//             {
//                 var currentNode = openSet.RemoveFirst();
//                 closedSet.Add(currentNode);
//
//                 if (currentNode == endNode)
//                 {
//                     return RetracePath(startNode, endNode);
//                 }
//
//                 foreach (var neighbour in GetNeighbours(currentNode, grid))
//                 {
//                     if (!neighbour.walkable || closedSet.Contains(neighbour))
//                     {
//                         continue;
//                     }
//
//                     var newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);
//                     if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
//                     {
//                         neighbour.gCost = newMovementCostToNeighbour;
//                         neighbour.hCost = GetDistance(neighbour, endNode);
//                         neighbour.parent = currentNode;
//
//                         if (!openSet.Contains(neighbour))
//                         {
//                             openSet.Add(neighbour);
//                         }
//                         else
//                         {
//                             openSet.UpdateItem(neighbour);
//                         }
//                     }
//                 }
//             }
//
//             return Array.Empty<Vector2Int>();
//         }
//         
//         private static Vector2Int[] RetracePath(Node startNode, Node endNode)
//         {
//             var path = new List<Vector2Int>();
//             var currentNode = endNode;
//
//             while (currentNode != startNode)
//             {
//                 path.Add(currentNode.position);
//                 currentNode = currentNode.parent;
//             }
//
//             path.Reverse();
//             return path.ToArray();
//         }
//         
//         private static int GetDistance(Node nodeA, Node nodeB)
//         {
//             var dstX = Mathf.Abs(nodeA.position.x - nodeB.position.x);
//             var dstY = Mathf.Abs(nodeA.position.y - nodeB.position.y);
//
//             if (dstX > dstY)
//             {
//                 return 14 * dstY + 10 * (dstX - dstY);
//             }
//
//             return 14 * dstX + 10 * (dstY - dstX);
//         }
//         
//         private static List<Node> GetNeighbours(Node node, Node[,] grid)
//         {
//             var neighbours = new List<Node>();
//             var x = node.position.x;
//             var y = node.position.y;
//
//             if (x - 1 >= 0)
//             {
//                 neighbours.Add(grid[x - 1, y]);
//             }
//
//             if (x + 1 < grid.GetLength(0))
//             {
//                 neighbours.Add(grid[x + 1, y]);
//             }
//
//             if (y - 1 >= 0)
//             {
//                 neighbours.Add(grid[x, y - 1]);
//             }
//
//             if (y + 1 < grid.GetLength(1))
//             {
//                 neighbours.Add(grid[x, y + 1]);
//             }
//
//             if (x - 1 >= 0 && y - 1 >= 0)
//             {
//                 neighbours.Add(grid[x - 1, y - 1]);
//             }
//
//             if (x + 1 < grid.GetLength(0) && y - 1 >= 0)
//             {
//                 neighbours.Add(grid[x + 1, y - 1]);
//             }
//
//             if (x - 1 >= 0 && y + 1 < grid.GetLength(1))
//             {
//                 neighbours.Add(grid[x - 1, y + 1]);
//             }
//
//             if (x + 1 < grid.GetLength(0) && y + 1 < grid.GetLength(1))
//             {
//                 neighbours.Add(grid[x + 1, y + 1]);
//             }
//
//             return neighbours;
//         }
//         
//         
//     }
//     
//     public class Node : IHeapItem<Node>
//     {
//         public bool walkable;
//         public Vector2Int position;
//         public Node parent;
//         public int gCost;
//         public int hCost;
//         public int fCost => gCost + hCost;
//         public int HeapIndex { get; set; }
//
//         public Node(bool walkable, Vector2Int position)
//         {
//             this.walkable = walkable;
//             this.position = position;
//         }
//
//         public int CompareTo(Node other)
//         {
//             var compare = fCost.CompareTo(other.fCost);
//             if (compare == 0)
//             {
//                 compare = hCost.CompareTo(other.hCost);
//             }
//
//             return -compare;
//         }
//     }
//     
//     public interface IHeapItem<T> : IComparable<T>
//     {
//         int HeapIndex { get; set; }
//     }
//     
//     public class Heap<T> where T : IHeapItem<T>
//     {
//         private readonly T[] _items;
//         private int _currentItemCount;
//
//         public Heap(int maxHeapSize)
//         {
//             _items = new T[maxHeapSize];
//         }
//
//         public void Add(T item)
//         {
//             item.HeapIndex = _currentItemCount;
//             _items[_currentItemCount] = item;
//             SortUp(item);
//             _currentItemCount++;
//         }
//
//         public T RemoveFirst()
//         {
//             var firstItem = _items[0];
//             _currentItemCount--;
//             _items[0] = _items[_currentItemCount];
//             _items[0].HeapIndex = 0;
//             SortDown(_items[0]);
//             return firstItem;
//         }
//
//         public void UpdateItem(T item)
//         {
//             SortUp(item);
//         }
//
//         public int Count => _currentItemCount;
//
//         public bool Contains(T item)
//         {
//             return Equals(_items[item.HeapIndex], item);
//         }
//
//         private void SortDown(T item)
//         {
//             while (true)
//             {
//                 var childIndexLeft = item.HeapIndex * 2 + 1;
//                 var childIndexRight = item.HeapIndex * 2 + 2;
//                 var swapIndex = 0;
//
//                 if (childIndexLeft < _currentItemCount)
//                 {
//                     swapIndex = childIndexLeft;
//
//                     if (childIndexRight < _currentItemCount)
//                     {
//                         if (_items[childIndexLeft].CompareTo(_items[childIndexRight]) < 0)
//                         {
//                             swapIndex = childIndexRight;
//                         }
//                     }
//
//                     if (item.CompareTo(_items[swapIndex]) < 0)
//                     {
//                         Swap(item, _items[swapIndex]);
//                     }
//                     else
//                     {
//                         return;
//                     }
//                 }
//                 else
//                 {
//                     return;
//                 }
//             }
//         }
//
//         private void SortUp(T item)
//         {
//             var parentIndex = (item.HeapIndex - 1) / 2;
//
//             while (true)
//             {
//                 var parentItem = _items[parentIndex];
//
//                 if (item.CompareTo(parentItem) > 0)
//                 {
//                     Swap(item, parentItem);
//                 }
//                 else
//                 {
//                     break;
//                 }
//
//                 parentIndex = (item.HeapIndex - 1) / 2;
//             }
//         }
//
//         private void Swap(T itemA, T itemB)
//         {
//             _items
//                 [itemA.HeapIndex] = itemB;
//             _items[itemB.HeapIndex] = itemA;
//             (itemA.HeapIndex, itemB.HeapIndex) = (itemB.HeapIndex, itemA.HeapIndex);
//         }
//     }
// }