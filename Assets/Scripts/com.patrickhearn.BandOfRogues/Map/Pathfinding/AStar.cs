using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Map.Pathfinding
{
    
    public class Node
    {
        // Change this depending on what the desired size is for each element in the grid
        public static int NodeSize = 1;
        public Node Parent;
        public Vector2Int Position;
        public float DistanceToTarget;
        public float Cost;
        public float Weight;
        public float F => DistanceToTarget != -1 && Cost != -1 ? DistanceToTarget + Cost : -1;
        public bool Walkable;

        public Node(Vector2Int pos, bool walkable, float weight = 1)
        {
            Parent = null;
            Position = pos;
            DistanceToTarget = -1;
            Cost = 1;
            Weight = weight;
            Walkable = walkable;
        }
        // TODO @ ASTART is all broken, find an implemnteation
         // Equality operator so we can compare nodes
        public static bool operator ==(Node a, Node b)
        {
            if (ReferenceEquals(a, b))
            {
                return true;
            }
            if (a is null || b is null)
            {
                return false;
            }
            return a.Position == b.Position;
        }

        public static bool operator !=(Node a, Node b) => !(a == b);
        
        public override bool Equals(object obj)
        {
            if (obj is Node node)
            {
                return Position == node.Position;
            }
            return false;
        }
        
        public override int GetHashCode()
        {
            return Position.GetHashCode();
        }
        
        public override string ToString()
        {
            return Position.ToString();
        }
        
        
    }

    public class AStar
    {
        [NonSerialized] 
        public List<List<Node>> Grid;

        private List<Vector2Int> _validMoves = new()
        {
            new Vector2Int(1,0),
            new Vector2Int(-1,0),
            new Vector2Int(0,1),
            new Vector2Int(0,-1),
        };
        protected int GridRows => Grid[0].Count;
        protected int GridCols => Grid.Count;
        public AStar(List<List<Node>> grid)
        {
            Grid = grid;
        }
        
        


        public Vector2Int[] FindPath(Vector2Int start, Vector2Int end)
        {
            var stack = FindPath(new Vector2(start.x, start.y), new Vector2(end.x, end.y));
            if (stack == null) return null;
            var path = new Vector2Int[stack.Count];
            var i = 0;
            while (stack.Count != 0)
            {
                var node = stack.Pop();
                path[i] = node.Position;
                i++;
            }
            
            return path;
        }
        
        public virtual Stack<Node> FindPath(Vector2 start, Vector2 end)
        {
            Debug.Log($"Finding path from {start} to {end}");
            var startNode = new Node(new Vector2Int((int)start.x, (int)start.y), true);
            var endNode = new Node(new Vector2Int((int)end.x, (int)end.y), true);

            var path = new Stack<Node>();
            var openList = new PriorityQueue<Node,float>();
            var closedList = new List<Node>();
            List<Node> adjacencies;
            var current = startNode;
           
            // add start node to Open List
            openList.Enqueue(startNode, startNode.F);
            
            while(openList.Count != 0 && !closedList.Exists(x => x.Position == endNode.Position))
            {
                Debug.Log($"Open list: {openList.Count}");
                current = openList.Dequeue();
                closedList.Add(current);
                adjacencies = GetNeighbours(current, _validMoves);
                foreach(var n in adjacencies)
                {
                    // Debug.Log($"Neighbour: {n.Position}");
                    if (closedList.Contains(n) || !n.Walkable) continue;
                    var isFound = false;
                    foreach (var _ in openList.UnorderedItems.Where(oLNode => oLNode.Key == n))
                    {
                        isFound = true;
                    }

                    if (isFound) continue;
                    n.Parent = current;
                    n.DistanceToTarget = Math.Abs(n.Position.x - endNode.Position.x) + Math.Abs(n.Position.y - endNode.Position.y);
                    n.Cost = n.Weight + n.Parent.Cost;
                    openList.Enqueue(n, n.F);
                }
            }
            
            // construct path, if end was not closed return null
            if(!closedList.Exists(x => x.Position == endNode.Position))
            {
                Debug.Log("End not found");
                return null;
            }

            // if all good, return path
            var temp = closedList[closedList.IndexOf(current)];
            if (temp == null) return null;
            do
            {
                path.Push(temp);
                temp = temp.Parent;
            } while (temp != startNode && temp != null) ;
            return path;
        }

        public virtual List<Node> GetNeighbours(Node n, List<Vector2Int> validMoves)
        {
            var neighbours = new List<Node>();

            var row = (int)n.Position.y;
            var col = (int)n.Position.x;
            if (col >= Grid.Count || col < 0)
            {
                // Debug.Log($"Node {n} col {col} outside grid range {Grid.Count}");
                return neighbours;
            }
            

            // Debug.Log($"{col}, {Grid.Count}");
            if (row >= Grid[col].Count || row < 0)
            {
                // Debug.Log($"Node {n} row {row} outside grid range {Grid[col].Count}");
                return neighbours;
            }
                
            if(row + 1 < GridRows)
            {
                neighbours.Add(Grid[col][row + 1]);
            }
            if(row - 1 >= 0)
            {
                neighbours.Add(Grid[col][row - 1]);
            }
            if(col - 1 >= 0)
            {
                neighbours.Add(Grid[col - 1][row]);
            }
            if(col + 1 < GridCols)
            {
                neighbours.Add(Grid[col + 1][row]);
            }

            return neighbours;
        }
        
    }
    
    public class PriorityQueue<T, TPriority> where TPriority : IComparable
    {
        private readonly List<KeyValuePair<T, TPriority>> _elements = new();

        public int Count => _elements.Count;

        public void Enqueue(T item, TPriority priority)
        {
            _elements.Add(new KeyValuePair<T, TPriority>(item, priority));
        }

        // Returns the Location that has the lowest priority
        public T Dequeue()
        {
            var bestIndex = 0;

            for (var i = 0; i < _elements.Count; i++)
            {
                if (_elements[i].Value.CompareTo(_elements[bestIndex].Value) < 0)
                {
                    bestIndex = i;
                }
            }

            var bestItem = _elements[bestIndex].Key;
            _elements.RemoveAt(bestIndex);
            return bestItem;
        }

        public List<KeyValuePair<T, TPriority>> UnorderedItems => _elements;
    }
}