﻿using System.Collections.Generic;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Map.Pathfinding
{
    public class AStarPathfinder : BasePathfinder<Vector2Int>
    {
        public override List<Vector2Int> FindPath(Vector2Int start, Vector2Int goal)
        {
            Dictionary<Vector2Int, Vector2Int> cameFrom = new Dictionary<Vector2Int, Vector2Int>();
            Dictionary<Vector2Int, float> costSoFar = new Dictionary<Vector2Int, float>();
        
            PriorityQueue<Vector2Int, float> frontier = new PriorityQueue<Vector2Int, float>();
            frontier.Enqueue(start, 0f);
        
            cameFrom[start] = start;
            costSoFar[start] = 0;

            while (frontier.Count > 0)
            {
                var current = frontier.Dequeue();

                if (current.Equals(goal))
                {
                    break;
                }

                foreach (var neighbour in PathFinding.GetNeighbours(current))
                {
                    var newCost = costSoFar[current] + PathFinding.GetCost(neighbour);
                    if (costSoFar.ContainsKey(neighbour) && !(newCost < costSoFar[neighbour])) continue;
                    costSoFar[neighbour] = newCost;
                    var priority = newCost + Heuristic(goal, neighbour);
                    frontier.Enqueue(neighbour, priority);
                    cameFrom[neighbour] = current;
                }
            }

            return ReconstructPath(cameFrom, goal);
        }

        private List<Vector2Int> ReconstructPath(Dictionary<Vector2Int, Vector2Int> cameFrom, Vector2Int goal)
        {
            var path = new List<Vector2Int>();
            var current = goal;
            while (current != cameFrom[current])
            {
                path.Add(current);
                current = cameFrom[current];
            }
            path.Add(current);
            path.Reverse();
            return path;
        }

        private float Heuristic(Vector2Int a, Vector2Int b)
        {
            if (AllowDiagonal)
            {
                // Euclidean distance
                var dx = a.x - b.x;
                var dy = a.y - b.y;
                return Sqrt2 * Mathf.Min(dx, dy) + Mathf.Abs(dx - dy);
            }
            else
            {
                // Manhattan distance
                return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
            }
        }

    }

}