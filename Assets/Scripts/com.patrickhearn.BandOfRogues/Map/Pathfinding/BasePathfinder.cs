﻿using System;
using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Map.Pathfinding
{
    [Serializable]
    public abstract class BasePathfinder<T> : IPathfinder<T>
    {
        protected const float Sqrt2 = 1.4142136f;  // sqrt(2)

        private bool[,] _walkableGrid;
        private float[,] _costGrid;
        

        public void SetWalkableGrid(bool[,] grid)
        {
            _walkableGrid = grid;
            UpdatePathFinding();
        }

        public void SetCostGrid(float[,] grid)
        {
            _costGrid = grid;
            UpdatePathFinding();
        }

        protected IPathFinding PathFinding;
        
        private void UpdatePathFinding()
        {
            PathFinding = new PathfindingTilemap(_walkableGrid, _costGrid);
        }
        

        [field: SerializeField]public bool AllowDiagonal { get; set; }


        public abstract List<T> FindPath(T start, T goal);
    }

}