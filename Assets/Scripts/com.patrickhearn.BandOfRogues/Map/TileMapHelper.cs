﻿using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Map.Tiles;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Map
{
    public static class TileMapHelper
    {
        public const float Sqrt2 = 1.41421356237f;
        public static Vector2Int[] Neighbours(Vector2Int position, int[,] grid)
        {
            var neighbours = new List<Vector2Int>();
            var x = position.x;
            var y = position.y;
            if (x > 0 && grid[x - 1, y] == 0)
            {
                neighbours.Add(new Vector2Int(x - 1, y));
            }
            if (x < grid.GetLength(0) - 1 && grid[x + 1, y] == 0)
            {
                neighbours.Add(new Vector2Int(x + 1, y));
            }
            if (y > 0 && grid[x, y - 1] == 0)
            {
                neighbours.Add(new Vector2Int(x, y - 1));
            }
            if (y < grid.GetLength(1) - 1 && grid[x, y + 1] == 0)
            {
                neighbours.Add(new Vector2Int(x, y + 1));
            }
            return neighbours.ToArray();
        }

        public static Vector2Int[] GetCellsInDistanceRange(Vector2Int target, int minDistance, int maxDistance, int[,] gridArray)
        {
            var cellsInRange = new List<Vector2Int>();
            for (var x = -maxDistance; x <= maxDistance; x++)
            {
                for (var y = -maxDistance; y <= maxDistance; y++)
                {
                    var distance = Mathf.Abs(x) + Mathf.Abs(y);
                    if (distance < minDistance || distance > maxDistance) continue;
                    var cell = new Vector2Int(target.x + x, target.y + y);
                    if (cell.x >= 0 && cell.x < gridArray.GetLength(0) && cell.y >= 0 && cell.y < gridArray.GetLength(1))
                    {
                        cellsInRange.Add(cell);
                    }
                }
            }
            return cellsInRange.ToArray();
        }

        public static int[,] MapTilesTypes(int[,] map, List<ETileType> tileTypes)
        {
            for (var x = 0; x < map.GetLength(0); x++)
            {
                for (var y = 0; y < map.GetLength(1); y++)
                {
                    var tileTypeIndex = map[x, y];
                    if (tileTypeIndex < 0 || tileTypeIndex >= tileTypes.Count)
                    {
                        map[x, y] = 0;
                    }
                    else
                    {
                        map[x, y] = (int) tileTypes[tileTypeIndex];
                    }
                }
            }
            return map;
        }
    }
}