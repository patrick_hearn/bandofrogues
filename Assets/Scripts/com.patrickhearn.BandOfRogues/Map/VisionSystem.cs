﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.patrickhearn.BandOfRogues.Interfaces;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Map
{
    public class VisionSystem : IVisionSystem
    {
        private ITileMap _tileMap;

        public VisionSystem(ITileMap tileMap)
        {
            this._tileMap = tileMap;
        }

        public List<Vector2Int> GetVisibleTiles(Vector2Int position, int maxDistance)
        {
            var visibleTiles = new List<Vector2Int> { position };
            var checkedTiles = new HashSet<Vector2Int> { position };

            for (var d = 1; d <= maxDistance; d++)
            {
                var ring = GetRing(position, d);
                foreach (var tile in ring)
                {
                    if (!_tileMap.IsInBounds(tile))
                        continue;
                    var neighbours = _tileMap.GetNeighbours(tile);
                    if (neighbours.Any(neighbour => checkedTiles.Contains(neighbour) && visibleTiles.Contains(neighbour)))
                    {
                        visibleTiles.Add(tile);
                    }
                }
                checkedTiles.UnionWith(ring);
            }

            return visibleTiles;
        }

        private List<Vector2Int> GetRing(Vector2Int position, int radius)
        {
            var ring = new List<Vector2Int>();
            for (var x = position.x - radius; x <= position.x + radius; x++)
            {
                ring.Add(new Vector2Int(x, position.y - radius));
                ring.Add(new Vector2Int(x, position.y + radius));
            }
            for (var y = position.y - radius + 1; y <= position.y + radius - 1; y++)
            {
                ring.Add(new Vector2Int(position.x - radius, y));
                ring.Add(new Vector2Int(position.x + radius, y));
            }
            return ring;
        }


        public bool IsTileVisibleFrom(Vector2Int fromPosition, Vector2Int toPosition, int? maxDistance = null)
        {
            if (maxDistance.HasValue && Vector2Int.Distance(fromPosition, toPosition) > maxDistance.Value)
            {
                return false;
            }

            var line = GetLine(fromPosition, toPosition);
            // Remove the first and last tile from the line, as they are the start and end positions
            line = line.Skip(1).Take(line.Length - 2).ToArray();
            // Remove any tiles that are out of bounds
            line = line.Where(_tileMap.IsInBounds).ToArray();
            // If any tile in the line is occluding, the tile is not visible
            return !line.Any(tile => _tileMap.IsOccluding(tile));
        }
        
        public static Vector2Int[] GetLine(Vector2Int fromPosition, Vector2Int toPosition)
        {
            var x0 = fromPosition.x;
            var y0 = fromPosition.y;
            var x1 = toPosition.x;
            var y1 = toPosition.y;
            
            var dx = Mathf.Abs(x1 - x0);
            var dy = Mathf.Abs(y1 - y0);
            var sx = x0 < x1 ? 1 : -1;
            var sy = y0 < y1 ? 1 : -1;
            var err = dx - dy;
            
            var points = new List<Vector2Int>();
            
            while (true)
            {
                points.Add(new Vector2Int(x0, y0));
                if (x0 == x1 && y0 == y1) break;
                var e2 = 2 * err;
                if (e2 > -dy)
                {
                    err -= dy;
                    x0 += sx;
                }
                if (e2 < dx)
                {
                    err += dx;
                    y0 += sy;
                }
            }

            return points.ToArray();
        }
    }

}