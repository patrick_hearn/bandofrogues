using System;
using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;
using com.patrickhearn.BandOfRogues.Map.Tiles;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Map
{
    [Serializable]
    public struct TileDefinition
    {
        public ETileType tileType;
        public GameObject prefab;
        public int TileValue => (int)tileType;
    }

    public class PathfindingTilemap : IPathFinding, IGrid<int>
    {
        private float[,] _costGrid = new float[0,0];
        private bool[,] _walkableGrid = new bool[0,0];
        
        public PathfindingTilemap(bool[,] walkableGrid, float[,] costGrid)
        {
            _walkableGrid = walkableGrid;
            _costGrid = costGrid;
        }


        public List<Vector2Int> GetNeighbours(Vector2Int position, bool includeDiagonal = false)
        {
            throw new NotImplementedException();
        }

        public bool IsWalkable(Vector2Int position)
        {
            if (position.x < 0 || position.x >= _walkableGrid.GetLength(0) || position.y < 0 || position.y >= _walkableGrid.GetLength(1))
            {
                return false;
            }
            return _walkableGrid[position.x, position.y];
        }

        public float GetCost(Vector2Int position)
        {
            if(!IsInBounds(position)) return -1;
            return _costGrid[position.x, position.y];
        }

        public int[,] GetWalkableGrid()
        {
            var grid = new int[_walkableGrid.GetLength(0), _walkableGrid.GetLength(1)];
            for (var x = 0; x < _walkableGrid.GetLength(0); x++)
            {
                for (var y = 0; y < _walkableGrid.GetLength(1); y++)
                {
                    grid[x, y] = _walkableGrid[x, y] ? 1 : 0;
                }
            }
            
            return grid;
        }

        public float[,] GetCostGrid() => _costGrid;

        public int GetItem(Vector2Int position)
        {
            if(!IsInBounds(position)) return -1;
            return _walkableGrid[position.x, position.y] ? 1 : 0;
        }

        public void SetItem(Vector2Int position, int item)
        {
            if (!IsInBounds(position)) return;
            _walkableGrid[position.x, position.y] = item == 1;
        }

        public bool IsInBounds(Vector2Int position)
        {
            return position.x >= 0 && position.x < _walkableGrid.GetLength(0) && position.y >= 0 && position.y < _walkableGrid.GetLength(1);
        }

        public Vector2Int Size => new Vector2Int(_walkableGrid.GetLength(0), _walkableGrid.GetLength(1));
    }
    
    [Serializable]
    public class TileMap : ITileMap
    {
        private int[,] _tiles;
        public int[,] Tiles => _tiles;
        private Dictionary<int, Tile> _tileDictionary;
        private Dictionary<Tile, int> _tileDictionaryReverse;
        public List<Tile> TileData;
        public int Width => Size.x;
        public int Height => Size.y;

        #region Constructors
        public TileMap(int width=10, int height=10, int[,] tiles=null, List<Tile> tileData=null)
        {
            _tiles = tiles ?? new int[width, height];
            TileData = tileData ?? new List<Tile>();
            PopulateTileDictionary();
        }
        
        
        
        #endregion
        
        private void PopulateTileDictionary()
        {
            _tileDictionary = new Dictionary<int, Tile>();
            _tileDictionaryReverse = new Dictionary<Tile, int>();
            foreach (var tile in TileData)
            {
                _tileDictionary.Add(TileData.IndexOf(tile), tile);
                _tileDictionaryReverse.Add(tile, TileData.IndexOf(tile));
            }
        }
        
        
        #region IGrid Implementation

        public Vector2Int Size => new Vector2Int(_tiles.GetLength(0), _tiles.GetLength(1));

        public ITile GetItem(Vector2Int position)
        {
            if (!IsInBounds(position)) return null;
            if (!_tileDictionary.ContainsKey(_tiles[position.x, position.y]))
            {
                Debug.LogError($"TileMap.GetItem: Tile not found at position {position}");
                return null;
            }
            
            return _tileDictionary[_tiles[position.x, position.y]];
        }
        
        public Tile GetTile(Vector2Int position)
        {
            return GetItem(position) as Tile;
        }

        public void SetItem(Vector2Int position, ITile item)
        {
            if (!IsInBounds(position)) return;
            if (item is not Tile tile) return;
            if (!_tileDictionaryReverse.ContainsKey(tile))
            {
                Debug.LogError($"TileMap.SetItem: Tile not found at position {position}");
                return;
            }
            _tiles[position.x, position.y] = _tileDictionaryReverse[tile];
        }
        

        public bool IsInBounds(Vector2Int position)
        {
            return position.x >= 0 && position.y >= 0 && position.x < Size.x && position.y < Size.y;
        }
        #endregion

        
        #region ITileMap Implementation
        public List<Vector2Int> GetNeighbours(Vector2Int position, bool includeDiagonal = false)
        {
            var neighbours = new List<Vector2Int>();
            // Iterate over all nearby positions.
            for (var dx = -1; dx <= 1; dx++)
            {
                for (var dy = -1; dy <= 1; dy++)
                {
                    // Skip the current position.
                    if (dx == 0 && dy == 0) continue;

                    // Skip diagonal neighbours if not included.
                    if (!includeDiagonal && dx != 0 && dy != 0) continue;

                    var neighbourPosition = new Vector2Int(position.x + dx, position.y + dy);
                    // Add the neighbour if it's in bounds.
                    if (IsInBounds(neighbourPosition))
                    {
                        neighbours.Add(neighbourPosition);
                    }
                }
            }
            return neighbours;
        }

        public bool IsWalkable(Vector2Int position)
        {
            return GetItem(position).IsWalkable;
        }

        public float GetCost(Vector2Int position)
        {
            return GetItem(position).MovementCost;
        }


        public int[,] GetWalkableGrid()
        {
            var width = Width;
            var height = Height;
            var walkableGrid = new int[width, height];
        
            for(var x=0; x<width; x++)
            {
                for(var y=0; y<height; y++)
                {
                    walkableGrid[x, y] = IsWalkable(new Vector2Int(x, y)) ? 1 : 0;
                }
            }
        
            return walkableGrid;
        }

        public float[,] GetCostGrid()
        {
            var costGrid = new float[Size.x, Size.y];
            for (var x = 0; x < Size.x; x++)
            {
                for (var y = 0; y < Size.y; y++)
                {
                    costGrid[x, y] = GetCost(new Vector2Int(x, y));
                }
            }
            return costGrid;
        }

        #endregion
        public TileMap(int width, int height, List<Tile> tileData)
        {
            this._tiles = new int[width, height];
            this.TileData = tileData;
        }

        // ITileMap Implementation

        #region ITileMap Implementation
        public int[,] GetOcclusionGrid()
        {
            var occlusionGrid = new int[Width, Height];
            for (var x = 0; x < Width; x++)
            {
                for(var y = 0; y < Height; y++)
                {
                    occlusionGrid[x, y] = IsOccluding(new Vector2Int(x, y)) ? 1 : 0;
                }
            }
            return occlusionGrid;
        }

        public bool IsOccluding(Vector2Int position)
        {
            return GetItem(position).IsOccluding;
        }
        #endregion

        
        
        public int GetTileIndex(int x, int y)
        {
            if (x < 0 || x >= Width || y < 0 || y >= Height)
            {
                return -1;
            }
            return Tiles[x, y];
        }

        //
        // public Vector2Int GetClosestTile(Vector2Int target, Vector2Int origin, bool[,] mask=null)
        // {
        //     // Get the closest walkable tile to the target which is not the target
        //     // Returns that which is closest to orign
        //     Debug.Log($"GetClosestTile: {target}, {origin}");
        //     var closest = new Vector2Int(-1, -1);
        //     var closestDistance = float.MaxValue;
        //     var neighbours = Neighbours(target);
        //     foreach (var neighbour in neighbours)
        //     {
        //         if (neighbour == target) continue;
        //         if (!IsWalkable(neighbour)) continue;
        //         if (mask is not null && mask[neighbour.x, neighbour.y]) continue;
        //         var distance = Vector2Int.Distance(neighbour, origin);
        //         if (!(distance < closestDistance)) continue;
        //         
        //         Debug.Log($"New Closest: {neighbour}");
        //         closest = neighbour;
        //         closestDistance = distance;
        //     }
        //     
        //     return closest;
        // }
        //
        //
        // public bool IsVisibleFrom(Vector2Int start, Vector2Int end)
        // {
        //     var line = Bresenham.GetLine(start, end, GridArray);
        //     return line.All(point => !IsOccluding(point));
        // }
        //
        //
        public static float Distance(Vector2Int start, Vector2Int end)
        {
            return Vector2Int.Distance(start, end);
        }
        
        public static int DistanceManhattan(Vector2Int start, Vector2Int end)
        {
            return Mathf.Abs(start.x - end.x) + Mathf.Abs(start.y - end.y);
        }
        
        // public Vector2Int[] Neighbours(Vector2Int position)
        // {
        //     return TileMapHelper.Neighbours(position, GridArray);
        // }
        //
        // public Vector2Int[] WalkableNeighbours(Vector2Int position)
        // {
        //     var neighbours = Neighbours(position);
        //     return neighbours.Where(IsWalkable).ToArray();
        // }
        //
        // public Vector2Int[] GetCellsInDistanceRange(Vector2Int target, int minDistance, int maxDistance)
        // {
        //     return TileMapHelper.GetCellsInDistanceRange(target, minDistance, maxDistance, GridArray);
        // }
        //
        public void SetTile(int i, int i1, Tile tile)
        {
            Tiles[i, i1] = TileData.IndexOf(tile);
        }
    }
}
