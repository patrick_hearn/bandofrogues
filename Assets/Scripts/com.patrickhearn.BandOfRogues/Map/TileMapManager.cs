﻿using System.IO;
using com.patrickhearn.BandOfRogues.Interfaces;
using Unity.Plastic.Newtonsoft.Json;

namespace com.patrickhearn.BandOfRogues.Map
{
    public class TileMapManager : SingletonBehaviour<TileMapManager>
    {
        public TileMap TileMap { get; private set; }

        public void CreateTileMap(int width, int height)
        {
            TileMap = new TileMap(width, height);
        }
        
        public void SaveTileMap(string filename)
        {
            string json = JsonConvert.SerializeObject(TileMap);
            File.WriteAllText(filename, json);
        }

        public void LoadTileMap(string filename)
        {
            string json = File.ReadAllText(filename);
            TileMap = JsonConvert.DeserializeObject<TileMap>(json);
        }
    }
    
    
    //     
    //     [SerializeField]
    //     private TileMap _tileMap = new(10, 10);
    //     public static TileMapManager Instance;
    //     public List<TileDefinition> TileDefinitions;
    //     private Dictionary<int, GameObject> _tileDictionary;
    //     
    //     public GameObject[,] GridObjectArray;
    //     
    //     public RoomGenerator roomGenerator;
    //     
    //     
    //
    //     public static TileMap TileMap => !Instance ? null : Instance._tileMap;
    //
    //     public int[,] GridArray { get; private set; }
    //     public int Width => _tileMap.Width;
    //     public int Height => _tileMap.Height;
    //
    //     public void Awake()
    //     {
    //         Instance = this;
    //     }
    //
    //
    //     
    //     public void Initialize()
    //     {
    //         PopulateTileDictionary();
    //         _tileMap = new TileMap(Width, Height);
    //         
    //         roomGenerator ??= ScriptableObject.CreateInstance<RoomGenerator>();
    //         var map = roomGenerator.GenerateMap(Width, Height);
    //         map = TileMapHelper.MapTilesTypes(map, new List<ETileType>() { ETileType.Wall ,ETileType.Floor});
    //         _tileMap.SetTiles(map);
    //         
    //         InstantiateGrid();
    //
    //     }
    //     
    //     private void PopulateTileDictionary()
    //     {
    //         _tileDictionary = new Dictionary<int, GameObject>();
    //         foreach (var tileDefinition in TileDefinitions)
    //         {
    //             _tileDictionary.Add(tileDefinition.TileValue, tileDefinition.prefab);
    //         }
    //     }
    //
    //     public void InstantiateGrid()
    //     {
    //         // TileMap.InstantiateGrid(tileType);
    //         GridObjectArray = new GameObject[Width, Height];
    //         
    //         for (var x = 0; x < Width; x++)
    //         {
    //             for (var y = 0; y < Height ; y++)
    //             {
    //                 var prefab = GetTilePrefab(_tileMap.GetTileIndex(x, y));
    //                 GridObjectArray[x, y] = Instantiate(prefab, new Vector3(x, y, 0), Quaternion.identity);
    //                 GridObjectArray[x, y].transform.parent = transform;
    //             }
    //         }
    //     }
    //     
    //     
    //     private GameObject GetTilePrefab(int tileValue)
    //     {
    //         if (_tileDictionary == null)
    //         {
    //             PopulateTileDictionary();
    //         }
    //
    //         if (_tileDictionary != null && _tileDictionary.ContainsKey(tileValue)) return _tileDictionary[tileValue];
    //         Debug.LogError("Tile value " + tileValue + " not found in dictionary");
    //         return null;
    //     }
    //     
    //      
    //
    // }
}