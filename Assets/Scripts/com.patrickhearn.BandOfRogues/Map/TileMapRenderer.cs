﻿using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Map.Tiles;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Map
{
    public class TileMapRenderer : MonoBehaviour
    {
        [FormerlySerializedAs("TileMap")] public TileMap tileMap;
        [FormerlySerializedAs("renderer")] public MeshRenderer meshRenderer;
        [FormerlySerializedAs("TileSize")] public Vector2Int tileSize = new Vector2Int(1, 1);
        [FormerlySerializedAs("DefaultColor")] public Color defaultColor = Color.white;
        [FormerlySerializedAs("NonWalkableColor")] public Color nonWalkableColor = Color.red;
        [FormerlySerializedAs("OccludingColor")] public Color occludingColor = Color.black;
        private Dictionary<string, Texture2D> _tileTextures = new Dictionary<string, Texture2D>();
        [FormerlySerializedAs("TileResolution")] public int tileResolution = 16;


        private void Start()
        {
            DrawTileMap();
        }

        private void DrawTileMap()
        {
            // Generate texture
            Texture2D texture = new Texture2D(tileMap.Width * tileResolution, tileMap.Height * tileResolution);
            texture.filterMode = FilterMode.Point;

            for (int x = 0; x < tileMap.Width; x++)
            {
                for (int y = 0; y < tileMap.Height; y++)
                {
                    var tile = tileMap.GetTile(new Vector2Int(x, y));
                    var tileName = tile.Name;

                    // Get or generate texture for tile
                    if (!_tileTextures.ContainsKey(tileName))
                    {
                        // Generate a color based on tile name
                        _tileTextures[tileName] = GenerateColorTexture(tileName);
                    }

                    // Draw the tile texture
                    texture.SetPixels(x * tileResolution, y * tileResolution, tileResolution, tileResolution, _tileTextures[tileName].GetPixels());
                }
            }

            texture.Apply();

            // Apply the texture
            if (meshRenderer == null)
            {
                meshRenderer = GetComponent<MeshRenderer>();
            }
            meshRenderer.material.mainTexture = texture;

            // Scale the plane to match the tile map size
            transform.localScale = new Vector3(tileMap.Width * tileSize.x / 10.0f, 1, tileMap.Height * tileSize.y / 10.0f);
        }

        private Texture2D GenerateColorTexture(string tileName)
        {
            Texture2D texture = new Texture2D(tileResolution, tileResolution);
            Color color = new Color(tileName.GetHashCode(), tileName.GetHashCode(), tileName.GetHashCode());
            for (int x = 0; x < tileResolution; x++)
            {
                for (int y = 0; y < tileResolution; y++)
                {
                    texture.SetPixel(x, y, color);
                }
            }
            texture.Apply();
            return texture;
        }

        private Texture2D ScaleTexture(Texture2D source, int targetWidth, int targetHeight)
        {
            Texture2D result = new Texture2D(targetWidth, targetHeight, source.format, true);
            Color[] rpixels = result.GetPixels(0);
            float incX = ((float)1 / source.width) * ((float)source.width / targetWidth);
            float incY = ((float)1 / source.height) * ((float)source.height / targetHeight);
            for (int px = 0; px < rpixels.Length; px++)
            {
                rpixels[px] = source.GetPixelBilinear(incX * ((float)px % targetWidth),
                   incY * ((float)Mathf.Floor(px / (float)targetWidth)));
            }
            result.SetPixels(rpixels, 0);
            result.Apply();
            return result;
        }

    }
}