﻿using System.Collections.Generic;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Map
{
    public static class LineOfSight
    {
        public static float GetVisibility(int x0, int y0, int x1, int y1, int[,] map)
        {
            var dx = x1 - x0;
            var dy = y1 - y0;
            var adx = Mathf.Abs(dx);
            var ady = Mathf.Abs(dy);
            var sx = dx > 0 ? 1 : -1;
            var sy = dy > 0 ? 1 : -1;
            var err = adx - ady;
            var x = x0;
            var y = y0;
            var visibility = 1f;
            while (true)
            {
                if (x == x1 && y == y1) break;
                var e2 = 2 * err;
                if (e2 > -ady)
                {
                    err -= ady;
                    x += sx;
                }

                if (e2 < adx)
                {
                    err += adx;
                    y += sy;
                }

                visibility *= map[x, y];
            }

            return visibility;
        }
        
        public static float GetVisibility(Vector2Int start, Vector2Int end, int[,] map)
        {
            return GetVisibility(start.x, start.y, end.x, end.y, map);
        }
        
        public static bool IsVisible(int x0, int y0, int x1, int y1, int[,] map, float threshold = 0.5f)
        {
            return GetVisibility(x0, y0, x1, y1, map) >= threshold;
        }
        
        public static bool IsVisible(Vector2Int start, Vector2Int end, int[,] map, float threshold = 0.5f)
        {
            return IsVisible(start.x, start.y, end.x, end.y, map, threshold);
        }
    }

    public static class Bresenham
    {
        public static List<Vector2Int> GetLine(Vector2Int start, Vector2Int end, int[,] map)
        {
            return GetLine(start.x, start.y, end.x, end.y, map);
        }
        
        public static List<Vector2Int> GetLine(int x0, int y0, int x1, int y1, int[,] map)
        {
            var dx = x1 - x0;
            var dy = y1 - y0;
            var adx = Mathf.Abs(dx);
            var ady = Mathf.Abs(dy);
            var sx = dx > 0 ? 1 : -1;
            var sy = dy > 0 ? 1 : -1;
            var err = adx - ady;
            var x = x0;
            var y = y0;
            var line = new List<Vector2Int>();
            while (true)
            {
                line.Add(new Vector2Int(x, y));
                if (x == x1 && y == y1) break;
                var e2 = 2 * err;
                if (e2 > -ady)
                {
                    err -= ady;
                    x += sx;
                }

                if (e2 >= adx) continue;
                err += adx;
                y += sy;
            }

            return line;
        }
    }
}