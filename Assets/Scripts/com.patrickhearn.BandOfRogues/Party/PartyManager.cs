using System;
using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Inventory;
using UnityEngine;
using UnityEngine.Serialization;

namespace com.patrickhearn.BandOfRogues.Party
{
    [Serializable]
    public struct PartyMember
    {
        public Character.Character Character;
        public List<InventoryItem> Inventory;
    }
    
    [Serializable]
    public class Party
    {
        [FormerlySerializedAs("Characters")] public List<PartyMember> characters;
    }
    public class PartyManager : MonoBehaviour
    {
        [FormerlySerializedAs("Party")] public Party party;
    }
}