﻿namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IEquipable
    {
        public int MinLevel { get; }
        public EEquipSlot EquipSlot { get; }
        float ApplyMetric(EMetric metric, float value);
    }
    
    public enum EEquipSlot
    {
        MainHand,
        OffHand,
        Head,
        Chest,
        Legs,
        Feet,
        Finger,
        Neck,
        Wrist,
        Waist
    }
}