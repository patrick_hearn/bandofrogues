﻿using System.Collections.Generic;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IInteractable : IActionable
    {
        void Interact(IInteractable source, Vector2Int position, IDictionary<string, object> parameters = null);
    }

}