﻿namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public enum ETileType
    { 
        // 2        1
        // OccludingBlocking
        None = -1,
        Floor =0,  //  00
        Wall=3,    //  11
        Screen=2, // 10
        Pit=1,  // 01
    }
}