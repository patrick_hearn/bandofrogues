﻿using System.Collections.Generic;
using com.patrickhearn.BandOfRogues.Interfaces;

namespace com.patrickhearn.BandOfRogues.Game
{
    /// <summary>
    /// Interface for a world.
    /// </summary>
    public interface IDungeonWorld
    {
        ITileMap TileMap { get; }
        List<IActor> Actors { get; }
    }
}