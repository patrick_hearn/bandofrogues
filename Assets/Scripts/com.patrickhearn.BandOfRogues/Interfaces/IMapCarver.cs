using System.Collections.Generic;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IMapCarver
    {
        int[,] CarveRoomsAndCorridors(int[,] initialMap, List<IRoom> rooms, List<ICorridor> corridors);
    }
}