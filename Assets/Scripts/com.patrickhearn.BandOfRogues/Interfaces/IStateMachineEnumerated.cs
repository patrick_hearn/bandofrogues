﻿namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IStateFactoryEnumerated<out T, in TEnum> : IStateFactory<T> where T : IStateEnumerated<TEnum> where TEnum : System.Enum
    {
        new T GetState(TEnum state);
    }


    public interface IStateEnumerated<TEnum> : IState where TEnum : System.Enum
    {
        TEnum StateEnum { get; }
        bool CanTransitionTo(TEnum state);
    }

    public interface IStatefulEnumerated<TEnum> : IStateful where TEnum : System.Enum
    {
        TEnum CurrentStateEnum { get; }
        void ChangeState(TEnum newState);
    }

    public interface IStatefulEnumerated<T, TEnum> : IStateful<T> where T : IStateEnumerated<TEnum> where TEnum : System.Enum
    {
        new TEnum CurrentStateEnum { get; }
        void ChangeState(TEnum newState);
    }
    

    public interface IStatefulEnumerated<T, TEnum, out TFactory> : IStateful<T, TFactory>, IStatefulEnumerated<T, TEnum> where T : IStateEnumerated<TEnum> where TFactory : IStateFactoryEnumerated<T, TEnum>, IStateFactory<T> where TEnum : System.Enum
    {
        new TFactory StateFactory { get; }
    }
}