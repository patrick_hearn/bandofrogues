﻿using System.Collections.Generic;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IMapGenerator<T> : IGenerator<int[,], T> where T : IMapGenerationParameters
    {
    }
    
    public interface IMapGenerationParameters : IGenerationParameters
    {
        int Width { get; }
        int Height { get; }
        int[,] BaseMap { get; }
    }
    
    

    public interface ITileMapFactory<TTileMap, TTile> where TTileMap : ITileMap where TTile : ITile
    {
        TTileMap Create(int[,] mapRepresentation, Dictionary<int, TTile> tileMapper);
    }
    public interface IRoomPlacer
    {
        List<IRoom> PlaceRooms(int roomCount, int minRoomSize, int maxRoomSize, int mapWidth, int mapHeight);
    }
    

}