using System.Collections.Generic;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface ICorridor
    {
        List<Vector2Int> GetPositions();
        Vector2Int GetStart();
        Vector2Int GetEnd();
    }
}