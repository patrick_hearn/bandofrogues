﻿namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IMetric
    {
        public float ApplyMetric(EMetric metric, float value);
        
        public float GetMetric(EMetric metric);
    }
}