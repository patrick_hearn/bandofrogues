using System.Collections.Generic;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface ICorridorPlacer
    {
        List<ICorridor> ConnectRooms(List<IRoom> rooms);
    }
}