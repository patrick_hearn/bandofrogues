﻿namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IPickupable<T>
    {
        public T Pickup();

    }
}