﻿using System.Collections.Generic;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    /// <summary>
    /// A tile map is a grid of tiles that can be used for pathfinding and occlusion.
    /// </summary>
    public interface ITileMap : IGrid<ITile>, IPathFinding
    {
        /// <summary>
        /// Returns a list of tiles that are adjacent to the given tile.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        bool IsOccluding(Vector2Int position);
        /// <summary>
        /// Returns a list of tiles that are adjacent to the given tile.
        /// </summary>
        /// <returns></returns>
        int[,] GetOcclusionGrid();
    }
}