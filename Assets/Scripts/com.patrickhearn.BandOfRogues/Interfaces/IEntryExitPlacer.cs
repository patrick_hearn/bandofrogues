using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IEntryExitPlacer
    {
        (Vector2Int Entry, Vector2Int Exit) PlaceEntryAndExit(int[,] map);
    }
}