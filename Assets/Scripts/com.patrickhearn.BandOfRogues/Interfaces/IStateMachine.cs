﻿namespace com.patrickhearn.BandOfRogues.Interfaces
{
    /// <summary>
    ///     Interface for a state
    /// </summary>
    public interface IState
    {
        /// <summary>
        ///     Enter the state
        /// </summary>
        void Enter();
        /// <summary>
        ///     Execute the state
        /// </summary>
        void Execute();
        /// <summary>
        ///     Exit the state
        /// </summary>
        void Exit();
        /// <summary>
        ///     Can transition to the state
        /// </summary>
        bool CanTransitionTo(IState state);
    }
    
    /// <summary>
    ///     Interface for a stateful object
    /// </summary>
    
    public interface IStateful
    {
        /// <summary>
        ///     The current state
        /// </summary>
        IState CurrentState { get; }
        /// <summary>
        ///     Change the state
        /// </summary>
        void ChangeState(IState newState);
        /// <summary>
        ///     Update the state
        /// </summary>
        void Update();
    }

    /// <summary>
    ///     Interface for a state factory
    /// </summary>
    public interface IStateFactory<out T>
    {
        /// <summary>
        ///     Create a new state
        /// </summary>
        /// <returns></returns>
        T Create();
    }

    /// <summary>
    ///     Interface for a stateful object with a generic state
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IStateful<T> : IStateful where T : IState
    {
        /// <summary>
        ///     The current state
        /// </summary>
        new T CurrentState { get; }
        /// <summary>
        ///     Change the state
        ///   <param name="newState"></param>
        /// </summary>
        void ChangeState(T newState);
    }

    
    /// <summary>
    ///     Interface for a stateful object with a generic state and factory
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TFactory"></typeparam>
    public interface IStateful<T, out TFactory> : IStateful<T> where T : IState where TFactory : IStateFactory<T>
    {
        /// <summary>
        ///     The state factory
        /// </summary>
        TFactory StateFactory { get; }
    }

  

}