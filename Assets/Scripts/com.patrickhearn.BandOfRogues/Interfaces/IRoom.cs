﻿using System.Collections.Generic;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IRoom
    {
        bool IsInside(Vector2Int position);
        Rect GetBounds();
        List<Vector2Int> GetIncludedPositions();
        List<Vector2Int> GetBoundaryPositions();
        List<Vector2Int> GetInternalPositions();
    }
}