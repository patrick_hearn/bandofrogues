﻿using UnityEngine;
using System.Collections.Generic;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IEffect
    {
        void Execute();
        float GetEnergy(Vector2Int position);
        void SetEnergy(Vector2Int position, float energy);
        float GetDecayRate();
        float GetTransmissionRate();
        Dictionary<Vector2Int, float> GetEnergyMap();
        void ApplyEnergyDistribution(IEnergyDistributionAlgorithm energyDistributionAlgorithm);
        }
    
    public interface IEnergyDistributionAlgorithm
    {
        void DistributeEnergy(IEffect effect);
    }


}