﻿using System.Collections.Generic;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IVisionSystem
    {
        List<Vector2Int> GetVisibleTiles(Vector2Int position, int maxDistance);
        bool IsTileVisibleFrom(Vector2Int fromPosition, Vector2Int toPosition, int? maxDistance = null);
    }

}