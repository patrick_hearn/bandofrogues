﻿namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public enum EMetric
    {
        Accuracy = 1,
        Evasion = 2,
        MinHit = 3,
        MaxHit = 4,
        MinDefense = 5,
        MaxDefense = 6,
        Speed = 7,
        AttackSpeed = 8,
        AttackRange = 9,
        Weight = 10,
        Value = 11,
        MaxHealth = 12,
        MaxMana = 13,
        ManaRegenRate = 14,
        HealthRegenRate = 15
    }
}