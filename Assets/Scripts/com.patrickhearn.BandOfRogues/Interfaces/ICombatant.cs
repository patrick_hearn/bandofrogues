namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface ICombatant
    {
        string Name { get; }
        float GetAccuracy();
        float GetEvasion();
        float GetMinHit();
        float GetMaxHit();
        float GetMinDefense();
        float GetMaxDefense();
        
        void TakeDamage(int damage, ICombatant attacker);
        
        bool IsDead();
        
        void Attack(ICombatant target);
    }
}