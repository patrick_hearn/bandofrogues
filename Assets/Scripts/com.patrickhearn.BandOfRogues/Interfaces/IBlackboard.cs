﻿namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IBlackboard
    {
        
    }
    
    public interface ICommandBlackboard<T> : IBlackboard where T : IActionable
    {
        ICommandContext<T> CommandContext { get; }
    }
    

    
}