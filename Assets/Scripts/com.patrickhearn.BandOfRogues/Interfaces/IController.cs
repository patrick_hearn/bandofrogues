﻿namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IController<TActionable> 
        where TActionable : IActionable
    {
        ICommand<TActionable> GetCommand(TActionable actor);
    }
}