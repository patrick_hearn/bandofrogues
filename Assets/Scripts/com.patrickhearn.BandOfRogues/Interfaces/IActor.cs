﻿using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    /// <summary>
    /// An actor is an entity that can move around the map.
    /// </summary>
    public interface IActor : IEntity, IInteractable
    {
        /// <summary>
        /// The position of the actor on the map.
        /// </summary>
        Vector2Int Position { get; set; }
        
        /// <summary>
        /// Indicates whether the actor is currently blocking movement.
        /// </summary>
        bool IsBlocking { get; set; }
    
        
        /// <summary>
        /// Called by the game manager to allow the actor to perform an action.
        /// </summary>
        public void DoTurn(float tickDelta);
    }
}