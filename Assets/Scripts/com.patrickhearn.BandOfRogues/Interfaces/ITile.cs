﻿namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface ITile
    {
        ETileType TileType { get;  }
        float MovementCost { get;  }
        bool IsWalkable { get;  }
        bool IsOccluding { get;  }
        
    }
}