﻿using System.Collections.Generic;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IMover : IActionable
    {
        float Speed { get; }
        float MoveProgress { get; }
        Vector2Int Destination { get; set; }

        bool AttemptMove(Vector2Int targetPosition);
    }

    public interface IMob : IActor, IMover, IInteractable
    {
        
    }
    
    public interface IActorManager
    {
        void RegisterActor(IActor actor);
        void UnregisterActor(IActor actor);
        List<IActor> GetActors();
        IActor GetActorAt(Vector2Int position);
        List<IActor> GetActorsOfType<T>(); 
    }
    
    public interface ITurnManager : ITickable
    {
        int GetTurnCount();
        float GetTickDelta();
        IActor GetCurrentActor(); 
    }
    
    public interface IPathManager
    {
        List<Vector2Int> GetPath(Vector2Int start, Vector2Int end, bool allowDiagonal = false);
        bool CanMoveTo(Vector2Int position);
    }


}