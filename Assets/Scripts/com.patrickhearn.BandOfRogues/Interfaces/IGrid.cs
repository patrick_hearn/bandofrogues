﻿using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IGrid<T>
    {
        T GetItem(Vector2Int position);
        void SetItem(Vector2Int position, T item);
        bool IsInBounds(Vector2Int position);
        Vector2Int Size { get; }
    }
}