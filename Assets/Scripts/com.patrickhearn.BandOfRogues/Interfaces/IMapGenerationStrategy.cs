namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IMapGenerationStrategy
    {
        int[,] Execute(int[,] initialMap);
    }
}