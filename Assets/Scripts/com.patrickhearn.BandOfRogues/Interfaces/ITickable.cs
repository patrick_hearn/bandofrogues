﻿namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface ITickable
    {
        void Tick();
        
    }
}