﻿using System.Collections.Generic;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IGenerator<T> 
    {
        T Generate();
    }
    
    public interface IGenerator<T, in TGenerationParameters> : IGenerator<T>
    {
        T Generate(TGenerationParameters parameters);
    }
    
    public interface IGenerationParameters
    {
    }
    
    
    /// <summary>
    /// Parameters for generating a mob.
    /// </summary>
    public interface IMobGenerationParameters : IGenerationParameters
    {
        int Level { get; }
        int Strength { get; }
        int Dexterity { get; }
        int Constitution { get; }
        int Intelligence { get; }
    }

    /// <summary>
    /// Generates a mob based on the parameters provided.
    /// </summary>
    public interface IMobGenerator : IGenerator<IMob, IMobGenerationParameters>
    {
        
    }
    
    
    /// <summary>
    /// Generates a world based on the parameters provided.
    /// </summary>
    public interface IWorldGenerator<TWorld, TWorldGenerationParameters> : IGenerator<TWorld, TWorldGenerationParameters> where TWorld : IWorld where TWorldGenerationParameters : IWorldGenerationParameters
    {
        
    }

    public interface IWorld
    {
    }

    public interface IWorldGenerationParameters : IGenerationParameters
    {
    }

    // /// <summary>
    // /// Parameters for generating a world.
    // /// </summary>
    // public interface ITileMapParameters : IGenerationParameters
    // {
    //     int[,] MapRepresentation { get; }
    //     Dictionary<int, ITile> TileMapper { get; }
    // }
}