﻿using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IMoveManager
    {
        bool TryMoveActor(IActor actor, Vector2Int position);
        bool TryPlaceActor(IActor actor, Vector2Int position);
    }
}
