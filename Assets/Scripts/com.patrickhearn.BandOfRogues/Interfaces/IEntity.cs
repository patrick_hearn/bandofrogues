﻿namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IEntity
    {
        public string Name { get; }
    }
}