﻿using System.Collections.Generic;
using UnityEngine;

namespace com.patrickhearn.BandOfRogues.Interfaces
{
    public interface IPathFinding
    {
        List<Vector2Int> GetNeighbours(Vector2Int position, bool includeDiagonal = false);
        bool IsWalkable(Vector2Int position);
        float GetCost(Vector2Int position);
        int[,] GetWalkableGrid();
        float[,] GetCostGrid();
    }
    
    public interface IPathfinder<T>
    {
        List<T> FindPath(T start, T goal);
        void SetWalkableGrid(bool[,] grid);
        void SetCostGrid(float[,] grid);
        bool AllowDiagonal { get; set; }
    }

}