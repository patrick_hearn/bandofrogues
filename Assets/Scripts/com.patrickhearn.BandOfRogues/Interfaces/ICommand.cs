﻿namespace com.patrickhearn.BandOfRogues.Interfaces
{

    public interface ICommandContext<TActionable>
        where TActionable : IActionable
    {
        TActionable Entity { get; set; }
        ICommand<TActionable> GetCommand();
    }

    public interface ICommand<TActionable> 
        where TActionable : IActionable
    {
        void Execute(ICommandContext<TActionable> context);
    }
}