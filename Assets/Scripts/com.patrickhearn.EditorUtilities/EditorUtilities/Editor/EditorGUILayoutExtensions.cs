﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace com.patrickhearn.EditorUtilities.Editor
{
    public static class EditorGUILayoutExtensions
    {
        public static bool ButtonGrid(IEnumerable<string> labels, GUIStyle style, Action<string> onClick)
        {
            bool clicked = false;

            // Store the labels in a list to allow indexed access
            List<string> labelList = labels.ToList();

            // Begin the horizontal layout group
            EditorGUILayout.BeginHorizontal();

            for (int i = 0; i < labelList.Count; i++)
            {
                // Layout the button but don't handle its click event yet
                GUILayout.Button(labelList[i], style);

                // Handle the click event during the repaint event
                if (Event.current.type == EventType.Repaint && GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition) && Event.current.button == 0 && Event.current.clickCount == 1)
                {
                    onClick?.Invoke(labelList[i]);
                    clicked = true;
                }

                // Wrap to the next line if the next button will exceed the container width
                if (i < labelList.Count - 1 && GUILayoutUtility.GetLastRect().xMax + style.CalcSize(new GUIContent(labelList[i + 1])).x > EditorGUIUtility.currentViewWidth)
                {
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.BeginHorizontal();
                }
            }

            // End the horizontal layout group
            EditorGUILayout.EndHorizontal();

            return clicked;
        }

    }

}