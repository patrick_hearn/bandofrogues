using ProceduralToolkit.Samples.Buildings.Runtime;
using UnityEditor;
using UnityEngine;

namespace ProceduralToolkit.Samples.Buildings.Editor
{
    [CustomEditor(typeof(BuildingGeneratorComponent))]
    public class BuildingGeneratorComponentEditor : UnityEditor.Editor
    {
        private BuildingGeneratorComponent generator;

        private void OnEnable()
        {
            generator = (BuildingGeneratorComponent) target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Space();
            if (GUILayout.Button("Generate building"))
            {
                var transform = generator.Generate();
                Undo.RegisterCreatedObjectUndo(transform.gameObject, "Generate building");
            }
        }
    }
}
