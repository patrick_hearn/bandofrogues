namespace ProceduralToolkit.Runtime.Geometry
{
    public enum IntersectionType : byte
    {
        None = 0,
        Point,
        TwoPoints,
        Line,
        Ray,
        Segment,
        Circle,
        Sphere,
    }
}
