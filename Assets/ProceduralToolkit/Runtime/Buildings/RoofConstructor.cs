using ProceduralToolkit.Runtime.Buildings.Interfaces;
using UnityEngine;

namespace ProceduralToolkit.Runtime.Buildings
{
    public abstract class RoofConstructor : ScriptableObject, IRoofConstructor
    {
        public abstract void Construct(IConstructible<MeshDraft> constructible, Transform parentTransform);
    }
}
