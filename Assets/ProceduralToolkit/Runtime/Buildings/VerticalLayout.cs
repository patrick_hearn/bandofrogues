using ProceduralToolkit.Runtime.Buildings.Interfaces;
using UnityEngine;

namespace ProceduralToolkit.Runtime.Buildings
{
    public class VerticalLayout : Layout
    {
        public override void Add(ILayoutElement element)
        {
            base.Add(element);
            element.origin = Vector2.up*height;
            height += element.height;
            width = Mathf.Max(width, element.width);
        }
    }
}
