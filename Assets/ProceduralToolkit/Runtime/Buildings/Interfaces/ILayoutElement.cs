using UnityEngine;

namespace ProceduralToolkit.Runtime.Buildings.Interfaces
{
    public interface ILayoutElement
    {
        Vector2 origin { get; set; }
        float width { get; set; }
        float height { get; set; }
    }
}
