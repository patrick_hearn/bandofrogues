using System.Collections.Generic;
using UnityEngine;

namespace ProceduralToolkit.Runtime.Buildings.Interfaces
{
    public interface IFacadePlanner
    {
        List<ILayout> Plan(List<Vector2> foundationPolygon, BuildingGenerator.Config config);
    }
}
