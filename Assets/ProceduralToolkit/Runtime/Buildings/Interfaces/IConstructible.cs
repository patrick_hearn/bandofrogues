using UnityEngine;

namespace ProceduralToolkit.Runtime.Buildings.Interfaces
{
    public interface IConstructible<out T>
    {
        T Construct(Vector2 parentLayoutOrigin);
    }
}
