using System.Collections.Generic;

namespace ProceduralToolkit.Runtime.Buildings.Interfaces
{
    public interface ILayout : ILayoutElement, IEnumerable<ILayoutElement>
    {
        void Add(ILayoutElement element);
    }
}
