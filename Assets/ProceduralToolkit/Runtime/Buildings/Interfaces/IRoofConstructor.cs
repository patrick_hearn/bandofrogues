using UnityEngine;

namespace ProceduralToolkit.Runtime.Buildings.Interfaces
{
    public interface IRoofConstructor
    {
        void Construct(IConstructible<MeshDraft> constructible, Transform parentTransform);
    }
}
