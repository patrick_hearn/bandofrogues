using System.Collections.Generic;
using ProceduralToolkit.Runtime.Buildings.Interfaces;
using UnityEngine;

namespace ProceduralToolkit.Runtime.Buildings
{
    public abstract class FacadeConstructor : ScriptableObject, IFacadeConstructor
    {
        public abstract void Construct(List<Vector2> foundationPolygon, List<ILayout> layouts, Transform parentTransform);
    }
}
